void generate() {

  TFile* out = new TFile("bootstraps.root", "RECREATE");
  TTree t1("toys1","");
  TTree t2("toys2","");

  Double_t v1;
  Double_t v2;
  Double_t v3;

  t1.Branch("XSA", &v1, "XSA/D");
  t2.Branch("XSA", &v2, "XSA/D");
  t2.Branch("XSB", &v3, "XSB/D");

  for (int i = 0; i < 1000; ++i) {

      TRandom rand(0);
      double random = rand.Uniform(0.,1.);
      int seed1 = 123+i;
      int seed2 = random < 0.3 ? seed1 : 123456+i;
      int seed3 = 7654321+i;

      TRandom3 rand1(seed1);
      v1 = rand1.Gaus(10, 2);
      TRandom3 rand2(seed2);
      v2 = rand2.Gaus(11, 3);
      TRandom3 rand3(seed3);
      v3 = rand3.Gaus(10, 5);

      t1.Fill();
      t2.Fill();
  }

  out->cd();
  t1.Write();
  t2.Write(); 
 
  out->Close();

}
