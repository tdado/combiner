void generate_NPuncorrelated() {

  TFile* out = new TFile("bootstraps_NPuncorrelated.root", "RECREATE");
  TTree t1("toys1","");
  TTree t2("toys2","");

  Double_t v1;
  Double_t v2;
  Double_t v3;
  Double_t NP1_1;
  Double_t NP2_1;
  Double_t NP1_2;
  Double_t NP2_2;

  t1.Branch("XSA", &v1, "XSA/D");
  t2.Branch("XSA", &v2, "XSA/D");
  t2.Branch("XSB", &v3, "XSB/D");
  t1.Branch("NP1", &NP1_1, "NP1/D");
  t1.Branch("NP2", &NP2_1, "NP2/D");
  t2.Branch("NP1", &NP1_2, "NP1/D");
  t2.Branch("NP2", &NP2_2, "NP2/D");

  for (int i = 0; i < 1000; ++i) {

      TRandom3 rand(0);

      v1 = rand.Gaus(10, 2);
      NP1_1 = rand.Gaus(0.3,0.1);
      NP2_1 = rand.Gaus(0.4,0.1);
      v2 = rand.Gaus(11, 3);
      NP1_2 = rand.Gaus(-0.3,0.1);
      NP2_2 = rand.Gaus(0.3,0.1);
      v3 = rand.Gaus(10, 5);

      t1.Fill();
      t2.Fill();
  }

  out->cd();
  t1.Write();
  t2.Write(); 
 
  out->Close();

}
