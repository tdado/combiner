#include "include/Plotter.h"

#include "TBox.h"
#include "TCanvas.h"
#include "TGraphAsymmErrors.h"
#include "TH1D.h"
#include "TH2.h"
#include "TH2D.h"
#include "TLatex.h"
#include "TLegend.h"
#include "TLine.h"
#include "TString.h"

#include <algorithm>
#include <iostream>

Plotter::Plotter(const std::string& outputPath, const std::vector<std::string>& format) :
    m_outputPath(outputPath),
    m_printFormat(format)
{
}
  
void Plotter::PlotCorrelationCovariance(const TMatrixDSym& mat, const std::vector<std::string>& poiNames, const bool isCovariance) const {

    if (mat.GetNrows() != static_cast<int>(poiNames.size())) {
        std::cerr << "Plotter::PlotCorrelationCovariance: ERROR: Size of the provided matrix and the POIs do not match.\n";
        return;
    }

    if (isCovariance) {
        std::cout << "Plotter::PlotCorrelationCovariance: Plotting the input POI covariance matrix\n";
    } else {
        std::cout << "Plotter::PlotCorrelationCovariance: Plotting the input POI correlation matrix\n";
    }

    auto hist = Plotter::MatrixToTH2(mat);
    const int size = hist->GetNbinsX();
    for (int ibin = 0; ibin < size; ++ibin) {
        hist->GetXaxis()->SetBinLabel(ibin+1, poiNames.at(ibin).c_str());
        hist->GetYaxis()->SetBinLabel(ibin+1, poiNames.at(size - ibin - 1).c_str());
    }
    hist->GetZaxis()->SetTitle(isCovariance ? "Covariance elements" : "Correlation element");
    hist->GetZaxis()->CenterTitle();
    hist->GetZaxis()->SetTitleOffset(1.3 * hist->GetZaxis()->GetTitleOffset());
    hist->LabelsOption("v");

    TCanvas canvas("","", 1200, 1200);
    canvas.SetLeftMargin(0.20);
    canvas.SetRightMargin(0.17);
    canvas.SetBottomMargin(0.20);
    canvas.cd();

    hist->SetStats(false);
    if (poiNames.size() < 10) {
        hist->Draw("COLZ TEXT");
    } else {
        hist->Draw("COLZ");
    }

    const std::string outName = m_outputPath + "/" + (isCovariance ? "CovarianceMatrix" : "CorrelationMatrix") + ".";

    for (const auto& iformat : m_printFormat) {
        canvas.Print((outName+iformat).c_str());
    }
}
  
void Plotter::PlotParametersCorrelation(const TH2* cor, const std::vector<std::string>& params, const double threshold) const {
    const int nbins = cor->GetNbinsX();
    if (nbins != static_cast<int>(params.size())) {
        std::cerr << "Plotter::PlotParametersCorrelation: ERROR: Number of bins of the correlation matrix and the parameters do not match\n";
        return;
    }
    std::cout << "Plotter::PlotParametersCorrelation: Plotting the postfit parameter correlation matrix with a threshold of " << threshold << "\n";

    std::vector<int> passedIndices;

    // first get the list of NPs with the correlations.
    for (int ibin = 1; ibin <= nbins; ++ibin) {
        for (int jbin = 1; jbin <= nbins; ++jbin) {
            if (ibin == jbin) continue;
            const double correlation = cor->GetBinContent(nbins - jbin + 1, ibin);
            if (std::abs(correlation) > threshold) {
                passedIndices.emplace_back(ibin);
                break;
            }
        }
    }

    if (passedIndices.empty()) {
        std::cout << "Plotter::PlotParametersCorrelation: No correlations passes the threshold of: " << threshold << ". No correlation matrix will be plotted.\n";
        return;
    }

    TH2D hist("", Form("Correlations with threshold > %.2f", threshold), passedIndices.size(), 0, passedIndices.size(), passedIndices.size(), 0, passedIndices.size());
    hist.SetStats(false);
    for (int ibin = 1; ibin <= hist.GetNbinsX(); ++ibin) {
        const int index_i = passedIndices.at(ibin - 1);
        for (int jbin = 1; jbin <= hist.GetNbinsX(); ++jbin) {
            const int index_j = passedIndices.at(jbin - 1);
            hist.SetBinContent(hist.GetNbinsX() - ibin + 1, jbin, cor->GetBinContent(nbins - index_j + 1, index_i));
        }
    }

    for (int ibin = 1; ibin <= hist.GetNbinsX(); ++ibin) {
        const int index = passedIndices.at(ibin - 1);
        hist.GetXaxis()->SetBinLabel(ibin, params.at(index-1).c_str());
        hist.GetYaxis()->SetBinLabel(hist.GetNbinsX() - ibin + 1, params.at(index-1).c_str());
    }

    hist.LabelsOption("v");
    hist.GetZaxis()->SetTitle("Correlation elements");
    hist.GetZaxis()->CenterTitle();
    hist.GetZaxis()->SetTitleOffset(1.3 * hist.GetZaxis()->GetTitleOffset());

    TCanvas canvas("","", 1200, 1200);
    canvas.SetLeftMargin(0.20);
    canvas.SetRightMargin(0.17);
    canvas.SetBottomMargin(0.20);
    canvas.cd();

    if (params.size() < 10) {
        hist.Draw("COLZ TEXT");
    } else {
        hist.Draw("COLZ");
    }

    const std::string outName = m_outputPath + "/PostFitCorrelations.";

    for (const auto& iformat : m_printFormat) {
        canvas.Print((outName+iformat).c_str());
    }
} 
  
void Plotter::PlotPOIs(const std::vector<std::tuple<std::string, std::string, double, double, double> >& pois) const {
    std::cout << "Plotter::PlotPOIs: Plotting the POIs and their uncertainties\n";

    const int size = pois.size();

    double min = 999999;
    double max = -999999;

    TGraphAsymmErrors graph(size);
    for (int ipoi = 0; ipoi < size; ++ipoi) {
        graph.SetPoint(size - ipoi - 1, std::get<2>(pois.at(ipoi)), size - ipoi - 1);
        graph.SetPointEXlow(size - ipoi - 1, -std::get<3>(pois.at(ipoi)));
        graph.SetPointEXhigh(size - ipoi - 1, std::get<4>(pois.at(ipoi)));
        graph.SetPointEYlow(size - ipoi - 1, 0);
        graph.SetPointEYhigh(size - ipoi - 1, 0);
        double minCurrent = std::get<2>(pois.at(ipoi)) + std::get<3>(pois.at(ipoi));
        double maxCurrent = std::get<2>(pois.at(ipoi)) + std::get<4>(pois.at(ipoi));
        if (minCurrent < min) min = minCurrent;
        if (maxCurrent > max) max = maxCurrent;
    }
    graph.SetLineColor(kBlack);
    graph.SetMarkerColor(kBlack);
    graph.SetMarkerStyle(kFullCircle);

    if (min < 0) {
        min *= 1.8;
    } else {
        min /= 1.8;
    }
    if (max < 0) {
        max /= 3.2;
    } else {
        max *= 3.2;
    }

    TH1D dummy("", "", 1, min, max);
    dummy.SetStats(false);
    dummy.SetMinimum(-0.5);
    dummy.SetMaximum(size - 0.5);
    dummy.SetLineColor(kWhite);
    dummy.GetYaxis()->Set(size, -0.5, size - 0.5);
    dummy.GetYaxis()->SetNdivisions(size);
    dummy.GetYaxis()->SetTickSize(0);
    dummy.GetXaxis()->SetTitle("A.U.");

    TCanvas c("", "", 700, 400 + 40*size);
    c.cd();
    c.SetLeftMargin(0.3);
    c.SetRightMargin(0.02);
    c.SetTopMargin(0.02);
    if (std::abs(max/min) > 100. && min > 0) {
        c.SetLogx();    
    } 

    dummy.Draw();
    graph.Draw("p same");

    TLatex values{};
    values.SetTextSize(0.04);
    values.SetTextFont(42);

    for (int ipoi = 0; ipoi < size; ++ipoi) {
        dummy.GetYaxis()->SetBinLabel(size - ipoi, std::get<0>(pois.at(ipoi)).c_str());
        values.DrawLatex(min + std::abs(max-min)*0.5, size - ipoi - 1.05, Form("%.2f^{#plus%.2f}_{#minus%.2f}", std::get<2>(pois.at(ipoi)), std::abs(std::get<3>(pois.at(ipoi))),std::get<4>(pois.at(ipoi))));
    }

    const std::string outName = m_outputPath + "/POIs.";

    for (const auto& iformat : m_printFormat) {
        c.Print((outName+iformat).c_str());
    }
}
  
void Plotter::PlotPulls(const std::vector<std::tuple<std::string, std::string, double, double, double> >& combinedNPs,
                        const std::vector<std::string>& measurements,
                        const std::vector<std::vector<std::string> >& npNames,
                        const std::vector<std::vector<double> >& pulls,
                        const std::vector<std::vector<double> >& constraints,
                        const std::string& category) const {

    if (combinedNPs.empty()) {
        std::cout << "Plotter::PlotPulls: No NPs found, will not produce a pull plot\n";
        return;
    }
    
    std::cout << "Plotter::PlotPulls: Plotting the NP pulls for the combined fit and the following measurements:\n";
    for (const auto& imeas : measurements) {
        std::cout << "Plotter::PlotPulls:\t" << imeas << "\n";
    }

    TGraphAsymmErrors graphCombined{};
    std::vector<TGraphAsymmErrors> compareGraphs(measurements.size());

    int inp(0);
    for (const auto& icombined : combinedNPs) {
        const std::string& npName = std::get<0>(icombined);
        const std::string& cat = std::get<1>(icombined);
        if (cat != category) {
            continue;
        }

        graphCombined.SetPoint(inp, std::get<2>(icombined), inp+0.5);
        graphCombined.SetPointEXlow(inp, -std::get<3>(icombined));
        graphCombined.SetPointEXhigh(inp, std::get<4>(icombined));
        graphCombined.SetPointEYlow(inp, 0);
        graphCombined.SetPointEYhigh(inp, 0);

        // fill the comparison graphs
        for (std::size_t imeas = 0; imeas < measurements.size(); ++imeas) {
            // find which NP this corresponds to
            auto itr = std::find(npNames.at(imeas).begin(), npNames.at(imeas).end(), npName);
            if (itr != npNames.at(imeas).end()) {
                const std::size_t position = std::distance(npNames.at(imeas).begin(), itr);
                compareGraphs.at(imeas).SetPoint(inp, pulls.at(imeas).at(position), inp+0.5 + 0.12*(imeas+1));
                compareGraphs.at(imeas).SetPointEXlow(inp,  constraints.at(imeas).at(position));
                compareGraphs.at(imeas).SetPointEXhigh(inp, constraints.at(imeas).at(position));
                compareGraphs.at(imeas).SetPointEYlow(inp, 0);
                compareGraphs.at(imeas).SetPointEYhigh(inp, 0);
            } else {
                compareGraphs.at(imeas).SetPoint(inp, -100, 0);
            }
        }
        ++inp;
    }

    if (inp == 0) {
        std::cout << "Plotter::PlotPulls: No NPs for this category: " << category << ", skipping\n";
        return;
    }

    static const std::vector<int> color = {kRed, kBlue, kViolet, kOrange, kGreen+2};
    static const std::vector<int> style = {kOpenCircle, kFullTriangleUp, kOpenTriangleDown, kOpenDiamond};

    int istyle(0);
    for (auto& igraph : compareGraphs) {
        const int colorPosition = istyle % color.size();
        const int stylePosition = istyle % style.size();
        igraph.SetLineColor(color.at(colorPosition));
        igraph.SetMarkerColor(color.at(colorPosition));
        igraph.SetMarkerStyle(style.at(stylePosition));
        ++istyle;
    }
    
    graphCombined.SetLineColor(kBlack);
    graphCombined.SetMarkerColor(kBlack);
    graphCombined.SetMarkerStyle(kFullCircle);

    TCanvas c("", "", 800, 150 + 20*inp);
    c.SetTicks(1,0);
    c.SetTopMargin(0.2);
    c.SetBottomMargin(0.2);
    c.SetRightMargin(0.3);
    c.cd();

    TH1D dummy("", "", 10, -2.9, 2.9);
    dummy.SetMaximum(inp);
    dummy.SetLineWidth(0);
    dummy.SetFillStyle(0);
    dummy.SetLineColor(kWhite);
    dummy.SetFillColor(kWhite);
    dummy.SetMinimum(0);
    dummy.GetYaxis()->SetLabelSize(0);
    dummy.GetYaxis()->SetNdivisions(0);
    dummy.GetXaxis()->SetLabelSize(0.15);
    dummy.SetStats(false);
    dummy.Draw();

    TBox sigma1box(-1, 0, 1, inp);
    TBox sigma2box(-2, 0, 2, inp);

    sigma1box.SetFillColor(kGreen);
    sigma2box.SetFillColor(kYellow);

    sigma2box.Draw("same");
    sigma1box.Draw("same");

    graphCombined.Draw("p same");
    for (auto& igraph : compareGraphs) {
        igraph.Draw("p same");
    }

    TLine line(0, 0, 0, inp);
    line.SetLineStyle(7);
    line.SetLineColor(kBlack);
    line.Draw("same");

    inp = 0;
    TLatex params{};
    params.SetTextSize(0.15);
    params.SetTextFont(42);
    for (const auto& iparam : combinedNPs) {
        if (std::get<1>(iparam) != category) continue;
        params.DrawLatex(3, inp + 0.25, std::get<0>(iparam).c_str());
        ++inp;
    }

    TLegend leg(0.1, 0.8, 0.9, 0.99);
    leg.SetFillStyle(0);
    leg.SetBorderSize(0);
    leg.SetNColumns(compareGraphs.size() + 1);
    leg.AddEntry(&graphCombined, "combined", "lp");
    for (std::size_t igraph = 0; igraph < compareGraphs.size(); ++igraph) {
        leg.AddEntry(&compareGraphs.at(igraph), measurements.at(igraph).c_str(), "lp");
    }
    leg.Draw("same");

    const std::string outName = m_outputPath + "/Pulls_"+category+".";
    for (const auto& iformat : m_printFormat) {
        c.Print((outName+iformat).c_str());
    }
}

std::unique_ptr<TH2D> Plotter::MatrixToTH2(const TMatrixDSym& mat) {
    const int size = mat.GetNrows();
    std::unique_ptr<TH2D> hist = std::make_unique<TH2D>("", "", size, 0, size, size, 0, size);

    for (int ibin = 0; ibin < size; ++ibin) {
        for (int jbin = 0; jbin < size; ++jbin) {
            hist->SetBinContent(ibin+1, jbin+1, mat(ibin,size-jbin-1));
        }
    }

    return hist;
}