#include "include/FitUtils.h"

#include "RooAbsData.h"
#include "RooAbsPdf.h"
#include "RooRealVar.h"

#include <iostream>

void FitUtils::FixParameters(RooAbsPdf* model,
                             RooAbsData* data,
                             const std::vector<std::pair<std::string, double> >& fixParams) {

    auto params = model->getParameters(data);
    std::cout << "\n";
    for (auto iparam : *params) {
        const std::string name = iparam->GetName();
        auto it = std::find_if(fixParams.begin(), fixParams.end(), [&name](const auto& element){return element.first == name;});
        if (it != fixParams.end()) {
            std::cout << "FitUtils::FixParameters: Fixing parameter " << name << " to: " << it->second << "\n";
            static_cast<RooRealVar*>(iparam)->setVal(it->second);
            static_cast<RooRealVar*>(iparam)->setConstant(true);
        }
    }
    std::cout << "\n";
}