#include "include/Parser.h"

#include "include/Fitter.h"
#include "include/Parametrisation.h"

#include "yaml-cpp/yaml.h"

#include "TFile.h"
#include "TMatrixDSym.h"
#include "TTree.h"

#include <iostream>
#include <numeric>

Parser::Parser(Fitter* fitter) :
    m_binSizes(0),
    m_createWS(true),
    m_debug(false),
    m_fitter(fitter),
    m_fractionToCov(0.),
    m_fractionToCovPOI(""),
    m_hasNuisanceParameters(false),
    m_hasStatInGeneral(false),
    m_inversionPrecision(1e-3),
    m_statOnly(false),
    m_useCorrelatedNPs(false)
{
}

Parser::~Parser() {
    for (auto& ifile : m_fileMap) {
        ifile.second->Close();
    }
}

void Parser::ReadConfigFile(const std::string& path) {
    YAML::Node config = Parser::OpenConfigFile(path);

    std::cout << "Parser::ReadConfigFile: Started reading config file at: " << path << "\n";

    if (!config["general"]) {
        std::cerr << "Parser::ReadConfigFile: ERROR: Config file does not have \"general\" settings\n";
        std::exit(EXIT_FAILURE);
    }

    const auto& general = config["general"];
    this->ReadDebug(general);
    this->ReadCreateWS(general);
    this->ReadCorrelationThreshold(general);
    this->ReadDraw(general);
    this->ReadPrintFormat(general);
    this->ReadFitStrategy(general);
    this->ReadRanking(general);
    this->ReadThreads(general);
    this->ReadSkipFit(general);
    this->ReadRankingNP(general);
    this->ReadFixParams(general);
    this->ReadFixNPsToFitted(general);
    this->ReadInversionPrecision(general);
    this->ReadFractionToCov(general);
    this->ReadRanges(general);
    this->ReadPrintParameters(general);
    m_useCorrelatedNPs = this->Parser::ReadUseCorrelatedNPs(general);
    m_fitter->SetUseCorrelatedNPs(m_useCorrelatedNPs);
    m_fitter->SetOutputPath(this->Parser::ReadOutputPath(general));
    m_fitter->SetUsePearsonChi2(this->Parser::ReadPearsonChi2(general));
    m_fitter->SetUseMinos(this->Parser::ReadMinos(general));
    m_hasStatInGeneral = this->ReadStatCovarianceMatrix(general, "general");
    m_statOnly = this->ReadStatOnly(general);
    
    if (!config["measurements"]) {
        std::cerr << "Parser::ReadConfigFile: ERROR: Config file does not have \"measurements\" settings\n";
        std::exit(EXIT_FAILURE);
    }

    {
        const auto& measurements = config["measurements"];
        for (const auto& imeas : measurements) {
            if (!imeas["name"]) {
                std::cerr << "Parser::ReadConfigFile: ERROR: Measurements have no \"name\" objects\n";
                std::exit(EXIT_FAILURE);
            }
            m_measurements.emplace_back(imeas["name"].as<std::string>());
        }
    }
    m_fitter->SetMeasurements(m_measurements);
    this->ReadCompareMeasurements(general);

    for (const auto& imeas : m_measurements) {
        if (!config[imeas]) {
            std::cerr << "Parser::ReadConfigFile: ERROR: Measurement: " << imeas << " not found in the config\n";
            std::exit(EXIT_FAILURE);
        }
        if (!this->CheckMeasurementNode(config[imeas])) {
            std::cerr << "Parser::ReadConfigFile: ERROR: Unsupported options found in measurement: " << imeas << "\n"; 
            std::exit(EXIT_FAILURE);
        }
        const auto& measNode = config[imeas];
        if (measNode["configFile"]) {
            // open the file and pass its node
            const YAML::Node newNode = Parser::OpenConfigFile(measNode["configFile"].as<std::string>());
            this->ReadSingleMeasurement(newNode, imeas);
        } else {
            this->ReadSingleMeasurement(config[imeas], imeas);
        }
    }

    m_fitter->SetNPOIs(m_bins);
    this->ReadStatNPCorrelations(config);
    this->ProcessByHandStatNPCorrelations();
    this->ReadReparametrisation(config);

    if (!m_hasStatInGeneral && m_createWS) {
        this->ProcessBootstraps();
    }

    if (!m_statOnly) {
        this->ReadCrossNPs(config);
    }

    if (m_useCorrelatedNPs && m_createWS) {
        this->ProcessBootstrapsNPs();
    }

    if (!m_skippedNPs.empty()) {
        TMatrixDSym cov = m_fitter->GetStatCovarianceMatrix();

        if (m_debug) {
            std::cout << "Parser::ReadConfigFile: Printing stat covariance matrix \n";
            cov.Print();
        }

        this->AddSystToStatCov(&cov);
        if (m_debug) {
            std::cout << "Parser::ReadConfigFile: Printing stat covariance matrix after adding systematic that were selected to be added to the covariance matrix \n";
            cov.Print();
        }

        m_fitter->SetStatCovariance(cov);
    }

    m_fitter->SetNPUniqueCategories(m_npUniqueCategories);

    std::cout << "Parser::ReadConfigFile: Finished reading config file at: " << path << "\n\n";
}

void Parser::ReadCommandLine(const std::vector<std::string>& params) {
    static const std::vector<std::string> allowedOptions = {"rankingNP", "fixNPsToFitted", "createWS", "ranking", "skipFit", "ranking", "statOnly"};
    for (const auto& ioption : params) {
        const std::vector<std::string> tokenize = Parser::Vectorize(ioption, '=');
        if (tokenize.size() != 2) {
            std::cerr << "Parser::ReadCommandLine: ERROR Wrong CLI input for option: " << ioption << "\n";
            std::exit(EXIT_FAILURE);
        }

        const std::string& setting = tokenize.at(0);
        const std::string& value   = tokenize.at(1);

        if (std::find(allowedOptions.begin(), allowedOptions.end(), setting) == allowedOptions.end()) {
            std::cerr << "Parser::ReadCommandLine: ERROR Unsupported option: " << setting << "\n";
            std::exit(EXIT_FAILURE);
        }

        if (setting == "rankingNP") {
            m_fitter->SetRankingNP(value);
        }
        if (setting == "fixNPsToFitted") {
            m_fitter->SetFixNPsToFitted(Parser::StringToBoolean(value));
        }
        if (setting == "createWS") {
            m_fitter->SetCreateWS(Parser::StringToBoolean(value));
        }
        if (setting == "ranking") {
            m_fitter->SetRanking(Parser::StringToBoolean(value));
        }
        if (setting == "skipFit") {
            m_fitter->SetSkipFit(Parser::StringToBoolean(value));
        }
        if (setting == "statOnly") {
            m_fitter->SetStatOnly(Parser::StringToBoolean(value));
        }
    }
}

bool Parser::StringToBoolean(std::string s) {
    std::transform(s.begin(), s.end(), s.begin(), ::tolower);

    if (s == "true") return true;
    return false;
}

std::vector<std::string> Parser::Vectorize(const std::string& s, const char c) {
    std::vector<std::string> result;
    if (s.empty()) return result;

    std::string token("");
    for (const auto& i : s) {
        if (i == c) {
            result.emplace_back(token);
            token = "";
        } else {
            token += i;
        }
    }
    if (token != "") {
        result.emplace_back(token);
    }

    return result;
}

void Parser::AddSystToStatCov(TMatrixDSym* cov) const {

    auto MatchIndices = [this](const int input) {
        std::size_t measIndex(0);
        std::size_t currentSum(0);
        for (const auto& ipoi : this->m_bins) {
             for (std::size_t i = 0; i < ipoi; ++i) {
                 if (static_cast<std::size_t>(input) == (currentSum + i)) return std::make_pair(measIndex, i);
             }
             currentSum += this->m_bins.at(measIndex);
             ++measIndex;
        }
        std::cerr << "Parser::AddSystToStatCov: Cannot parse index: " << input << "\n";
        std::exit(EXIT_FAILURE);
    };

    for (int i = 0; i < cov->GetNrows(); ++i) {
        for (int j = i; j < cov->GetNrows(); ++j) {
            // get the correlations
            const double sigma_i = std::sqrt((*cov)(i,i));
            const double sigma_j = std::sqrt((*cov)(j,j));
            const double cor = (*cov)(i,j)/(sigma_i*sigma_j);

            const std::pair<std::size_t, std::size_t> indexI = MatchIndices(i);
            const std::pair<std::size_t, std::size_t> indexJ = MatchIndices(j);

            const std::size_t imeasi = indexI.first;
            const std::size_t imeasj = indexJ.first;
            const std::size_t ipoi = indexI.second;
            const std::size_t jpoi = indexJ.second;

            // add stat + syst
            const double statSyst_i = std::hypot(sigma_i, m_toCovariance.at(imeasi).at(ipoi));
            const double statSyst_j = std::hypot(sigma_j, m_toCovariance.at(imeasj).at(jpoi));

            // set the values again
            (*cov)(i, j) = statSyst_i * cor * statSyst_j;
            if (i != j) { 
                (*cov)(j, i) = statSyst_i * cor * statSyst_j;
            }
        }
    }
}

double Parser::CalculateBootsrapCorrelation(const std::vector<double>& vec1, const std::vector<double>& vec2) {
    if (vec1.size() != vec2.size()) {
        std::cerr << "Parser::CalculateBootsrapCorrelation: ERROR: Vector sizes do not match\n";
        std::exit(EXIT_FAILURE);
    }

    const std::size_t n = vec1.size();
    double sumX(0.);
    double sumY(0.);
    double sumXY(0.);
    double squareSumX(0.);
    double squareSumY(0.);

    for (std::size_t i = 0; i < n; ++i) {
        sumX += vec1[i];
        sumY += vec2[i];
        sumXY += vec1[i]*vec2[i];
        squareSumX += vec1[i]*vec1[i];
        squareSumY += vec2[i]*vec2[i];
    }
    const double nominator = n * sumXY - sumX * sumY;
    const double denominator = std::sqrt((n * squareSumX - sumX * sumX) * (n * squareSumY - sumY * sumY));
    return nominator/denominator;
}

double Parser::CalculateRMS(const std::vector<double>& vec) {
    const double sum = std::accumulate(vec.begin(), vec.end(), 0.0);
    const double mean = sum/vec.size();

    const double sqrsum = std::inner_product(vec.begin(), vec.end(), vec.begin(), 0.0);
    return std::sqrt(sqrsum/vec.size() - mean*mean);
}

bool Parser::CheckCorrelationMatrix(const std::vector<std::vector<double> >& values) {
    for (const auto& i : values) {
        for (const auto j : i) {
            if (std::abs(j) > 1.) return false;
        }
    }

    return true;
}

bool Parser::CheckNode(const YAML::Node& node, const std::vector<std::string>& names) const {
    for (const auto& element : node) {
        auto itr = std::find(names.begin(), names.end(), element.first.as<std::string>());
        if (itr == names.end()) {
            std::cerr << "Parser::CheckNode: ERROR: Unsupported option: " << element.first.as<std::string>() << "\n";  
            return false;
        }
    }

    return true;
}

bool Parser::CheckMeasurementNode(const YAML::Node& node) const {
    static const std::vector<std::string> names = {"configFile", "poiNames",
                                                   "statCovMatrix", 
                                                   "bootstrapReplicaFile", "bootstrapReplicaTree", "bootstrapReplicaVariables",
                                                   "values", 
                                                   "systematics",
                                                   "nuisanceParameters"};

    return this->CheckNode(node, names);
}

bool Parser::CheckSystematicsNode(const YAML::Node& node) const {
    static const std::vector<std::string> names = {"parameter",
                                                   "impact", 
                                                   "constraint", "pull", "addToCovariance", "category"};

    return this->CheckNode(node, names);
}

bool Parser::CheckNuisanceParameterNode(const YAML::Node& node) const {
    static const std::vector<std::string> names = {"names",
                                                   "correlationMatrix"};

    return this->CheckNode(node, names);
}

std::vector<std::vector<double> > Parser::GetCovarianceMatrix(const YAML::Node& node,
                                                              const std::string& name,
                                                              const std::string& measurement) {
    std::vector<std::vector<double> > result;
    if (!node[name]) {
        return result;
    }

    for (const auto& irow : node[name]) {
        if (!irow["row"]) {
            if (measurement.empty()) {
                std::cerr << "Parser::GetCovarianceMatrix: ERROR: No \"row\" provided in the crossNuisanceParameters block\n";
            } else {
                std::cerr << "Parser::GetCovarianceMatrix: ERROR: No \"row\" provided in measurement: " << measurement << "\n";
            }
            std::exit(EXIT_FAILURE);
        }

        if (!irow["row"].IsSequence()) {
            if (measurement.empty()) {
                std::cerr << "Parser::GetCovarianceMatrix: ERROR: \"row\" is not a sequence in the crossNuisanceParameters block\n";
            } else {
                std::cerr << "Parser::GetCovarianceMatrix: ERROR: \"row\" is not a sequence in measurement " << measurement << "\n";
            }
            
            std::exit(EXIT_FAILURE);
        }
        std::vector<double> rowValues;
        for (const auto ivalue : irow["row"]) {
            rowValues.emplace_back(ivalue.as<double>());
        }

        result.emplace_back(rowValues);
    }
    return result;
}

TMatrixDSym Parser::GetNuisanceParameters(const YAML::Node& node,
                                          const std::size_t nSyst,
                                          const std::string& measurement) {
    if (!node["names"]) {
        std::cerr << "Parser::GetNuisanceParameters: ERROR: No \"names\" provided in measurement: " << measurement << "\n";
        std::exit(EXIT_FAILURE);
    }

    if (!node["names"].IsSequence()) {
        std::cerr << "Parser::GetNuisanceParameters: ERROR: \"names\" are not a sequence in measurement: " << measurement << "\n";
        std::exit(EXIT_FAILURE);
    }

    std::vector<std::string> nps;
    const auto& nuisances = node["names"];
    for (const auto& inp : nuisances) {
        const std::string name = inp.as<std::string>();
        if (std::find(nps.begin(), nps.end(), name) != nps.end()) {
            std::cerr << "Parser::GetNuisanceParameters: ERROR: NuisanceParameter: " << name << " appeared multiple times in the list in measurement " << measurement << "\n";
            std::exit(EXIT_FAILURE);
        }
        nps.emplace_back(name);
    }

    if (nps.size() != nSyst) {
        std::cerr << "Parser::GetNuisanceParameters: ERROR: Size of the names and the listed systematics do not match in measurement: " << measurement << "\n";
        std::exit(EXIT_FAILURE); 
    }

    // retrieve the correlation not covaraince, but mathematically it is the same thing
    const auto corMatrixValues = this->GetCovarianceMatrix(node, "correlationMatrix", measurement);
    if (corMatrixValues.empty()) {
        std::cerr << "Parser::GetNuisanceParameters: ERROR: No correlation matrix provided for in measurement!: " << measurement << "\n";
        std::exit(EXIT_FAILURE);
    }

    if (!Parser::CheckCorrelationMatrix(corMatrixValues)) {
        std::cerr << "Parser::GetNuisanceParameters: ERROR: Correlation matrix has values outside of <-1,1> in measurement: " << measurement << " \n";
        std::exit(EXIT_FAILURE);
    }

    TMatrixDSym cor = Parser::VectorToMatrix(corMatrixValues);

    m_fitter->AddNPCorrelationNames(nps);
    m_npNamesOrder.emplace_back(nps);
    return cor;
}
  
std::vector<double> Parser::GetValues(const YAML::Node& node,
                                      const std::string& name,
                                      const std::string& nodeName) const {

    if (!node[name]) {
        std::cerr << "Parser::GetValues: ERROR: \"" << name << "\" has no values provided!\n";
        std::exit(EXIT_FAILURE);
    }

    std::vector<double> result;

    if (!node[name].IsSequence()) {
        std::cerr << "Parser::GetValues: ERROR: \"" << name << "\" is expected to be a sequence in node: " << nodeName << "\n";
        std::exit(EXIT_FAILURE);
    }

    for (const auto ivalue : node[name]) {
        result.emplace_back(ivalue.as<double>());
    }

    return result;
}

std::vector<double> Parser::GetVector(const std::vector<std::vector<double> >& vec, const std::size_t index) const {
    std::vector<double> result;
    for (const auto& ival : vec) {
        result.emplace_back(ival.at(index));
    }
    return result;
}

YAML::Node Parser::OpenConfigFile(const std::string& path) {
    YAML::Node config;
    try {
        config = YAML::LoadFile(path);
    } catch (const YAML::BadFile& e) {
        std::cerr << "Parser::OpenConfigFile: ERROR: Cannot open config file at " << path << "\n";
        std::exit(EXIT_FAILURE);
    }

    return config;
}

std::shared_ptr<TFile> Parser::OpenRootFile(const std::string& path) {
    std::shared_ptr<TFile> file(TFile::Open(path.c_str(), "READ"));
    if (!file) {
        std::cerr << "Parser::OpenRootFile: ERROR: Cannot open file: " << path << "\n";
        std::exit(EXIT_FAILURE);
    }
    auto itr = m_fileMap.find(path);
    if (itr == m_fileMap.end()) {
        m_fileMap.insert(std::make_pair(path, file));
        return file;
    } else {
        return itr->second;
    }
}

std::vector<std::pair<std::string, std::vector<double > > > Parser::PreReadImpacts(const YAML::Node& node) const {
    std::vector<std::pair<std::string, std::vector<double> > > result;
    for (const auto& isyst : node["systematics"]) {
        const std::string param = isyst["parameter"].as<std::string>();
        const std::vector<double> impacts = this->GetValues(isyst, "impact", param);

        result.emplace_back(std::make_pair(std::move(param), std::move(impacts)));
    }

    return result;
} 

void Parser::PrintSummary() const {
    std::cout << "\nParser::PrintSummary: ----------------------------------\n";
    std::cout << "Parser::PrintSummary: Printing config summary below\n";
    std::cout << "Parser::PrintSummary: ----------------------------------\n";
    std::cout << "Parser::PrintSummary: Print debug info: " << std::boolalpha << m_debug << std::noboolalpha << " \n";
    std::cout << "Parser::PrintSummary: Draw plots: " << std::boolalpha << m_fitter->GetDraw() << std::noboolalpha << " \n";
    if (m_fitter->GetDraw()) {
        std::cout << "Parser::PrintSummary: Using the following print formats: \n";
        for (const auto& iformat : m_fitter->GetPrintFormat()) {
            std::cout << "Parser::PrintSummary:\t " << iformat << "\n";
        }
    }
    std::cout << "Parser::PrintSummary: Matrix inversion precision: " << m_inversionPrecision << " \n";
    std::cout << "Parser::PrintSummary: Use CorrelatedNPs: " << std::boolalpha << m_fitter->GetUseCorrelatedNPs() << std::noboolalpha << " \n";
    std::cout << "Parser::PrintSummary: Use Minos: " << std::boolalpha << m_fitter->GetUseMinos().first << std::noboolalpha << " \n";
    if (m_fitter->GetUseMinos().first) {
        for (const auto& i : m_fitter->GetUseMinos().second) {
            std::cout << "Parser::PrintSummary: \tUse Minos for param: " << i << " \n";
        }
    }
    std::cout << "Parser::PrintSummary: Use Pearson chi^2 for: \n";
    const std::vector<std::string> pearson = m_fitter->GetUsePearsonChi2();
    for (const auto& i : pearson) {
        std::cout << "Parser::PrintSummary:\t" << i <<"\n";
    }
    std::cout << "Parser::PrintSummary: Statistics only fit: " << std::boolalpha << m_statOnly << std::noboolalpha << " \n";
    std::cout << "Parser::PrintSummary: ----------------------------------\n";
    std::cout << "Parser::PrintSummary: Found the following POIs\n";
    const std::vector<std::string> pois = m_fitter->GetPOINames();
    for (const auto& ipoi : pois) {
        std::cout << "Parser::PrintSummary: \t" << ipoi << "\n"; 
    }
    std::cout << "Parser::PrintSummary: ----------------------------------\n";
    std::cout << "Parser::PrintSummary: Found the following measurements\n";
    for (std::size_t imeas = 0; imeas < m_measurements.size(); ++imeas) {
        std::cout << "Parser::PrintSummary: \t" << m_measurements.at(imeas) << " with " << 
            m_bins.at(imeas) << " bin(s), NP provided: " << std::boolalpha << m_hasNuisanceParameters.at(imeas) << std::noboolalpha << "\n";
    }
    std::cout << "Parser::PrintSummary: ----------------------------------\n";
    std::cout << "Parser::PrintSummary: Found the following systematics\n";
    const auto& systematics = m_fitter->GetSystematicMap();
    for (const auto& isyst : systematics) {
        std::cout << "Parser::PrintSummary: \t" << isyst.first << "\n";
    }
    const auto& repar = m_fitter->GetReparametrised();
    std::cout << "\nParser::PrintSummary: Reparametrisation\n";
    repar.Print();
    std::cout << "Parser::PrintSummary: ----------------------------------\n\n";
}

void Parser::ProcessByHandStatNPCorrelations() {
    if (m_byHandNPcorrelations.GetNrows() == 0) return;
    const auto& inputConstraints = m_fitter->GetInputConstraints();
    std::vector<double> constraints;
    for (const auto& i : inputConstraints) {
        for (const auto c : i) {
            constraints.emplace_back(c);
        }
    }

    if (constraints.size() != static_cast<std::size_t>(m_byHandNPcorrelations.GetNrows())) {
        std::cerr << "Parser::ProcessByHandStatNPCorrelations: ERROR: Sizes of the constraints and the by hand stat NP correlations do not match\n";
        std::cerr << "Parser::ProcessByHandStatNPCorrelations: ERROR: size of constraints: " << constraints.size() << ", correlations: " << m_byHandNPcorrelations.GetNrows() << "\n";
        std::exit(EXIT_FAILURE);
    }

    TMatrixDSym cov = Fitter::CorrelationToCovariance(m_byHandNPcorrelations, constraints);
    if (m_debug && (cov.GetNrows() < 10)) {
        std::cout << "Parser::ProcessByHandStatNPCorrelations: Printing the by hand NP stat covariance matrix\n";
        cov.Print();
    }
    m_fitter->SetTotalNPCovarianceCorrelated(cov);
}

void Parser::ProcessBootstraps() {
    std::cout << "Parser::ProcessBootstraps: Started processing bootstraps for POIs\n";
    if (m_replicas.empty()) {
        std::cerr << "Parser::ProcessBootstraps: ERROR: Replicas are empty!\n";
        std::exit(EXIT_FAILURE);
    }

    if (m_replicas.at(0).empty()) {
        std::cerr << "Parser::ProcessBootstraps: ERROR: Replicas at(0) are empty\n";
        std::exit(EXIT_FAILURE);
    }


    const std::size_t totalSize = std::accumulate(m_bins.begin(), m_bins.end(), 0);

    TMatrixDSym covariance(totalSize);
    // add the correlations that we can from the individual measurements
    {
        std::size_t offset(0);
        for (std::size_t icov = 0; icov < m_statCovMatrices.size(); ++icov) {
            for (int i = 0; i < m_statCovMatrices.at(icov).GetNrows(); ++i) {
                for (int j = 0; j < m_statCovMatrices.at(icov).GetNrows(); ++j) {
                    covariance[offset+i][offset+j] = m_statCovMatrices.at(icov)[i][j];
                }
            }
            offset += m_bins.at(icov);
        }
    }

    // need to get the cross-correlations
    for (std::size_t i = 0; i < m_bins.size(); ++i) {
        for (std::size_t j = i + 1; j < m_bins.size(); ++j) {
            for (std::size_t ibin = 0; ibin < m_bins.at(i); ++ibin) {
                for (std::size_t jbin = 0; jbin < m_bins.at(j); ++jbin) {
                    std::size_t offsetX(0);
                    for (std::size_t index = 0; index < j;  ++index) {
                        offsetX += m_bins.at(index);
                    }

                    std::size_t offsetY(0);
                    for (std::size_t index = 0; index < i;  ++index) {
                        offsetY += m_bins.at(index);
                    }
                    const std::size_t indexX = offsetX + jbin;
                    const std::size_t indexY = offsetY + ibin;
                    
                    double corr = this->CalculateBootsrapCorrelation(GetVector(m_replicas.at(i), ibin), GetVector(m_replicas.at(j), jbin));
                    if ((i != j) && (std::abs(corr) > 1-m_inversionPrecision)) {
                        std::cout << "Parser::ProcessBootstraps: WARNING: CovMatrix(" << indexX << "," << indexY << ") has correlations too close to 1\n";
                        std::cout << "Parser::ProcessBootstraps: WARNING: You use the same name of the NPs instead of trying to correlate them by 100%\n";
                        std::cout << "Parser::ProcessBootstraps: WARNING: Setting the correlation to " << (corr > 0 ? 1-m_inversionPrecision : -1+m_inversionPrecision) <<"\n";
                        corr = corr > 0 ? 1-m_inversionPrecision : -1+m_inversionPrecision;
                    }
                    // use: cov = sigma_i * corr * sigma_j
                    const double sigma_i = std::sqrt(m_statCovMatrices.at(i)[ibin][ibin]);
                    const double sigma_j = std::sqrt(m_statCovMatrices.at(j)[jbin][jbin]);

                    const double cov = sigma_i * corr * sigma_j;
                    covariance[indexX][indexY] = cov;
                    if (indexX != indexY) {
                        covariance[indexY][indexX] = cov;
                    }
                }
            }
        }
    }

    if (m_debug) {
        std::cout << "Parser::ProcessBootstraps: Printing the POI correlation matrix\n";
        const auto correlation = Fitter::CovarianceToCorrelation(covariance);
        correlation.Print();
        std::cout << "Parser::ProcessBootstraps: Printing the POI covariance matrix\n";
        covariance.Print();
    }
    
    m_fitter->SetStatCovariance(covariance);
}

void Parser::ProcessBootstrapsNPs() {
    if (m_byHandNPcorrelations.GetNrows() > 0) return;
    std::cout << "Parser::ProcessBootstraps: Started processing bootstraps for NPs\n";

    if (m_replicasNPs.empty()) {
        std::cerr << "Parser::ProcessBootstrapsNPs: ERROR: Replicas are empty!\n";
        std::exit(EXIT_FAILURE);
    }

    if (m_replicasNPs.at(0).empty()) {
        std::cerr << "Parser::ProcessBootstrapsNPs: ERROR: Replicas at(0) are empty\n";
        std::exit(EXIT_FAILURE);
    }

    const std::size_t totalSize = std::accumulate(m_nps.begin(), m_nps.end(), 0);

    std::vector<TMatrixDSym> npCovMatrices;
    for (std::size_t imeas = 0; imeas < m_npConstraint.size(); ++imeas) {
        TMatrixDSym cov(m_npConstraint.at(imeas).size());
        for (std::size_t i = 0; i < m_npConstraint.at(imeas).size(); ++i) {
            for (std::size_t j = i; j < m_npConstraint.at(imeas).size(); ++j) {
                auto itr_i = std::find(m_npNamesOrder.at(imeas).begin(), m_npNamesOrder.at(imeas).end(), m_npNames.at(imeas).at(i));
                auto itr_j = std::find(m_npNamesOrder.at(imeas).begin(), m_npNamesOrder.at(imeas).end(), m_npNames.at(imeas).at(j));
                if (itr_i == m_npNamesOrder.at(imeas).end()) {
                    std::cerr << "Parser::ProcessBootstrapsNPs: ERROR: Cannot find " << m_npNamesOrder.at(imeas).at(i) << " in the list of NPs\n";
                    std::exit(EXIT_FAILURE);
                }
                if (itr_j == m_npNamesOrder.at(imeas).end()) {
                    std::cerr << "Parser::ProcessBootstrapsNPs: ERROR: Cannot find " << m_npNamesOrder.at(imeas).at(j) << " in the list of NPs\n";
                    std::exit(EXIT_FAILURE);
                }
                const std::size_t index_i = std::distance(m_npNamesOrder.at(imeas).begin(), itr_i);
                const std::size_t index_j = std::distance(m_npNamesOrder.at(imeas).begin(), itr_j);
                const double sigma_i = m_npConstraint.at(imeas).at(i);
                const double sigma_j = m_npConstraint.at(imeas).at(j);
                const double corr = m_npCorrelations.at(imeas)(index_i,index_j);
                cov(i,j) = sigma_i * corr * sigma_j; 
                if (i != j) {
                    cov(j,i) = sigma_i * corr * sigma_j;
                }
            }
        }
        npCovMatrices.emplace_back(cov.GetNrows());
        npCovMatrices.back() = cov;
    }

    TMatrixDSym covariance(totalSize);
    // add the correlations that we can from the individual measurements
    {
        std::size_t offset(0);
        for (std::size_t icov = 0; icov < npCovMatrices.size(); ++icov) {
            for (int i = 0; i < npCovMatrices.at(icov).GetNrows(); ++i) {
                for (int j = 0; j < npCovMatrices.at(icov).GetNrows(); ++j) {
                    covariance[offset+i][offset+j] = npCovMatrices.at(icov)[i][j];
                }
            }
            offset += m_nps.at(icov);
        }
    }

    // need to get the cross-correlations
    for (std::size_t i = 0; i < m_nps.size(); ++i) {
        for (std::size_t j = i + 1; j < m_nps.size(); ++j) {
            for (std::size_t ibin = 0; ibin < m_nps.at(i); ++ibin) {
                for (std::size_t jbin = 0; jbin < m_nps.at(j); ++jbin) {
                    // use: cov = sigma_i * corr * sigma_j
                    const double sigma_i = std::sqrt(npCovMatrices.at(i)[ibin][ibin]);
                    const double sigma_j = std::sqrt(npCovMatrices.at(j)[jbin][jbin]);

                    std::size_t offsetX(0);
                    for (std::size_t index = 0; index < j;  ++index) {
                        offsetX += m_nps.at(index);
                    }

                    std::size_t offsetY(0);
                    for (std::size_t index = 0; index < i;  ++index) {
                        offsetY += m_nps.at(index);
                    }
                    const std::size_t indexX = offsetX + jbin;
                    const std::size_t indexY = offsetY + ibin;
                    double corr = this->CalculateBootsrapCorrelation(GetVector(m_replicasNPs.at(i), ibin), GetVector(m_replicasNPs.at(j), jbin));
                    if ((i != j) && (std::abs(corr) > 1-m_inversionPrecision)) {
                        std::cout << "Parser::ProcessBootstrapsCorrelated: WARNING: CovMatrix(" << indexX << "," << indexY << ") has correlations too close to 1\n";
                        std::cout << "Parser::ProcessBootstrapsCorrelated: WARNING: You use the same name of the NPs instead of trying to correlate them by 100%\n";
                        std::cout << "Parser::ProcessBootstrapsCorrelated: WARNING: Setting the correlation to " << (corr > 0 ? 1-m_inversionPrecision : -1+m_inversionPrecision) <<"\n";
                        corr = corr > 0 ? 1-m_inversionPrecision : -1+m_inversionPrecision;
                    }
                    const double cov = sigma_i * corr * sigma_j;

                    covariance[indexX][indexY] = cov;
                    if (indexX != indexY) {
                        covariance[indexY][indexX] = cov;
                    }
                }
            }
        }
    }

    if (m_debug && (covariance.GetNrows() < 10)) {
        std::cout << "Parser::ProcessBootstrapsNPs: Printing the total NP correlation matrix\n";
        const auto correlation = Fitter::CovarianceToCorrelation(covariance);
        correlation.Print();
        std::cout << "Parser::ProcessBootstrapsNPs: Printing the total NP covariance matrix\n";
        covariance.Print();
    }
    
    m_fitter->SetTotalNPCovarianceCorrelated(covariance);
}

void Parser::ProcessCovarianceMatrixFromReplicas() {
    if (m_replicas.empty()) {
        std::cerr << "Parser::ProcessCovarianceMatrixFromReplicas: ERROR: Replicas are empty!\n";
        std::exit(EXIT_FAILURE);
    }

    TMatrixDSym cov(m_pois.size());
    const int size = cov.GetNrows();

    for (int i = 0; i < size; ++i) {
        for (int j = i; j < size; ++j) {
            const double corr = this->CalculateBootsrapCorrelation(GetVector(m_replicas.back(), i), GetVector(m_replicas.back(), j));
            const double sigma_i = this->CalculateRMS(GetVector(m_replicas.back(), i));
            const double sigma_j = this->CalculateRMS(GetVector(m_replicas.back(), j));

            const double covariance = sigma_i * corr * sigma_j;
            cov(i,j) = covariance;
            if (i != j) {
                cov(j,i) = covariance;
            }
        }
    }

    if (m_debug) {
        std::cout << "Parser::ProcessCovarianceMatrixFromReplicas: Printing the calculated covariance matrix from the replicas for measaurement " << m_bins.size() << "\n";
        cov.Print();
    }

    m_statCovMatrices.emplace_back(cov);
    m_statCovMatrices.back() = cov;
}
  
void Parser::ReadBootstraps(const YAML::Node& node) {
    if (!node["bootstrapReplicaFile"] || !node["bootstrapReplicaTree"] || !node["bootstrapReplicaVariables"]) {
        std::cerr << "Parser::ReadBootstraps: ERROR: Missing at least one of the following settings: \"bootstrapReplicaFile\", \"bootstrapReplicaTree\" or \"bootstrapReplicaVariables\"\n";
        std::exit(EXIT_FAILURE); 
    }

    const std::string path     = node["bootstrapReplicaFile"].as<std::string>();
    const std::string treeName = node["bootstrapReplicaTree"].as<std::string>();

    std::shared_ptr<TFile> file = this->OpenRootFile(path);
    TTree* tree = file->Get<TTree>(treeName.c_str());
    if (!tree) {
        std::cerr << "Parser::ReadBootstraps: ERROR: Tree: " << treeName << " is not present in the file: " << path << "\n";
        std::exit(EXIT_FAILURE);
    }
    const long int nReplicas = tree->GetEntries();
    if (nReplicas <= 0) {
        std::cerr << "Parser::ReadBootstraps: ERROR: Tree " << treeName << " has <= 0 events\n";
        std::exit(EXIT_FAILURE);
    }

    for (const auto& ireplica : m_replicas) {
        if (static_cast<int>(ireplica.size()) != nReplicas) {
            std::cerr << "Parser::ReadBootstraps: ERROR: Mismatch between the replica sizes!\n";
            std::exit(EXIT_FAILURE);
        }
    }    

    const auto variables = node["bootstrapReplicaVariables"];
    std::map<std::string, std::string> poiMap;
    for (const auto& ivar : variables) {
        if (!ivar["parameter"] || !ivar["branchName"]) {
            std::cerr << "Parser::ReadBootstraps: ERROR: bootstrapReplicaVariables requires \"parameter\" and \"branchName\"\n";
            std::exit(EXIT_FAILURE);
        }
        const std::string key = ivar["parameter"].as<std::string>();
        const std::string value = ivar["branchName"].as<std::string>();
        poiMap.insert({key, value}); 
    }
    std::vector<std::string> orderedBranches;
    for (const auto& ipoi : m_pois) {
        auto itr = poiMap.find(ipoi);
        if (itr == poiMap.end()) {
            std::cerr << "Parser::ReadBootstraps: ERROR: POI " << ipoi << " is not in the bootstrap map\n";
            std::exit(EXIT_FAILURE);
        }
        orderedBranches.emplace_back(itr->second);
    }

    if (m_useCorrelatedNPs) {
        for (const auto& inp : m_npPerMeas) {
            auto itr = poiMap.find(inp);
            if (itr == poiMap.end()) {
                std::cerr << "Parser::ReadBootstraps: ERROR: NP " << inp << " is not in the bootstrap map\n";
                std::exit(EXIT_FAILURE);
            }
            orderedBranches.emplace_back(itr->second);
        }
    }

    const std::vector<std::vector<double> > replicas = this->ReadTree(tree, orderedBranches);
    std::vector<std::vector<double> > replicasPOI;
    std::vector<std::vector<double> > replicasNPs;
    for (std::size_t itoy = 0; itoy < replicas.size(); ++itoy) {
        std::vector<double> pois;
        std::vector<double> nps;
        for (std::size_t i = 0; i < replicas.at(itoy).size(); ++i) {
            if (i < m_pois.size()) {
                pois.emplace_back(replicas.at(itoy).at(i));
            } else {
                nps.emplace_back(replicas.at(itoy).at(i));
            }
        }
        replicasPOI.emplace_back(pois);
        replicasNPs.emplace_back(nps);
    }
    m_replicas.emplace_back(replicasPOI);
    m_replicasNPs.emplace_back(replicasNPs);
}

void Parser::ReadCrossNPs(const YAML::Node& node) {
    if (!node["crossNuisanceParameters"]) return;

    const auto& npNode = node["crossNuisanceParameters"];
    YAML::Node newNode{};
    if (npNode["configFile"]) {
        newNode = Parser::OpenConfigFile(npNode["configFile"].as<std::string>());     
    } else {
        newNode = npNode;
    }
    if (!newNode["names"]) {
        std::cerr << "Parser::ReadCrossNPs: ERROR: No \"names\" provided\n";
        std::exit(EXIT_FAILURE);
    }

    if (!newNode["names"].IsSequence()) {
        std::cerr << "Parser::ReadCrossNPs: ERROR:  \"names\" is not a sequence\n";
        std::exit(EXIT_FAILURE);
    }

    const auto& nameNode = newNode["names"];
    std::vector<std::string> names;
    for (const auto& iname : nameNode) {
        names.emplace_back(iname.as<std::string>());
    }

    m_fitter->SetCrossNPNames(names);

    // correlation matrix
    const std::vector<std::vector<double> > cor = this->GetCovarianceMatrix(newNode, "correlationMatrix", "");
    if (cor.size() != names.size()) {
        std::cerr << "Parser::ReadCrossNPs: ERROR: Inconsistent sizes of the correlation matrix and the NP names\n";
        std::exit(EXIT_FAILURE);
    }

    if (!Parser::CheckCorrelationMatrix(cor)) {
        std::cerr << "Parser::ReadCrossNPs: ERROR: Correlation matrix has values outside of <-1,1>\n";
        std::exit(EXIT_FAILURE);
    }

    TMatrixDSym corMatrix = Parser::VectorToMatrix(cor);

    m_fitter->SetCrossNPCorrelations(corMatrix);
}

void Parser::ReadCreateWS(const YAML::Node& node) {
    if (!node["createWS"]) {
        m_fitter->SetCreateWS(true);
        m_createWS = true;
        return;
    }

    m_fitter->SetCreateWS(node["createWS"].as<bool>());
    m_createWS = node["createWS"].as<bool>();
}

void Parser::ReadDebug(const YAML::Node& node) {
    if (!node["debug"]) {
        m_debug = false;
    } else {
        m_debug = node["debug"].as<bool>();
    }

    m_fitter->SetDebug(m_debug);
}

void Parser::ReadCompareMeasurements(const YAML::Node& node) {
    auto compare = node["compareMeasurements"];
    std::vector<std::string> measurements;
    if (!compare) {
        // add the first two measurements
        measurements.emplace_back(m_measurements.at(0));
        if (m_measurements.size() > 1) {
            measurements.emplace_back(m_measurements.at(1));
        }
    } else {
        if (!compare.IsSequence()) {
            std::cerr << "Parser::ReadCompareMeasurements: WARNING: \"compareMeasurements\" is not a sequence, ignoring!\n";
            return;
        }
        for (const auto& imeas : compare) {
            if (std::find(m_measurements.begin(), m_measurements.end(), imeas.as<std::string>()) == m_measurements.end()) {
                std::cerr << "Parser::ReadCompareMeasurements: WARNING: Provided measurement: " << imeas.as<std::string>() << " is not known, ignoring\n";
                continue;
            }
            measurements.emplace_back(imeas.as<std::string>());
        }
    }

    m_fitter->SetCompareMeasurements(measurements);
}
  
void Parser::ReadCorrelationThreshold(const YAML::Node& node) {
    auto cor = node["correlationMatrixThreshold"];
    if (!cor) return;

    const double correlation = cor.as<double>();
    if (correlation < 0 || correlation > 1) {
        std::cout << "Parser::ReadCorrelationThreshold: WARNING: \"correlationMatrixThreshold\" is < 0 or > 1, setting to default (0.1)\n";
        return;
    }

    m_fitter->SetCorrelationThreshold(correlation);
}

void Parser::ReadDraw(const YAML::Node& node) {
    if (!node["draw"]) {
        m_fitter->SetDraw(true);
    } else {
        m_fitter->SetDraw(node["draw"].as<bool>());
    }
}

void Parser::ReadFitStrategy(const YAML::Node& node) {
    if (!node["fitStrategy"]) {
        m_fitter->SetFitStrategy(1);
        return;
    }

    const int strategy = node["fitStrategy"].as<int>();
    if (strategy < 0 || strategy > 2) {
        std::cout << "Parser::ReadFitStrategy: WARNING: fitStrategy < 0 or > 2. Setting to 1.\n";
        m_fitter->SetFitStrategy(1);
        return;
    }

    m_fitter->SetFitStrategy(strategy);
}

void Parser::ReadFixNPsToFitted(const YAML::Node& node) {
    if (!node["fixNPsToFitted"]) return;

    m_fitter->SetFixNPsToFitted(node["fixNPsToFitted"].as<bool>());
}

void Parser::ReadFixParams(const YAML::Node& node) {
    if (!node["fixParams"]) return;

    for (const auto& iparam : node["fixParams"]) {
        if (!iparam["parameter"]) {
            std::cerr << "Parser::ReadFixParams: ERROR: The fixParams node has no \"parameter\" entry\n";
            std::exit(EXIT_FAILURE);
        }
        if (!iparam["value"]) {
            std::cerr << "Parser::ReadFixParams: ERROR: The fixParams node has no \"value\" entry\n";
            std::exit(EXIT_FAILURE);
        }
        const std::string param = iparam["parameter"].as<std::string>();
        const double value = iparam["value"].as<double>();
        m_fitter->AddFixParam(param, value);
    }
}

void Parser::ReadFractionToCov(const YAML::Node& node) {
    if (!node["fractionToCov"]) return;

    double fraction = node["fractionToCov"].as<double>();

    if (fraction < 0 || fraction > 1) {
        std::cout << "Parser::ReadFractionToCov: WARNING: fractionToCov is < 0 or > 1, setting to 0";
        fraction = 0;
    }

    m_fractionToCov = fraction;
}

void Parser::ReadFractionToCovPOI(const YAML::Node& node) {
    if (!node["fractionToCovPOI"]) return;

    m_fractionToCovPOI = node["fractionToCovPOI"].as<std::string>();
}

void Parser::ReadInversionPrecision(const YAML::Node& node) {
    if (!node["inversionPrecision"]) return;

    const double prec = node["inversionPrecision"].as<double>();
    if (prec < 0 || prec > 1) {
        std::cout << "Parser::ReadInversionPrecision: WARNING: Provided precision is > 1 or < 0. Setting to 1e-3\n";
        return;
    }
    m_inversionPrecision = prec;
    m_fitter->SetInversionPrecision(m_inversionPrecision);
}

std::pair<bool, std::vector<std::string> > Parser::ReadMinos(const YAML::Node& node) {
    std::vector<std::string> params{};
    if (!node["minos"]) return std::make_pair(false, params);
    const bool use = node["minos"].as<bool>();
    if (!use) {
        return std::make_pair(use, params);
    }
    if (!node["minosParameters"]) {
        return std::make_pair(use, params);
    } else {
        const auto& paramNode = node["minosParameters"];
        if (!paramNode.IsSequence()) {
            std::cerr << "Parser::ReadMinos: \"minosParameters\" is not a sequence\n";
            exit(EXIT_FAILURE);
        }
        for (const auto& iparam : paramNode) {
            params.emplace_back(iparam.as<std::string>());
        }
        return std::make_pair(use, params);
    }
}
  
bool Parser::ReadNuisanceParameters(const YAML::Node& node, const std::size_t nSyst, const std::string& measurement) {
    if (node["nuisanceParameters"]) {
        if (!this->CheckNuisanceParameterNode(node["nuisanceParameters"])) {
            std::cerr << "Parser::ReadNuisanceParameters: ERROR: Unsupported options provided for nuisanceParameters in measurement: " << measurement << "\n";
            std::exit(EXIT_FAILURE);
        }
        m_npCorrelations.emplace_back(GetNuisanceParameters(node["nuisanceParameters"], nSyst, measurement));
        m_fitter->AddNPCorrelation(m_npCorrelations.back());
        return true;
    } else {
        /// pass correlation matrix = unity 
        TMatrixDSym cor(nSyst);
        for (std::size_t i = 0; i < nSyst; ++i) {
            for (std::size_t j = 0; j < nSyst; ++j) {
                cor(i,j) = (i == j) ? 1.0 : 0.;
            }
        } 

        m_npCorrelations.emplace_back(cor);
        m_fitter->AddNPCorrelation(cor);
        m_fitter->AddNPCorrelationNames({});

        return false;
    }
}

std::vector<std::string> Parser::ReadPearsonChi2(const YAML::Node& node) const {
    std::vector<std::string> result;
    if (!node["pearsonChi2"]) return result;

    if (!node["pearsonChi2"].IsSequence()) {
        std::cerr << "Parser::ReadPearsonChi2: ERROR: \"pearsonChi2\" is not a sequence\n";
        exit(EXIT_FAILURE);
    }

    const auto& pearson = node["pearsonChi2"];
    for (const auto& ipoi : pearson) {
        result.emplace_back(ipoi.as<std::string>());
    }

    return result;
}

void Parser::ReadPrintParameters(const YAML::Node& node) const {
    if (!node["printParameters"]) return;

    if (!node["printParameters"].IsSequence()) {
        std::cerr << "Parser::ReadPrintParameters:: ERROR \"printParameters\" is not a sequence\n";
        exit(EXIT_FAILURE);
    }

    const auto& params = node["printParameters"];
    std::vector<std::string> result;
    for (const auto& iparam : params) {
        result.emplace_back(iparam.as<std::string>());
    }

    m_fitter->SetPrintParameters(result);
}

void Parser::ReadPrintFormat(const YAML::Node& node) {
    if (!node["printFormat"]) return;

    auto print = node["printFormat"];
    if (!print.IsSequence()) {
        std::cerr << "Parser::ReadPrintFormat: WARNING: \"printFormat\" is not a sequence, ignoring\n";
        return;
    }

    std::vector<std::string> format;
    for (const auto& iformat : print) {
        format.emplace_back(iformat.as<std::string>());
    }

    m_fitter->SetPrintFormat(std::move(format));
}

void Parser::ReadRanges(const YAML::Node& node) {
    if (!node["parameterRange"]) {
        std::cerr << "Parser::ReadRanges: ERROR \"parameterRange\" config not provided\n";
        std::exit(EXIT_FAILURE);
    }

    for (const auto& inode : node["parameterRange"]) {
        if (!inode["parameter"] || !inode["min"] || !inode["max"]) {
            std::cerr << "Parser::ReadRanges: ERROR Either \"parameter\", \"min\" or \"max\" is not provided for \"parameterRange\"\n";
            std::exit(EXIT_FAILURE);
        }

        m_fitter->AddParameterRange(inode["parameter"].as<std::string>(), inode["min"].as<double>(), inode["max"].as<double>());
    }
}

void Parser::ReadRanking(const YAML::Node& node) const {
    if (!node["ranking"]) return;

    m_fitter->SetRanking(node["ranking"].as<bool>());
}

void Parser::ReadRemoveCovarianceTerms(const YAML::Node& node) {
    if (!node["removeNonConstrainedCovarianceTerms"]) return;

    m_fitter->SetRemoveCovTerms(node["removeNonConstrainedCovarianceTerms"].as<bool>());
}

void Parser::ReadReparametrisation(const YAML::Node& node) {
    if (!node["reparametrisation"]) return;
    
    const auto& reparamNode = node["reparametrisation"];
    YAML::Node newNode{};
    if (reparamNode["configFile"]) {
        newNode = Parser::OpenConfigFile(reparamNode["configFile"].as<std::string>());
    } else {
        newNode = reparamNode;
    }

    const auto& formulae = newNode["formulae"];
    Parametrisation parametrisation{};
    if (!formulae) {
        std::cerr << "Parser::ReadReparametrisation: ERROR: Missing \"formulae\" setting";
        std::exit(EXIT_FAILURE);
    }
    for (const auto& inode : formulae) {
        if (!inode["replacementFor"] || !inode["formula"]) {
            std::cerr << "Parser::ReadReparametrisation: ERROR: Missing \"replacementFor\" or \"formula\"\n";
            std::exit(EXIT_FAILURE);
        }

        parametrisation.AddFormula(inode["replacementFor"].as<std::string>(), inode["formula"].as<std::string>());
    }

    const auto& dependencies = newNode["dependency"];
    if (!dependencies) {
        std::cerr << "Parser::ReadReparametrisation: ERROR: Missing \"dependency\" setting";
        std::exit(EXIT_FAILURE);
    }
    for (const auto& idep : dependencies) {
        if (!idep["parameter"] || !idep["mean"] || !idep["min"] || !idep["max"]) {
            std::cerr << "Parser::ReadReparametrisation: ERROR: Missing option \"parameter\", \"mean\", \"min\" or \"max\" for dependency\n";
            std::exit(EXIT_FAILURE);
        }
        Parameter par(idep["parameter"].as<std::string>(), idep["mean"].as<double>(), idep["min"].as<double>(), idep["max"].as<double>());
        parametrisation.AddParameter(par);
    }
    m_fitter->SetReparametrisation(parametrisation);
}

void Parser::ReadRankingNP(const YAML::Node& node) const {
    if (!node["rankingNP"]) return;

    m_fitter->SetRankingNP(node["rankingNP"].as<std::string>());
}

void Parser::ReadSkipFit(const YAML::Node& node) const {
    if (!node["skipFit"]) return;

    m_fitter->SetSkipFit(node["skipFit"].as<bool>());
}

void Parser::ReadThreads(const YAML::Node& node) const {
    if (!node["threads"]) {
        m_fitter->SetNthreads(1);
    } else {
        const int thread = node["threads"].as<int>();
        if (thread < 0) {
            std::cerr << "Parser::ReadThreads: WARNING: Number of threads is < 0, setting to 1.\n";
            m_fitter->SetNthreads(1);
            return;
        }
        m_fitter->SetNthreads(thread);
    }
}

void Parser::ReadSingleMeasurement(const YAML::Node& node, const std::string& name) {

    /// read poi names
    if (!node["poiNames"]) {
        std::cerr << "Parser::ReadSingleMeasurement: ERROR: No \"poiNames\" provided in measurement: " << name << "\n";
        std::exit(EXIT_FAILURE);
    }

    if (!node["poiNames"].IsSequence()) {
        std::cerr << "Parser::ReadSingleMeasurement: ERROR: \"poiNames\" is not a sequence for measurement: " << name << "\n";
        std::exit(EXIT_FAILURE);
    }

    m_pois.clear();
    m_npPerMeas.clear();

    const auto& poiName = node["poiNames"];
    for (const auto& iname : poiName) {
        m_pois.emplace_back(iname.as<std::string>());
    }

    /// read values
    const std::vector<double> values = this->GetValues(node, "values", name);
    if (values.size() != m_pois.size()) {
        std::cerr << "Parser::ReadSingleMeasurement: ERROR: Size of POI values and names do not match for measurement: " << name << "\n";
        std::exit(EXIT_FAILURE);
    }
    
    for (std::size_t i = 0; i < values.size(); ++i) {
        m_fitter->AddValuePOI(values.at(i), m_pois.at(i));
    }

    m_binSizes += values.size();
    m_bins.emplace_back(values.size());

    m_toCovariance.emplace_back(std::vector<double>(values.size(), 0.));

    if (!node["systematics"] && !m_statOnly) {
        std::cerr << "Parser::ReadSingleMeasurement: ERROR: No \"systematics\" provided for measurement: " << name << "\n";
        std::exit(EXIT_FAILURE);
    }

    this->ReadFractionToCovPOI(node);
    
    auto impactsAll = this->PreReadImpacts(node);

    // sort impact in the ascending order where the comaprison is done by taking the largest impact
    std::sort(impactsAll.begin(), impactsAll.end(), [this](const auto& lhs, const auto& rhs){
        if (this->m_fractionToCovPOI != "") {
            auto itr = std::find(this->m_pois.begin(), this->m_pois.end(), this->m_fractionToCovPOI);
            if (itr == this->m_pois.end()) {
                std::cerr << "Parser::ReadSingleMeasurement: ERROR: fractionToCovPOI does not match any POIs!";
                std::exit(EXIT_FAILURE);
            }

            const std::size_t index = std::distance(this->m_pois.begin(), itr); 
            return std::abs(lhs.second.at(index)) < std::abs(rhs.second.at(index));
        } else {
            auto maxAbs = [](const double a, const double b){return std::abs(a) < std::abs(b);};
            const double maxLHS = *std::max_element(lhs.second.begin(), lhs.second.end(), maxAbs);
            const double maxRHS = *std::max_element(rhs.second.begin(), rhs.second.end(), maxAbs);
            return std::abs(maxLHS) < std::abs(maxRHS);
        }
    });

    // take only the small ones
    impactsAll.resize(m_fractionToCov*impactsAll.size());

    m_fitter->SetStatOnly(m_statOnly);
    if (m_statOnly) {
        m_hasNuisanceParameters.emplace_back(false);
        m_fitter->AddNPNames({});
        m_fitter->AddNPConstraints({});
        m_fitter->AddPulls({});
    } else {
        std::size_t nSyst(0);
        std::vector<std::string> npNames;
        std::vector<double> constraints;
        std::vector<double> pulls;
        std::vector<std::string> skippedNPs;
        std::vector<std::string> categories;
        for (const auto& isyst : node["systematics"]) {
            if (!this->CheckSystematicsNode(isyst)) {
                std::cerr << "Parser::ReadSingleMeasurement: ERROR: Unsupported options provided for systematics for measurement: " << name << "\n";
                std::exit(EXIT_FAILURE);
            }
            if (!isyst["parameter"]) {
                std::cerr << "Parser::ReadSingleMeasurement: ERROR: No \"parameter\" provided for measurement: " << name << "\n";
                std::exit(EXIT_FAILURE);
            }

            const std::string param = isyst["parameter"].as<std::string>();
            const std::vector<double> impacts = this->GetValues(isyst, "impact", param);

            if (impacts.size() != values.size()) {
                std::cerr << "Parser::ReadSingleMeasurement: ERROR: Size of \"impact\" and \"values\" do not match for parameter: " << param << " for measurement: " << name << "\n";
                std::exit(EXIT_FAILURE);
            }

            double constraint(1.);
            if (isyst["constraint"]) {
                constraint = isyst["constraint"].as<double>();
                if (constraint < 0. || constraint > 1.) {
                    std::cout << "Parser::ReadSingleMeasurement: WARNING: Constraint for systematics: " << param << " in measurement: " << name << " is smaller than 0 or larger than 1, setting to 1.\n";
                    constraint = 1.;
                }
            }

            // is the small one in the list?
            auto itr = std::find_if(impactsAll.begin(), impactsAll.end(), [&param](const auto& element){return param == element.first;});
            const bool isSmall = (itr != impactsAll.end());

            if (isyst["addToCovariance"] || isSmall) {
                if (isSmall || (isyst["addToCovariance"] && isyst["addToCovariance"].as<bool>())) {
                    if (m_debug) {
                        std::cout << "Parser::ReadSingleMeasurement: Adding systematic: " << param << " to the covariance matrix in measurement " << name << " \n";
                    }
                    // add the values to be added to covariance
                    for (std::size_t ivalue = 0; ivalue < impacts.size(); ++ivalue) {
                        const double value = m_toCovariance.back().at(ivalue);
                        // add in squares
                        m_toCovariance.back().at(ivalue) = std::hypot(value, impacts.at(ivalue));
                    }
                    skippedNPs.emplace_back(param);

                    // skip the rest of the systematics processing
                    continue;
                }
            }

            const std::string category = isyst["category"] ? isyst["category"].as<std::string>() : "Other";
            auto itrSkippedNPs = std::find(skippedNPs.begin(), skippedNPs.end(), param);
            if (itrSkippedNPs == skippedNPs.end()) {
                categories.emplace_back(category);
                auto itrCategory = std::find(m_npUniqueCategories.begin(), m_npUniqueCategories.end(), category);
                if (itrCategory == m_npUniqueCategories.end()) {
                    m_npUniqueCategories.emplace_back(category);
                }
	    }

            constraints.emplace_back(constraint);
            const double pull = (isyst["pull"]) ? isyst["pull"].as<double>() : 0.;
            pulls.emplace_back(pull);

            m_fitter->AddSystematic(param, impacts, m_binSizes);
            npNames.emplace_back(param);
            m_npPerMeas.emplace_back(param);
            ++nSyst;
        }
        if (!skippedNPs.empty()) m_skippedNPs.emplace_back(skippedNPs);
        m_fitter->AddSkippedNPs(skippedNPs);
        m_npNames.emplace_back(m_npPerMeas);
        m_fitter->AddNPCategories(categories);
        m_npConstraint.emplace_back(constraints);
        m_nps.emplace_back(nSyst);
        const bool hasNPs = this->ReadNuisanceParameters(node, nSyst+skippedNPs.size(), name);
        m_hasNuisanceParameters.emplace_back(hasNPs);
        m_fitter->AddNPNames(npNames);
        m_fitter->AddNPConstraints(constraints);
        m_fitter->AddPulls(pulls);
    }

    /// must be provided in each measurement if not in general
    if (!m_hasStatInGeneral) {
        if (m_createWS) {
            this->ReadBootstraps(node);
        }
        const auto cov = this->GetCovarianceMatrix(node, "statCovMatrix", name);
        if (cov.empty()) {
            if (!node["bootstrapReplicaFile"]) {
                std::cerr << "Parser::ReadSingleMeasurement: ERROR: No valid stat covariance matrix or bootstrap files found for measurement: " << name << "\n";
                std::exit(EXIT_FAILURE);
            } else {
                this->ProcessCovarianceMatrixFromReplicas();
            }
        } else {
            m_statCovMatrices.emplace_back(Parser::VectorToMatrix(cov));
        }
    }
}

bool Parser::ReadStatCovarianceMatrix(const YAML::Node& node, const std::string& name) {
    const std::vector<std::vector<double> > values = this->GetCovarianceMatrix(node, "statCovMatrix", name);

    if (values.empty()) {
        return false;
    }

    TMatrixDSym cov = Parser::VectorToMatrix(values);

    m_fitter->SetStatCovariance(cov);

    return true;
}

void Parser::ReadStatNPCorrelations(const YAML::Node& node) {
    if (!node["statNPCorrelations"]) return;

    const auto& npNode = node["statNPCorrelations"];
    YAML::Node newNode{};
    if (npNode["configFile"]) {
        newNode = Parser::OpenConfigFile(npNode["configFile"].as<std::string>());
    } else {
        newNode = npNode;
    }

    const auto corVec = this->GetCovarianceMatrix(newNode, "correlationMatrix", "");
    if (!Parser::CheckCorrelationMatrix(corVec)) {
        std::cerr << "Parser::ReadStatNPCorrelations: ERROR: Matrix has values outside of <-1,1>\n";
        std::exit(EXIT_FAILURE);
    }

    const TMatrixDSym cor = Parser::VectorToMatrix(corVec);
    m_byHandNPcorrelations.ResizeTo(cor);
    m_byHandNPcorrelations = cor;
}

bool Parser::ReadStatOnly(const YAML::Node& node) const {
    if (!node["statOnly"]) return false;

    return node["statOnly"].as<bool>();
} 

bool Parser::ReadUseCorrelatedNPs(const YAML::Node& node) {
    if (!node["useCorrelatedNPs"]) return false;

    return node["useCorrelatedNPs"].as<bool>();
}

std::string Parser::ReadOutputPath(const YAML::Node& node) const {
    if (node["outputPath"]) {
        return node["outputPath"].as<std::string>();
    }

    return "";
}

std::vector<std::vector<double> > Parser::ReadTree(TTree* tree, const std::vector<std::string>& branches) {
    std::vector<std::vector<double> > result;
    std::vector<double> values(branches.size());
    for (std::size_t i = 0; i < values.size(); ++i) {
        tree->SetBranchAddress(branches.at(i).c_str(), &values.at(i));
    }

    for (int ievent = 0; ievent < tree->GetEntries(); ++ievent) {
        tree->GetEntry(ievent);
        result.emplace_back(values);
    }

    return result;
}

TMatrixDSym Parser::VectorToMatrix(const std::vector<std::vector<double> >& values) {
    TMatrixDSym cov(values.size());
    for (std::size_t i = 0; i < values.size(); ++i) {
        for (std::size_t j = 0; j < values.at(i).size(); ++j) {
            cov[i][j] = values.at(i).at(j);
            if (i !=j ) {
                cov[j][i] = values.at(i).at(j);
            }
        }
    }

    return cov;
}

