#include "include/Parametrisation.h"

#include "RooRealVar.h"

#include <algorithm>
#include <iostream>

Parametrisation::Parametrisation()
{
}

RooArgList Parametrisation::GetDependencies(const std::string& poi) const {
    RooArgList result;

    auto itr = std::find_if(m_formulae.begin(), m_formulae.end(), [&poi](const auto& formula){return formula.first == poi;});
    if (itr == m_formulae.end()) {
        std::cerr << "Parametrisation::GetDependencies: ERROR: Cannot find POI: " << poi << " in the reparametrisation\n";
        std::exit(EXIT_FAILURE);
    }

    const std::string& formula = itr->second;

    for (const auto& i : m_parameters) {
        if (formula.find(i.Name()) == std::string::npos) continue;
        RooRealVar var(i.Name().c_str(), i.Name().c_str(), i.Mean(), i.Min(), i.Max());
        result.addClone(std::move(var));
    }

    return result;
}

std::vector<std::string> Parametrisation::GetDependencyNames() const {
    std::vector<std::string> result;

    for (const auto& i : m_parameters) {
        result.emplace_back(i.Name());
    }

    return result;
}

std::string Parametrisation::GetFormula(const std::string& poi) const {
    auto itr = std::find_if(m_formulae.begin(), m_formulae.end(), [&poi](const auto& formula){return formula.first == poi;});
    if (itr == m_formulae.end()) {
        std::cerr << "Parametrisation::GetFormula: ERROR: Cannot find POI: " << poi << " in the reparametrisation\n";
        std::exit(EXIT_FAILURE);
    }

    return itr->second;
}

bool Parametrisation::IsReparametrised(const std::string& poi) const {
    auto itr = std::find_if(m_formulae.begin(), m_formulae.end(), [&poi](const auto& formula){return formula.first == poi;});

    return (itr != m_formulae.end());
}

void Parametrisation::Print() const {
    for (const auto& iformula : m_formulae) {
        std::cout << "Parametrisation::Print: POI: " << iformula.first << ", reparametrised with: " << iformula.second << "\n";
    }
    for (const auto& iparameter : m_parameters) {
        std::cout << "Parametrisation::Print: Dependancies: parameter: " << iparameter.Name() << ", mean: "
        << iparameter.Mean() << ", min: " << iparameter.Min() << ", max: " << iparameter.Max() << "\n";
    }
}