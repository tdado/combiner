#include "include/Fitter.h"
#include "include/FitUtils.h"
#include "include/Plotter.h"
#include "include/Ranking.h"

#include "TFile.h"
#include "TH2.h"

#include "RooAbsReal.h"
#include "RooArgList.h"
#include "RooArgSet.h"
#include "RooDataSet.h"
#include "RooFitResult.h"
#include "RooFormulaVar.h"
#include "RooMinimizer.h"
#include "RooProdPdf.h"
#include "RooRealVar.h"
#include "RooStats/ModelConfig.h"
#include "RooWorkspace.h"

#include <chrono>
#include <fstream>
#include <future>
#include <iostream>
#include <iomanip>
#include <numeric>

Fitter::Fitter() :
    m_correlationThreshold(0.1),
    m_crossNPCorrelation(TMatrixDSym()),
    m_crossNPNames({}),
    m_createWS(true),
    m_debug(false),
    m_draw(true),
    m_fitStrategy(1),
    m_fixNPsToFitted(false),
    m_inversionPrecision(1e-3),
    m_npCorrelations({}),
    m_npInputConstraints({}),
    m_npNames({}),
    m_outPath(""),
    m_poiNames({}),
    m_poiNamesUnique({}),
    m_printFormat({"png"}),
    m_pulls({}),
    m_rankingNP(""),
    m_removeCovarianceTerms(true),
    m_runRanking(false),
    m_scaleCovMatrix(1.),
    m_skipFit(false),
    m_statCovariance(TMatrixDSym()),
    m_statOnly(false),
    m_systematicMap({}),
    m_threadN(1),
    m_useCorrelatedNPs(false),
    m_useMinos({true,{}}),
    m_usePearsonChi2(false),
    m_values({}),
    m_valuesUnique({})
{
}

void Fitter::AddFixParam(const std::string& param, const double value) {
    auto it = std::find_if(m_fixParams.begin(), m_fixParams.end(), [&param](const auto& item){return item.first == param;});
    if (it != m_fixParams.end()) {
        std::cout << "Fitter::AddFixParam: WARNING: Parameter " << param << " already in the list of parameters to fix, ignoring.\n";
        return;
    }

    m_fixParams.emplace_back(std::make_pair(param, value));
}

void Fitter::AddParameterRange(const std::string& name, const double min, const double max) {
    auto itr = m_parameterRanges.find(name);
    if (itr != m_parameterRanges.end()) {
        std::cout << "Fitter::AddParameterRange: WARNING: Parameter " << name << " already in the list of POI ranges, ignoring\n";
        return;
    }

    m_parameterRanges.insert(std::make_pair(name, std::make_pair(min, max)));
}

void Fitter::AddSystematic(const std::string& param,
                           const std::vector<double>& impact,
                           const std::size_t totalSize) {
    auto itr = m_systematicMap.find(param);
    if (itr != m_systematicMap.end()) {
        if (totalSize - impact.size() == itr->second.size()) {
            /// was filled regularly for each measurement
            itr->second.insert(itr->second.end(), impact.begin(), impact.end());
        } else {
            /// was filled e.g. in first and third but not second meaasurement
            if (totalSize < (itr->second.size() + impact.size())) {
                std::cerr << "Fitter::AddSystematic: ERROR: Inconsistent inputs\n";
                std::exit(EXIT_FAILURE);
            }
            const std::size_t size = totalSize - itr->second.size() - impact.size();

            /// need to add zeroes for the missing measurements
            std::vector<double> dummy(size, 0.);

            /// and add the new measurement
            dummy.insert(dummy.end(), impact.begin(), impact.end());

            itr->second.insert(itr->second.end(), dummy.begin(), dummy.end());
        }
    } else {
        if (totalSize == impact.size()) {
            m_systematicMap.insert({param, impact});
        } else {
            // we need to add zeroes at he beginning
            std::vector<double> dummy(totalSize - impact.size(), 0);
            dummy.insert(dummy.end(), impact.begin(), impact.end());
            m_systematicMap.insert({param, dummy});
        }
    }
}

void Fitter::AddValuePOI(const double value, const std::string& poi) {
    auto itr = std::find(m_poiNamesUnique.begin(), m_poiNamesUnique.end(), poi);
    /// always add POI
    m_poiNames.emplace_back(poi);
    m_values.emplace_back(value);
    if (itr == m_poiNamesUnique.end()) {
        /// only unique POIs
        m_poiNamesUnique.emplace_back(poi);
        m_valuesUnique.emplace_back(value);
    }
}

void Fitter::AddNPCorrelation(const TMatrixDSym& cor) {
    m_npCorrelations.emplace_back(TMatrixDSym(cor.GetNrows()));
    m_npCorrelations.back() = cor;
}

void Fitter::AdjustStatCovarianceMatrix() {

    if (m_statCovariance.Determinant() < 0) {
        std::cerr << "Fitter::AdjustStatCovarianceMatrix: ERROR: The determinant of the stat covariance matrix is negative: " << m_statCovariance.Determinant() << "\n";
        std::exit(EXIT_FAILURE);
    }

    while (m_statCovariance.Determinant() > 10e20) {
        // divide stat uncertainty by 10
        std::cout << "Fitter::AdjustStatCovarianceMatrix: Stat covariance matrix determinant is: " << m_statCovariance.Determinant() << ", which is > 10^20, scaling the stat uncertainty by a factor of 1/10\n";
        m_statCovariance *= 1e-2;
        m_scaleCovMatrix /= 10.;
    }
    
    while (m_statCovariance.Determinant() < 10e-20) {
        std::cout << "Fitter::AdjustStatCovarianceMatrix: Stat covariance matrix determinant is " << m_statCovariance.Determinant() << ", which is < 10^-20, scaling the stat uncertainty by a factor of 10\n";
        // multiply stat uncertainty by 10
        m_statCovariance *= 1e2;
        m_scaleCovMatrix *= 10.;
    }

    std::cout << "Fitter::AdjustStatCovarianceMatrix: The final determinant of the covariance matrix is: " << m_statCovariance.Determinant() << ", the scaling factor is: " << m_scaleCovMatrix << "\n";
}

void Fitter::AdjustImpactSizes() {
    /// add padding - zeroes - to impacts that are defined only for the first impact
    const std::size_t size = m_values.size();
    for (auto& isyst : m_systematicMap) {
        if (isyst.second.size() != size) {
            for (std::size_t isize = isyst.second.size(); isize < size; ++isize) {
                isyst.second.emplace_back(0.);
            }
        } 
    }
}

TMatrixDSym Fitter::CorrelationToCovariance(const TMatrixDSym& cor, const std::vector<double>& sigmas) {
    TMatrixDSym cov;
    cov.ResizeTo(cor);
    if (static_cast<int>(sigmas.size()) != cov.GetNrows()) {
        std::cerr << "Fitter::CorrelationToCovariance: ERROR: Inconsistent sized of correlation matrix and the sigma\n";
        std::exit(EXIT_FAILURE);
    }

    for (std::size_t i = 0; i < sigmas.size(); ++i) {
        for (std::size_t j = i; j < sigmas.size(); ++j) {
            const double sigma_i = sigmas.at(i);
            const double sigma_j = sigmas.at(j);
            cov(i,j) = sigma_i * cor(i,j) * sigma_j;
            if (i != j) {
                cov(j,i) = sigma_i * cor(i,j) * sigma_j;
            }
        }
    }

    return cov;
}

TMatrixDSym Fitter::CovarianceToCorrelation(const TMatrixDSym& cov) {
    TMatrixDSym cor;

    cor.ResizeTo(cov);

    std::vector<double> sigma(cov.GetNrows());

    for (int i = 0; i < cov.GetNrows(); ++i) {
        sigma[i] = std::sqrt(cov(i,i));
    }

    for (int i = 0; i < cov.GetNrows(); ++i) {
        for (int j = i; j < cov.GetNrows(); ++j) {
            const double correlation = cov(i,j)/(sigma.at(i)*sigma.at(j));
            cor(i,j) = correlation;
            if (i != j) {
                cor(j,i) = correlation;
            }
        }
    } 

    return cor;
}

void Fitter::PrintDebugInfo() const {
    std::cout << "Fitter::PrintDebugInfo: Stat covariance\n";
    m_statCovariance.Print();
    for (std::size_t i = 0; i < m_poiNames.size(); ++i) {
        std::cout << "Fitter::PrintDebugInfo: POI: " << m_poiNames.at(i) << ", pearson chi^2: " <<
        std::boolalpha << m_pearsonIndices.at(i) << std::noboolalpha << "\n";
    }
    std::cout << "\n";
    std::cout << "Fitter::PrintDebugInfo: POI input values\n";
    for (std::size_t ipoi = 0; ipoi < m_poiNames.size(); ++ipoi) {
        std::cout << "Fitter::PrintDebugInfo:   POI: " << m_poiNames.at(ipoi) << ", value: " << m_values.at(ipoi) << "\n";
    }
    if (m_totalNPcorrelation.GetNrows() > 0 && m_totalNPcorrelation.GetNrows() < 10) {
        std::cout << "\nFitter::PrintDebugInfo: NP cross-correlation matrix\n";
        m_totalNPcorrelation.Print();
    } else {
        std::cout << "\n";
    }
    std::cout << "Fitter::PrintDebugInfo: Printing NPs: \n";
    for (std::size_t i = 0; i < m_allNPs.size(); ++i) {
        std::cout << "Fitter::PrintDebugInfo:   NP: " << m_allNPs.at(i) << "\n";
    }
    std::cout << "\n";
    for (std::size_t ipoi = 0; ipoi < m_poiNames.size(); ++ipoi) {
        std::cout << "Fitter::PrintDebugInfo: POI " << ipoi << ", name: " << m_poiNames.at(ipoi) << "\n";
        for (const auto& ipull : m_poiPulls.at(ipoi)) {
            std::cout << "Fitter::PrintDebugInfo:\t  NP pull: " << ipull << "\n";
        } 
    }
    std::cout << "\n";
    if (m_statOnly) return;
    for (std::size_t imeas = 0; imeas < m_pulls.size(); ++imeas) {
        std::cout << "Fitter::PrintDebugInfo: Measurement: " << imeas << "\n";
        for (std::size_t inp = 0; inp < m_pulls.at(imeas).size(); ++inp) {
            std::cout << "Fitter::PrintDebugInfo:   NP: " << m_npNames.at(imeas).at(inp) <<
            ", pull: " << m_pulls.at(imeas).at(inp) << ", sigma: " << m_npInputConstraints.at(imeas).at(inp) << "\n";
        }
        if (!m_useCorrelatedNPs) {
            std::cout << "Fitter::PrintDebugInfo: Printing C^1 - I^1 matrix\n";
            m_cx.at(imeas).Print();
            std::cout << "Fitter::PrintDebugInfo: Printing (C^1 - I^1)^-1 * C^-1 matrix\n";
            m_cxInvC.at(imeas).Print();
            std::cout << "Fitter::PrintDebugInfo: Printing (C^1 - I^1)^-1 * C^-1 * tau\n";
            for (const auto i : m_totalPull.at(imeas)) {
                std::cout << "Fitter::PrintDebugInfo: " << i << "\n";
            }
        }
        std::cout << "\n";
    }
    if (m_useCorrelatedNPs && (m_cx_correlated.GetNrows() < 10)) {
        std::cout << "Fitter::PrintDebugInfo: Printing C^1 - K^1 matrix\n";
        m_cx_correlated.Print();
        std::cout << "Fitter::PrintDebugInfo: Printing (C^1 - K^1)^-1 * C^-1 matrix\n";
        m_cxInvC_correlated.Print();
        std::cout << "Fitter::PrintDebugInfo: Printing (C^1 - K^1)^-1 * C^-1 * tau\n";
        for (const auto i : m_totalPullCorrelated) {
            std::cout << "Fitter::PrintDebugInfo:   " << i << "\n";
        }
    }
}

RooArgList Fitter::GetGlobalObservables(const RooAbsPdf* model, const RooAbsData* data) const {
    RooArgList result;

    static const std::vector<std::string> matches = {"pull_POI_"};

    auto params = model->getParameters(*data);
    for (const auto iparam : *params) {
        const std::string name = iparam->GetName();
        for (const auto& imatch : matches) {
            if (name.find(imatch) != std::string::npos) {
                result.add(*iparam);
                break;
            }
        }
    }

    return result;
}

RooArgList Fitter::GetPOIs(const RooAbsPdf* model, const RooAbsData* data) {
    RooArgList result;
    m_poiNamesReparametrised.clear();

    std::vector<std::string> reparametrised = m_reparametrisation.GetDependencyNames();

    auto params = model->getParameters(*data);
    for (const auto iparam : *params) {
        const std::string name = iparam->GetName();
        auto itr = std::find(m_poiNamesUnique.begin(), m_poiNamesUnique.end(), name);
        if (itr != m_poiNamesUnique.end()) {
            result.add(*iparam);
            m_poiNamesReparametrised.emplace_back(name);
        }
        auto itrRep = std::find(reparametrised.begin(), reparametrised.end(), name);
        if (itrRep != reparametrised.end()) {
            result.add(*iparam);
            m_poiNamesReparametrised.emplace_back(name);
        }
    }

    return result;
}

RooArgList Fitter::GetNPs(const RooAbsPdf* model, const RooAbsData* data) {
    RooArgList result;

    static const std::vector<std::string> matches = {"pull_POI_", "pull_meas_", "pull_NP_"};
    const auto pois = this->GetPOIs(model, data);

    auto params = model->getParameters(*data);
    for (const auto iparam : *params) {
        const std::string name = iparam->GetName();
        if (pois.find(name.c_str())) continue;
        bool match(false);
        for (const auto& imatch : matches) {
            if (name.find(imatch) != std::string::npos) {
                match = true;
                break;
            }
        }
        if (!match) {
            result.add(*iparam);
        }
    }

    return result;
}

TMatrixDSym Fitter::MultiplyMatrices(const TMatrixDSym& a, const TMatrixDSym& b) {
    if (a.GetNrows() != b.GetNrows()) {
        std::cerr << "Fitter::MultiplyMatrices: ERROR: Matrices of different sizes\n";
        std::exit(EXIT_FAILURE);
    }

    const int size = a.GetNrows();

    TMatrixDSym result(size);
    for (int i = 0; i < size; ++i) {
        for (int j = i; j < size; ++j) {
            double sum(0);
            for (int k = 0; k < size; ++k) {
                sum += a(i,k)*b(k,j);
            }
            result(i,j) = sum;
            if (i != j) {
                result(j,i) = sum;
            }
        }
    }

    return result;
}

void Fitter::PrepareChi2() {
    std::cout << "Fitter::PrepareChi2: Preparing the workspace with the chi^2\n";
    auto start = std::chrono::steady_clock::now();
    RooArgList obs;

    std::unique_ptr<RooWorkspace> tmpWs = std::make_unique<RooWorkspace>("tmpWs");

    this->PrepareStatGaussian(&obs, tmpWs.get());
    
    if (!m_statOnly) {
        this->PrepareNpCorrelationGaussian(tmpWs.get());
    
        this->PrepareNpConstraintGaussian(tmpWs.get());
    }

    // get the PDFs from the WS and combine them
    RooArgSet set;
    auto statGauss = tmpWs->pdf("statGauss");
    if (!statGauss) {
        std::cerr << "Fitter::RunFit: ERROR: Cannot read \"statGauss\" from the WS!\n";
        std::exit(EXIT_FAILURE);
    }
    set.add(*statGauss);
    if (!m_statOnly) {
        auto npGauss = tmpWs->pdf("npCor");
        if (!npGauss) {
            std::cerr << "Fitter::RunFit: ERROR: Cannot read \"npCor\" from the WS!\n";
            std::exit(EXIT_FAILURE);
        }
        set.add(*npGauss);
        if (m_useCorrelatedNPs) {
            if (m_cxInv_correlated.GetNrows() != 0) {
                auto pullGauss = tmpWs->pdf("pull_Gaussian");
                if (!pullGauss) {
                    std::cerr << "Fitter::RunFit: ERROR: Cannot read \"pull_Gaussian\" from the WS!\n";
                    std::exit(EXIT_FAILURE);
                }
                set.add(*pullGauss);
            } else {
                std::cout << "Fitter::RunFit: Ignoring the last chi^2 term. This happens if all NPs are not pulled nor constrained\n";
            }
        } else {
            for (std::size_t i = 0; i < m_nPOIs.size(); ++i) {
                if (m_cxInv.at(i).GetNrows() == 0) {
                    std::cout << "Fitter::RunFit: Ignoring the last chi^2 term for measurement: " << i << ". This happens if all NPs are not pulled nor constrained\n";
                    continue;
                }
                auto pullGauss = tmpWs->pdf(("pull_Gaussian_meas_"+std::to_string(i)).c_str());
                if (!pullGauss) {
                    std::cerr << "Fitter::RunFit: ERROR: Cannot read \"pull_Gaussian_meas_" << i << "\" from the WS!\n";
                    std::exit(EXIT_FAILURE);
                }
                set.add(*pullGauss);
            }
        }
    }

    // construct the product
    RooProdPdf model("model","model", set);
    tmpWs->import(model, RooFit::RecycleConflictNodes());

    // retrieve the final pdf and use it
    auto finalModel = tmpWs->pdf("model");
    if (!finalModel) {
        std::cerr << "Fitter::RunFit: ERROR Cannot read \"model\" from the workspace!\n";
        std::exit(EXIT_FAILURE);
    }

    // create the final WS
    auto ws = std::make_unique<RooWorkspace>("ws");    

    RooDataSet data("data","data", obs);
    // fill data and pull points
    for (std::size_t ipoi = 0; ipoi < m_poiNames.size(); ++ipoi) {
        const std::string name = m_poiNames.at(ipoi)+"_data_"+std::to_string(ipoi);
        auto tmp = obs.find(name.c_str());
        if (tmp) {
            *static_cast<RooRealVar*>(tmp) = m_scaleCovMatrix * m_values.at(ipoi);
        }
    }

    data.add(obs);
    if (m_debug) {
        data.Print("v");
    }
    tmpWs->import(data);
    if (m_debug) {
        tmpWs->Print(); 
    }

    ws->import(*finalModel);
    ws->import(data);

    // add ModelConfig
    auto pois = this->GetPOIs(finalModel, &data);
    RooStats::ModelConfig mc("ModelConfig",ws.get());
    mc.SetPdf(*finalModel);
    mc.SetParametersOfInterest(pois);
    mc.SetNuisanceParameters(this->GetNPs(finalModel, &data));
    mc.SetObservables(obs);
    if (m_debug) {
        mc.Print();
    }
    ws->import(mc);

    ws->writeToFile((m_outPath+"/workspace.root").c_str());
    auto end = std::chrono::steady_clock::now();
    auto duration = std::chrono::duration_cast<std::chrono::seconds>(end - start);
    std::cout << "Fitter::PrepareChi2: Finished preparing the workspace with the chi^2 in " << duration.count() << " seconds\n";
}

void Fitter::PrepareStatGaussian(RooArgList* obs, RooWorkspace* ws) const {
    RooArgList poiData;
    RooArgList pois; // parameters to be fitted
    for (std::size_t ipoi = 0; ipoi < m_poiNames.size(); ++ipoi) {
        const std::string name = m_poiNames.at(ipoi)+"_data_"+std::to_string(ipoi);
        auto itrRange = m_parameterRanges.find(m_poiNames.at(ipoi));
        if (itrRange == m_parameterRanges.end()) {
            std::cerr << "Fitter::PrepareStatGaussian: ERROR POI \"" << m_poiNames.at(ipoi) << "\" does not have a range defined!\n";
            std::exit(EXIT_FAILURE);
        }
        RooRealVar value(name.c_str(), name.c_str(), 0.01, m_scaleCovMatrix * itrRange->second.first, m_scaleCovMatrix * itrRange->second.second);

        // check for reparametrisation
        if (m_reparametrisation.IsReparametrised(m_poiNames.at(ipoi))) {
            RooArgList list = m_reparametrisation.GetDependencies(m_poiNames.at(ipoi));
            pois.addClone(std::move(list));
        } else {
            const double sigma = std::sqrt(m_statCovariance(ipoi, ipoi));
            RooRealVar poi(m_poiNames.at(ipoi).c_str(), m_poiNames.at(ipoi).c_str(), m_values.at(ipoi), m_values.at(ipoi) - 10.*sigma, m_values.at(ipoi) + 10.*sigma);
            pois.addClone(std::move(poi));
        }

        poiData.addClone(value);
        obs->addClone(std::move(value));
    }

    // NPs
    RooArgList nps;
    for (const auto& inp : m_systematicMap) {
        RooRealVar theta(inp.first.c_str(), inp.first.c_str(), -5., 5.);
        nps.addClone(std::move(theta));
    }

    RooArgList muDeltas;

    std::vector<RooArgList> shifts(m_poiNames.size());

    // genererate the Np shifts i.e. x - sum_i delta_i * (theta_i - theta_i^bar)
    for (std::size_t ipoi = 0; ipoi < m_poiNames.size(); ++ipoi) {
        // ceck for reparametrisation
        const bool isReparametrised = m_reparametrisation.IsReparametrised(m_poiNames.at(ipoi));
        if (isReparametrised) {
            RooArgList list = m_reparametrisation.GetDependencies(m_poiNames.at(ipoi));
            shifts.at(ipoi).addClone(std::move(list));
        } else {
            auto poi = static_cast<RooRealVar*>(pois.find(m_poiNames.at(ipoi).c_str()));
            if (!poi) {
                std::cerr << "Fitter::PrepareStatGaussian: ERROR: Cannot find POI: " << m_poiNames.at(ipoi);
                std::exit(EXIT_FAILURE);
            }
            shifts.at(ipoi).addClone(*poi);
        }
        shifts.at(ipoi).addClone(nps);
        RooArgList pulls;
        for (const auto& inp : m_systematicMap) {
            const std::string pullName = "pull_POI_"+std::to_string(ipoi) + "_" + inp.first;
            auto itrNP = std::find(m_allNPs.begin(), m_allNPs.end(), inp.first);
            if (itrNP == m_allNPs.end()) {
                std::cerr << "Fitter::PrepareStatGaussian: ERROR: Cannot find " << inp.first << " in the list of NPs\n";
                std::exit(EXIT_FAILURE);
            }
            const std::size_t position = std::distance(m_allNPs.begin(), itrNP);
            RooRealVar pull(pullName.c_str(), pullName.c_str(), m_poiPulls.at(ipoi).at(position));
            pulls.addClone(pull);
        }
        shifts.at(ipoi).addClone(std::move(pulls));
        // need to create the formula for the shift
        std::string formula("");
        if (isReparametrised) {
            formula = "(" + m_reparametrisation.GetFormula(m_poiNames.at(ipoi)) + ")";
        } else {
            formula = m_poiNames.at(ipoi);
        }
        formula = std::to_string(m_scaleCovMatrix) + " * " + formula;
        if (!m_statOnly) {
            const bool usePearson = m_pearsonIndices.at(ipoi);
            if (usePearson) {
                if (isReparametrised) {
                    formula += " + (" + m_reparametrisation.GetFormula(m_poiNames.at(ipoi)) + ")/" + m_poiNames.at(ipoi)+"_data_"+std::to_string(ipoi) + " *"; 
                } else {
                    formula += " + " + m_poiNames.at(ipoi) + "/" + m_poiNames.at(ipoi)+"_data_"+std::to_string(ipoi) + " *"; 
                }
                formula += "(";
            }
            for (const auto& inp : m_systematicMap) {
                const std::string delta = std::to_string(m_scaleCovMatrix * inp.second.at(ipoi));
                formula += " - " + delta + " * (" + inp.first + " - " + "pull_POI_"+std::to_string(ipoi) + "_" + inp.first + ")";
            }
            if (usePearson) {
                formula += ")";
                RooRealVar* value = static_cast<RooRealVar*>(obs->at(ipoi));
                shifts.at(ipoi).addClone(*value);
            }
        }
        const std::string nameMuDelta = "mu_delta_POI_"+std::to_string(ipoi);
        RooFormulaVar mu_delta(nameMuDelta.c_str(), nameMuDelta.c_str(), formula.c_str(), shifts.at(ipoi));
        muDeltas.addClone(std::move(mu_delta));
    }
    RooMultiVarGaussian statGauss("statGauss","statGauss", poiData, muDeltas, m_statCovariance);
    ws->import(statGauss);
}

void Fitter::PrepareNpCorrelationGaussian(RooWorkspace* ws) const {
    RooArgList nps;

    for (const auto& inp : m_allNPs) {
        RooRealVar theta(inp.c_str(), inp.c_str(), -5., 5.);
        nps.addClone(theta);
    }

    RooMultiVarGaussian npCor("npCor","npCor", nps, m_totalNPcorrelation);
    ws->import(npCor);
}

void Fitter::PrepareNpConstraintGaussian(RooWorkspace* ws) const {
    if (m_useCorrelatedNPs) {
        if (m_cxInv_correlated.GetNrows() == 0) return;
        RooArgList pulls;
        RooArgList nps;
        
        for (std::size_t inp = 0; inp < m_totalPullCorrelated.size(); ++inp) {
            RooRealVar theta(m_npsCorrelated.at(inp).c_str(), m_npsCorrelated.at(inp).c_str(), -5., 5.);
            const std::string pullName = "pull_NP_"+ m_npsCorrelated.at(inp) + "_" + std::to_string(inp);
            RooRealVar pull(pullName.c_str(), pullName.c_str(), m_totalPullCorrelated.at(inp));
            pulls.addClone(std::move(pull));
            nps.addClone(std::move(theta));
        }

        static const std::string nameGaussian = "pull_Gaussian";
        RooMultiVarGaussian constraint(nameGaussian.c_str(),nameGaussian.c_str(),nps, pulls, m_cxInv_correlated);
        ws->import(constraint);

    } else {
        for (std::size_t imeas = 0; imeas < m_nPOIs.size(); ++imeas) {
            if (m_cxInv.at(imeas).GetNrows() == 0) continue;
            RooArgList pulls;
            RooArgList nps;
            for (std::size_t isyst = 0; isyst < m_allNPsReduced.at(imeas).size(); ++isyst) {
                RooRealVar theta(m_allNPsReduced.at(imeas).at(isyst).c_str(), m_allNPsReduced.at(imeas).at(isyst).c_str(), -5., 5.);
                const std::string pullName = "pull_meas_"+std::to_string(imeas)+"_"+m_allNPsReduced.at(imeas).at(isyst);
                RooRealVar pull(pullName.c_str(), pullName.c_str(), m_totalPull.at(imeas).at(isyst));
                pulls.addClone(std::move(pull));
                nps.addClone(std::move(theta));
            }
            const std::string nameGaussian = "pull_Gaussian_meas_"+std::to_string(imeas);
            RooMultiVarGaussian constraint(nameGaussian.c_str(),nameGaussian.c_str(),nps, pulls, m_cxInv.at(imeas));
            ws->import(constraint);
        }
    }
}

void Fitter::PrintParameters(const RooFitResult* fr) const {
    std::fstream out;
    std::string suffix("");
    if (m_statOnly) suffix = "_statOnly";
    else if (m_fixNPsToFitted) suffix = "_fixedNPs";
    
    out.open(m_outPath+"/Parameters"+suffix+".txt", std::fstream::out);
    if (!out.is_open() || !out.good()) {
        std::cerr << "Fitter::PrintParameters: ERROR: Cannot open output at: " << m_outPath+"/Parameters"+suffix+".txt" << "\n";
        std::exit(EXIT_FAILURE);
    }
    for (const auto ipar : fr->floatParsFinal()) {
        auto var = static_cast<RooRealVar*>(ipar);
        const std::string name = ipar->GetName();
        out << name << " " << var->getVal() << " " << var->getErrorLo() << " +" << var->getErrorHi() << "\n";
    }
    out.close();

    if (!m_draw) return;

    std::vector<std::tuple<std::string, std::string, double, double, double> > combinedNPs;
    std::vector<std::tuple<std::string, std::string, double, double, double> > poisFitted;
    for (const auto ipar : fr->floatParsFinal()) {
        const std::string name = ipar->GetName();
        const auto var = static_cast<RooRealVar*>(ipar);
        std::string category("");

        {
            // find the category for this NP
            int imeas(0);
            for (const auto& inps : m_npNames) {
                auto itr = std::find(inps.begin(), inps.end(), name);
                if (itr != inps.end()) {
                    const std::size_t index = std::distance(inps.begin(), itr);
                    category = m_npCategories.at(imeas).at(index);
                    break;
                }
                ++imeas;
            }
        }

        auto tuple = std::make_tuple(name, category, var->getVal(), var->getErrorLo(), var->getErrorHi());

        auto itrPOI = std::find(m_poiNamesUnique.begin(), m_poiNamesUnique.end(), name);
        auto itrPOIReparamatrizsed = std::find(m_poiNamesReparametrised.begin(), m_poiNamesReparametrised.end(), name);

        if (itrPOI != m_poiNamesUnique.end() || itrPOIReparamatrizsed != m_poiNamesReparametrised.end()) {
            poisFitted.emplace_back(std::move(tuple));
        } else {
            combinedNPs.emplace_back(std::move(tuple));
        }
    }

    std::vector<std::string> measurementsToConsider;
    std::vector<std::size_t> measurementIndices;
    std::size_t index(0);
    for (const auto& imeasurement : m_measurements) {
        if (std::find(m_compareMeasurements.begin(), m_compareMeasurements.end(), imeasurement) == m_compareMeasurements.end()) {
            ++index;
            continue;
        }
        measurementsToConsider.emplace_back(imeasurement);        
        measurementIndices.emplace_back(index);
        ++index;
    }

    std::vector<std::vector<std::string> > npsPerMeasurement;
    std::vector<std::vector<double> > pullsPerMeasurement;
    std::vector<std::vector<double> > constraintsPerMeasurement;

    for (const std::size_t imeas : measurementIndices) {
        npsPerMeasurement.emplace_back(m_npNames.at(imeas));
        pullsPerMeasurement.emplace_back(m_pulls.at(imeas));
        constraintsPerMeasurement.emplace_back(m_npInputConstraints.at(imeas));
    }

    Plotter plotter(m_outPath, m_printFormat);
    plotter.PlotPOIs(poisFitted);
    for (const auto& icategory : m_npUniqueCategories) {
        plotter.PlotPulls(combinedNPs, measurementsToConsider, npsPerMeasurement, pullsPerMeasurement, constraintsPerMeasurement, icategory);
    }
}

void Fitter::PrintCorrelationCovariance(const RooFitResult* fr, const bool customParams) const {
    const auto cor = fr->correlationHist("");
    const auto cov = fr->covarianceMatrix();

    std::fstream outCor;
    std::fstream outCov;

    std::string suffix = customParams ? "_customParams" : "";
    if (m_statOnly) suffix = "_statOnly";
    else if (m_fixNPsToFitted) suffix = "_fixedNPs";

    outCor.open(m_outPath+"/Correlation"+suffix+".txt", std::fstream::out);
    outCov.open(m_outPath+"/Covariance"+suffix+".txt", std::fstream::out);
    if (!outCor.is_open() || !outCor.good()) {
        std::cerr << "Fitter::PrintCorrelationCovariance: ERROR: Cannot open output at: " << m_outPath+"/Correlation"+suffix+".txt" << "\n";
        std::exit(EXIT_FAILURE);
    }
    if (!outCov.is_open() || !outCov.good()) {
        std::cerr << "Fitter::PrintCorrelationCovariance: ERROR: Cannot open output at: " << m_outPath+"/Covariance"+suffix+".txt" << "\n";
        std::exit(EXIT_FAILURE);
    }

    const auto& myParams = customParams ? m_printParameters : m_poiNamesReparametrised;
    
    std::vector<std::string> params;
    std::vector<int> indices;
    int index(0);
    for (const auto iparam : fr->floatParsFinal()) {
        if (customParams) {
            auto itr = std::find(myParams.begin(), myParams.end(), iparam->GetName());
            if (itr != myParams.end()) {
                params.emplace_back(iparam->GetName());
                indices.emplace_back(index);
            }
        } else {
            params.emplace_back(iparam->GetName());
            indices.emplace_back(index);
        }
        ++index;
    }

    const int size = params.size();
    outCor << "\t";
    outCov << "\t";
    for (int i = 1; i <= size; ++i) {
        outCor << params.at(i-1) << " ";
        outCov << params.at(i-1) << " ";
    }
    outCor << "\n";
    outCov << "\n";
    const int nBins = cor->GetNbinsX();
    for (int i = 1; i <= size; ++i) {
        outCor << params.at(i-1) << " ";
        outCov << params.at(i-1) << " ";
        for (int j = 1; j <= size; ++j) {
            const int index_i = indices.at(i-1);
            const int index_j = indices.at(j-1);
            outCor << cor->GetBinContent(nBins-index_i,index_j+1) << " ";
            outCov << cov(index_i,index_j) << " ";
        }
        outCor << "\n";
        outCov << "\n";
    }
    outCor.close();
    outCov.close();

    if (!m_draw) return;
    if (customParams) return;

    Plotter plotter(m_outPath, m_printFormat);
    plotter.PlotParametersCorrelation(cor, params, m_correlationThreshold);
}

void Fitter::ProcessCovarianceTerms() {

    // need to build covariance matrix and pull vector for each measurement
    for (std::size_t imeas = 0; imeas < m_npNames.size(); ++imeas) {
        // get the pulls
        std::vector<double> pulls(m_allNPs.size(), 0.);
        for (std::size_t i = 0; i < m_npNames.at(imeas).size(); ++i) {
            auto itr = std::find(m_allNPs.begin(), m_allNPs.end(), m_npNames.at(imeas).at(i));
            if (itr == m_allNPs.end()) {
                std::cerr << "Fitter::ProcessCovarianceTerms: ERROR: Cannot find NP: " << m_npNames.at(imeas).at(i) << " in the list of NPs\n";
                std::exit(EXIT_FAILURE);
            }
            const std::size_t index = std::distance(m_allNPs.begin(), itr);
            pulls.at(index) = m_pulls.at(imeas).at(i); 
        }

        TMatrixDSym cov(m_allNPs.size());
        // initialise to unity
        for (int i = 0; i < cov.GetNrows(); ++i) {
            for (int j = 0; j < cov.GetNrows(); ++j) {
                cov(i,j) = (i == j) ? (m_removeCovarianceTerms ? 1.0 : 1-(m_inversionPrecision*m_inversionPrecision)) : 0.;
            }
        }

        /// Skip NPs covariance matrix calculation if the correlation matrix is not provided
        for (std::size_t i = 0; i < m_npNames.at(imeas).size(); ++i) {
            for (std::size_t j = i; j < m_npNames.at(imeas).size(); ++j) {
                double sigma_i = m_npInputConstraints.at(imeas).at(i);
                double sigma_j = m_npInputConstraints.at(imeas).at(j);
                double correlation = (i == j) ? 1.0 : 0.;
                if (!m_npCorrelationNames.at(imeas).empty()) {
                    if (std::find(m_skippedNPs.at(imeas).begin(), m_skippedNPs.at(imeas).end(), m_npNames.at(imeas).at(i)) != m_skippedNPs.at(imeas).end()) continue;
                    if (std::find(m_skippedNPs.at(imeas).begin(), m_skippedNPs.at(imeas).end(), m_npNames.at(imeas).at(j)) != m_skippedNPs.at(imeas).end()) continue;
                    auto itr_i = std::find(m_npCorrelationNames.at(imeas).begin(), m_npCorrelationNames.at(imeas).end(), m_npNames.at(imeas).at(i));
                    auto itr_j = std::find(m_npCorrelationNames.at(imeas).begin(), m_npCorrelationNames.at(imeas).end(), m_npNames.at(imeas).at(j));
                    if (itr_i == m_npCorrelationNames.at(imeas).end() || itr_j == m_npCorrelationNames.at(imeas).end()) {
                        std::cerr << "Fitter::ProcessCovarianceTerms: ERROR: Problem with the indices of the NPs\n";
                        std::exit(EXIT_FAILURE);
                    }
                    const std::size_t index_i = std::distance(m_npCorrelationNames.at(imeas).begin(), itr_i);
                    const std::size_t index_j = std::distance(m_npCorrelationNames.at(imeas).begin(), itr_j);
                    if (!m_removeCovarianceTerms) {
                        const double precSquare = m_inversionPrecision*m_inversionPrecision;
                        if (sigma_i >= 1.-precSquare) sigma_i = 1-precSquare;
                        if (sigma_j >= 1.-precSquare) sigma_j = 1-precSquare;
                    }
                    correlation = m_npCorrelations.at(imeas)(index_i,index_j);
                }
                auto itrPar_i = std::find(m_allNPs.begin(), m_allNPs.end(), m_npNames.at(imeas).at(i));
                auto itrPar_j = std::find(m_allNPs.begin(), m_allNPs.end(), m_npNames.at(imeas).at(j));
                if (itrPar_i == m_allNPs.end() || itrPar_j == m_allNPs.end()) {
                    std::cerr << "Fitter::ProcessCovarianceTerms: ERROR: Cannot match the indices\n";
                    std::exit(EXIT_FAILURE);
                }
                const std::size_t indexPar_i = std::distance(m_allNPs.begin(), itrPar_i);
                const std::size_t indexPar_j = std::distance(m_allNPs.begin(), itrPar_j);
                cov(indexPar_i,indexPar_j) = sigma_i * correlation * sigma_j;
                if (indexPar_i != indexPar_j) {
                    cov(indexPar_j,indexPar_i) = sigma_i * correlation * sigma_j;
                }
            }
        }

        if (m_removeCovarianceTerms) {
            auto pullsCov = this->RemoveElements(pulls, cov);

            pulls = pullsCov.first;
            cov.ResizeTo(pullsCov.second);
            cov = pullsCov.second;
        }

        cov.Invert();

        m_cx.emplace_back(pulls.size());
        m_cx.back() = cov;

        // generate unity matrix
        TMatrixDSym unity(m_cx.back());
        for (int i = 0; i < unity.GetNrows(); ++i) {
            for (int j = 0; j < unity.GetNrows(); ++j) {
                unity(i,j) = (i == j) ? 1. : 0.;
            }
        }

        // inverse of unity is unity
        // C^-1 - I^-1
        m_cx.back() -= unity;

        m_cxInvC.emplace_back(pulls.size());
        m_cxInvC.back() = m_cx.back();
        // (C^-1 - I^-1)^-1
        m_cxInvC.back().Invert();

        m_cxInv.emplace_back(m_cxInvC.back());

        // (C^-1 - I^-1)^-1 * C^-1
        m_cxInvC.back() = Fitter::MultiplyMatrices(m_cxInvC.back(), cov);

        std::vector<double> pullVec;
        for (std::size_t i = 0; i < pulls.size(); ++i) {
            double sum(0.);
            // (C^-1 - I^-1)^-1 * C^-1 * pull
            for (std::size_t j = 0; j < pulls.size(); ++j) {
                sum += m_cxInvC.back()(i,j) * pulls.at(j);
            }
            pullVec.emplace_back(sum);
        }
        m_totalPull.emplace_back(pullVec);
    }
}

void Fitter::ProcessCovarianceTermsCorrelated() {
    int size(0);

    for (const auto& inpMeas : m_npNames) {
        size += inpMeas.size();
    }

    if (m_totalNPcovarianceCorrelated.GetNrows() != size) {
        std::cerr << "Fitter::ProcessCovarianceTermsCorrelated: ERROR: Sizes of the total NP covariance matrix and the NPs do not match\n";
        std::exit(EXIT_FAILURE);
    }

    m_npsCorrelated.clear();
    for (const auto& inpMeas : m_npNames) {
        for (const auto& inp : inpMeas) {
            m_npsCorrelated.emplace_back(inp);
        }
    }

    std::vector<double> pulls;
    for (const auto& ipullMeas : m_pulls) {
        for (const auto ipull : ipullMeas) {
            pulls.emplace_back(ipull);
        }
    }


    TMatrixDSym covInverted(m_totalNPcovarianceCorrelated);

    if (m_removeCovarianceTerms) {
        pulls = this->RemoveElementsCorrelated(pulls);
    }
    auto correlation = Fitter::CovarianceToCorrelation(covInverted);
    covInverted.Invert();

    m_cx_correlated.ResizeTo(covInverted);
    m_cxInvC_correlated.ResizeTo(covInverted);

    // C^-1
    m_cx_correlated = covInverted;
    // C^-1 - I^-1
    m_cx_correlated -= correlation;

    // C^-1 - I^-1
    m_cxInvC_correlated = m_cx_correlated;

    // (C^-1 - I^1)^-1
    m_cxInvC_correlated.Invert();

    m_cxInv_correlated.ResizeTo(m_cxInvC_correlated);
    m_cxInv_correlated = m_cxInvC_correlated;
    
    // (C^-1 - I^1)^-1 x C^-1
    m_cxInvC_correlated = Fitter::MultiplyMatrices(m_cxInvC_correlated, covInverted);

    m_totalPullCorrelated.resize(pulls.size());

    // (C^-1 - I^1)^-1 x C^-1 x pull
    for (std::size_t i = 0; i < pulls.size(); ++i) {
        double value(0.);
        for (int icolumn = 0; icolumn < m_cxInvC_correlated.GetNrows(); ++icolumn) {
            value += m_cxInvC_correlated(i, icolumn) * pulls.at(icolumn);
        }
        m_totalPullCorrelated.at(i) = value;
    }
}

void Fitter::ProcessInputs() {
    /// map the pois
    for (const auto& ipoi : m_poiNames) {
        auto itr = std::find(m_poiNamesUnique.begin(), m_poiNamesUnique.end(), ipoi);
        if (itr == m_poiNamesUnique.end()) {
            std::cerr << "Fitter::ProcessInputs: ERROR: POI: " << ipoi << " not found in the unique list - this should not happen\n";
            std::exit(EXIT_FAILURE);
        }
    }
    
    /// set the indices for the NPs for the measurements
    std::size_t index(m_poiNamesUnique.size());
    for (const auto& isyst : m_systematicMap) {
        m_allNPs.emplace_back(isyst.first);
        ++index;
    }

    std::cout << "Fitter::ProcessInputs: Preparing the X matrix\n";
    // prepare the total NP correlation matrix - the one set by hand
    // first, check if the NPs provided are present only in some measurements
    std::map<std::string, std::size_t> matchMap;
    for (std::size_t imeas = 0; imeas < m_npNames.size(); ++imeas) {
        for (const auto& inp : m_npNames.at(imeas)) {
            std::size_t matches(1);
            for (std::size_t jmeas = imeas + 1; jmeas < m_npNames.size(); ++jmeas) {
                auto itr = std::find(m_npNames.at(jmeas).begin(), m_npNames.at(jmeas).end(), inp);
                if (itr != m_npNames.at(jmeas).end()) {
                    ++matches;
                }
            }
            auto itr = matchMap.find(inp);
            if (itr == matchMap.end()) {
                matchMap.insert(std::make_pair(inp, matches));
            }
        }
    }

    /// prepare the inputs for cross-correlations
    for (const auto& inp : m_crossNPNames) {
        auto itr = std::find(m_allNPs.begin(), m_allNPs.end(), inp);
        if (itr == m_allNPs.end()) {
            std::cerr << "Fitter::ProcessInputs: ERROR: Cross NP: " << inp << " not found in the list of systematics\n";
            std::exit(EXIT_FAILURE);
        }
        /// check if the NP is not provided for all measurements
        auto itrMap = matchMap.find(inp);
        if (itrMap == matchMap.end()) {
            std::cerr << "Fitter::ProcessInputs: ERROR: Cross NP: " << inp << " not found in the list of systematics\n";
            std::exit(EXIT_FAILURE);
        }
        if (itrMap->second == m_npNames.size()) {
            std::cerr << "Fitter::ProcessInputs: ERROR: Cross NP: " << inp << " is used by all measurements, you cannot provide cross-correlations for them\n";
            std::exit(EXIT_FAILURE);
        }
    }

    // prepare the NP correlation matrix
    // prepare a unitary matrix and then set then corresponding correlation values
    TMatrixDSym totalCorrelation(m_allNPs.size());
    for (std::size_t i = 0; i < m_allNPs.size(); ++i) {
        for (std::size_t j = 0; j < m_allNPs.size(); ++j) {
            totalCorrelation(i,j) = (i == j) ? 1. : 0.;
        }
    }

    for (int i = 0; i < m_crossNPCorrelation.GetNrows(); ++i) {
        for (int j = i+i; j < m_crossNPCorrelation.GetNrows(); ++j) {
            auto itr_i = std::find(m_allNPs.begin(), m_allNPs.end(), m_crossNPNames.at(i));
            auto itr_j = std::find(m_allNPs.begin(), m_allNPs.end(), m_crossNPNames.at(j));
            if (itr_i == m_allNPs.end() || itr_j == m_allNPs.end()) {
                std::cerr << "Fitter::ProcessInputs: ERROR: Something went wrong with the indices for cross-correlations\n";
                std::exit(EXIT_FAILURE);
            }
            const std::size_t index_i = std::distance(m_allNPs.begin(), itr_i);
            const std::size_t index_j = std::distance(m_allNPs.begin(), itr_j);
            totalCorrelation(index_i, index_j) = m_crossNPCorrelation(i,j);   
            totalCorrelation(index_j, index_i) = m_crossNPCorrelation(j,i);   
        }
    }

    m_totalNPcorrelation.ResizeTo(totalCorrelation);
    m_totalNPcorrelation = totalCorrelation;

    if (m_npInputConstraints.size() != m_npNames.size()) {
        std::cerr << "Fitter::ProcessInputs: ERROR: POI: Size of the input NP constraints and npNames do not match\n";
        std::exit(EXIT_FAILURE);
    }

    // pearson indices
    m_pearsonIndices.clear();
    for (const auto& ipoi : m_poiNames) {
        auto itr = std::find(m_usePearsonChi2.begin(), m_usePearsonChi2.end(), ipoi);
        if (itr != m_usePearsonChi2.end()) {
            m_pearsonIndices.emplace_back(true);
        } else {
            m_pearsonIndices.emplace_back(false);
        }
    }

    if (m_statOnly) {
        m_poiPulls.resize(m_poiNames.size(), std::vector<double>(m_allNPs.size(), 0.));
        return;
    }

    /// add the covariance - correlation terms
    std::cout << "Fitter::ProcessInputs: Preparing the covariance terms\n";
    if (m_useCorrelatedNPs) {
        this->ProcessCovarianceTermsCorrelated();
    } else {
        this->ProcessCovarianceTerms();
    }

    std::cout << "Fitter::ProcessInputs: Preparing the NP pulls\n";
    /// process the poi NP pulls
    this->ProcessPOIPulls();
}

void Fitter::ProcessPOIPulls() {
    m_poiPulls.resize(m_poiNames.size());
    for (std::size_t ipoi = 0; ipoi < m_poiPulls.size(); ++ipoi) {
        std::size_t measIndex(0);
        std::size_t total(0);
        // need to match the POI to a measurement
        for (std::size_t imeas = 0; imeas < m_nPOIs.size(); ++imeas) {
            total += m_nPOIs.at(imeas);
            if (ipoi < total) {
                measIndex = imeas;
                break;
            }
            if (imeas == m_nPOIs.size() - 1) {
                std::cerr << "ProcessPOIPulls: ERROR: Cannot match the POI to a measurement\n";
                std::exit(EXIT_FAILURE);
            }
        }
        std::vector<double> values(m_allNPs.size());
        for (std::size_t inp = 0; inp < m_npNames.at(measIndex).size(); ++inp) {
            const std::string np = m_npNames.at(measIndex).at(inp);
            auto itrNP = std::find(m_allNPs.begin(), m_allNPs.end(), np);
            if (itrNP == m_allNPs.end()) {
                std::cerr << "ProcessPOIPulls: ERROR: Mismatch between the NP names\n";
                std::exit(EXIT_FAILURE);
            }

            const std::size_t npIndex = std::distance(m_allNPs.begin(), itrNP);
            values.at(npIndex) = m_pulls.at(measIndex).at(inp);
        }
        m_poiPulls.at(ipoi) = values;
    }
}

void Fitter::RunFit() {

    if (!m_debug) {
        RooMsgService::instance().setGlobalKillBelow(RooFit::ERROR);
    }
    
    if (m_createWS && (m_statCovariance.GetNrows() != static_cast<int>(m_values.size()))) {
        std::cerr << "Fitter::RunFit: ERROR: Size of the provided POI covariance matrix and the number of POIs do not match\n.";
        std::exit(EXIT_FAILURE);
    }

    if (m_createWS) { 
        this->AdjustImpactSizes();

        std::cout << "Fitter::RunFit: Preparing the inputs for the workspace...\n";
        this->ProcessInputs();
    }

    // scale the covariance matrix to be closer to 1
    this->AdjustStatCovarianceMatrix();

    if (m_debug && m_createWS) {
        this->PrintDebugInfo();
    }

    if (m_draw) {
        Plotter plotter(m_outPath, m_printFormat);
        plotter.PlotCorrelationCovariance(m_statCovariance, m_poiNames, true);
        auto corr = Fitter::CovarianceToCorrelation(m_statCovariance);
        plotter.PlotCorrelationCovariance(corr, m_poiNames, false);
    }

    if (m_createWS && (m_poiNames.size() != m_values.size())) {
        std::cerr << "Fitter::RunFit: ERROR: Sizes of POI names and POI values do not match\n";
        std::exit(EXIT_FAILURE);
    }

    if (m_createWS && (m_poiNamesUnique.size() != m_valuesUnique.size())) {
        std::cerr << "Fitter::RunFit: ERROR: Sizes of unique POI names and POI values do not match\n";
        std::exit(EXIT_FAILURE);
    }

    if (m_createWS && !m_fixNPsToFitted) {
        this->PrepareChi2();
    } else {
        std::cout << "\nFitter::RunFit: Reusing the workspace\n";
    }

    std::unique_ptr<TFile> fileWs(TFile::Open((m_outPath+"/workspace.root").c_str()));
    if (!fileWs) {
        std::cerr << "Fitter::RunFit: ERROR: Cannot open the file with the workspace\n";
        std::exit(EXIT_FAILURE); 
    }

    auto ws = dynamic_cast<RooWorkspace*>(fileWs->Get("ws"));
    if (!ws) {
        std::cerr << "Fitter::RunFit: ERROR: Cannot read the workspace\n";
        std::exit(EXIT_FAILURE); 
    }

    auto data = ws->data("data");
    auto mc   = dynamic_cast<RooStats::ModelConfig*>(ws->genobj("ModelConfig"));
    if (!mc) {
        std::cerr << "Fitter::RunFit: ERROR: Cannot read the model config from the workspace\n";
        std::exit(EXIT_FAILURE);
    }
    
    auto model = ws->pdf(mc->GetPdf()->GetName());

    if (!model || !data) {
        std::cerr << "Fitter::RunFit: ERROR: Cannot read data or the pdf from the workspace\n";
        std::exit(EXIT_FAILURE); 
    }

    auto globalObs = this->GetGlobalObservables(model, data);

    if (m_fixNPsToFitted) {
        this->ReadFittedNPs();
    }

    // fix parameters to the values specified by the user
    FitUtils::FixParameters(model, data, m_fixParams);

    std::unique_ptr<RooAbsReal> nll(model->createNLL(*data,
                                                     RooFit::NumCPU(m_threadN),
                                                     RooFit::GlobalObservables(globalObs),
                                                     RooFit::Optimize(kTRUE)
                                                     ));

    if (!m_skipFit) {
        std::cout << "\nFitter::RunFit: Started minimisation with strategy " << m_fitStrategy << "\n\n";
        auto minimisationStart = std::chrono::steady_clock::now();
        ROOT::Math::MinimizerOptions::SetDefaultMinimizer("Minuit2");
        const TString minimType = ::ROOT::Math::MinimizerOptions::DefaultMinimizerType().c_str();
        const TString algorithm = ::ROOT::Math::MinimizerOptions::DefaultMinimizerAlgo().c_str();
        const double tol =        ::ROOT::Math::MinimizerOptions::DefaultTolerance();
    
        RooMinimizer minimizer(*nll);
        minimizer.optimizeConst(2);
        minimizer.setStrategy(m_fitStrategy);
        minimizer.setMinimizerType(minimType.Data());
        minimizer.setPrintLevel(m_debug ? 1 : 0);
        minimizer.setEps(tol);
        int status = minimizer.minimize(minimType.Data(),algorithm.Data());
        auto minimisationEnd = std::chrono::steady_clock::now();
        auto minimisationDuration = std::chrono::duration_cast<std::chrono::seconds>(minimisationEnd - minimisationStart);
        std::cout << "\nFitter::RunFit: Migrad minimisation finished in " << minimisationDuration.count() << " seconds" << "\n\n"; 
        std::cout << "Fitter::RunFit: Started running hesse()\n\n"; 
        minimizer.hesse();
        auto hesseEnd = std::chrono::steady_clock::now();
        auto hesseDuration = std::chrono::duration_cast<std::chrono::seconds>(hesseEnd - minimisationEnd);
        std::cout << "\nFitter::RunFit: Hesse finished in " << hesseDuration.count() << " seconds" << "\n"; 
        std::unique_ptr<RooFitResult> fitResult(minimizer.save());
        std::cout << "\n========================================================================\n"; 
        std::cout << "Fitter::RunFit: Minimisation status:     " << status << "\n"; 
        std::cout << "Fitter::RunFit: Minimisation nllVal:     " << nll->getVal() << "\n"; 
        std::cout << "Fitter::RunFit: Minimisation EDM:        " << fitResult->edm() << "\n"; 
        std::cout << "========================================================================\n\n"; 
        if (m_useMinos.first) {
            minimizer.setPrintLevel(0);
            RooArgList minosList;
            if (m_useMinos.second.empty()) {
                minimizer.minos();
            } else {
                auto params = fitResult->floatParsFinal();
                for (const auto iparam : params) {
                    const std::string name = iparam->GetName();
                    auto it = std::find(m_useMinos.second.begin(), m_useMinos.second.end(), name);
                    if (it != m_useMinos.second.end()) {
                        minosList.add(*iparam);
                    }
                }
                minimizer.minos(minosList);
            }
            auto minosEnd = std::chrono::steady_clock::now();
            auto minosDuration = std::chrono::duration_cast<std::chrono::seconds>(minosEnd - hesseEnd);
            std::cout << "\nFitter::RunFit: Minos finished in " << minosDuration.count() << " seconds" << "\n";
            fitResult.reset(minimizer.save());
        }
        this->PrintParameters(fitResult.get());
        this->PrintCorrelationCovariance(fitResult.get(), false);
        this->PrintCorrelationCovariance(fitResult.get(), true);
    }
    if (m_runRanking && !m_statOnly && !m_fixNPsToFitted)  {
        std::cout << "Fitter::RunFit: Started running ranking\n";
        const auto pois = this->GetPOIs(model,data);
        Ranking ranking{};
        ranking.SetDebug(m_debug);
        if (m_fixParams.empty()) {
            ranking.SetPoiNamesUnique(m_poiNamesReparametrised);
        } else {
            // need to remove the POis if they are fixed
            std::vector<std::string> tmp;
            for (const auto& iparam : m_poiNamesReparametrised) {
                auto it = std::find_if(m_fixParams.begin(), m_fixParams.end(), [&iparam](const auto& element){return element.first == iparam;});
                if (it != m_fixParams.end()) continue;
                tmp.emplace_back(iparam);
            }
            ranking.SetPoiNamesUnique(std::move(tmp));
        }
        ranking.SetThreadN(m_threadN);
        ranking.SetFitStrategy(m_fitStrategy);
        ranking.ReadFileResults(m_outPath + "/Parameters.txt");
        std::vector<std::string> params;
        if (m_rankingNP.empty()) {
            if (m_fixParams.empty()) {
                params = m_allNPs;
            } else {
                // need to remove the fixed NPs
                std::vector<std::string> tmp;
                for (const auto& iparam : m_allNPs) {
                    auto it = std::find_if(m_fixParams.begin(), m_fixParams.end(), [&iparam](const auto& element){return element.first == iparam;});
                    if (it != m_fixParams.end()) continue;
                    tmp.emplace_back(iparam);
                }
                params = std::move(tmp);
            }
        } else {
            params.emplace_back(m_rankingNP);
        }
        ranking.RunRanking(ws, globalObs, params);
        ranking.WriteRankingToFile(m_outPath, m_rankingNP.empty() ? "" : "_"+m_rankingNP);
        std::cout << "Fitter::RunFit: Finished running ranking\n";
    }

}

void Fitter::ReadFittedNPs() {
    std::fstream file;
    file.open(m_outPath + "/Parameters.txt", std::fstream::in);
    if (!file.is_open() || !file.good()) {
        std::cerr << "Fitter::ReadFittedNPs: ERROR: Cannot open results file at: " << m_outPath + "/Parameters.txt" << "\n";
        std::exit(EXIT_FAILURE);
    }

    std::string name;
    double mean;
    double up;
    double down;

    while (file >> name >> mean >> down >> up) {
        auto itr = std::find(m_allNPs.begin(), m_allNPs.end(), name);
        if (itr == m_allNPs.end()) continue;

        m_fixParams.emplace_back(std::make_pair(name, mean));
    }

    file.close();
}

std::pair<std::vector<double>, TMatrixDSym> Fitter::RemoveElements(const std::vector<double>& pulls, const TMatrixDSym& cov) {
    std::vector<double> pullsNew;

    const std::size_t size = cov.GetNrows();

    std::vector<std::size_t> skip;
    std::vector<std::string> nps;
    std::vector<std::vector<double> > matrix;

    for (std::size_t i = 0; i < size; ++i) {
        const double element = cov(i,i);
        if ((1 - element) < m_inversionPrecision) {
            skip.emplace_back(i);
        } else {
            pullsNew.emplace_back(pulls.at(i));
            nps.emplace_back(m_allNPs.at(i));
        } 
    }
    for (std::size_t i = 0; i < size; ++i) {
        if (std::find(skip.begin(), skip.end(), i) != skip.end()) continue;
        std::vector<double> tmp;
        for (std::size_t j = 0; j < size; ++j) {
            if (std::find(skip.begin(), skip.end(), j) != skip.end()) continue;
            tmp.emplace_back(cov(i,j));
        }
        matrix.emplace_back(tmp);
    }

    TMatrixDSym m(matrix.size());
    for (std::size_t i = 0; i < matrix.size(); ++i) {
       for (std::size_t j = i; j < matrix.size(); ++j) {
            m(i,j) = matrix.at(i).at(j);
            if (i != j) {
                m(j,i) = matrix.at(i).at(j);
            }
       }
    }

    m_allNPsReduced.emplace_back(std::move(nps));

    return std::make_pair(pullsNew,m);
}

std::vector<double> Fitter::RemoveElementsCorrelated(const std::vector<double>& pulls) {
    const std::size_t size = m_totalNPcovarianceCorrelated.GetNrows();

    if (m_npsCorrelated.size() != size) {
        std::cerr << "Fitter::RemoveElementsCorrelated: ERROR vector size mismatch\n";
        std::exit(EXIT_FAILURE);
    }

    std::vector<double> result;
    std::vector<std::string> nps;
    std::vector<std::vector<double> > cov;
    std::vector<std::size_t> skip;

    for (std::size_t i = 0; i < size; ++i) {
        const double element = m_totalNPcovarianceCorrelated(i,i);
        if ((1 - element) < m_inversionPrecision) {
            skip.emplace_back(i);
        } else {
            result.emplace_back(pulls.at(i));
            nps.emplace_back(m_npsCorrelated.at(i));
        }
    }

    for (std::size_t i = 0; i < size; ++i) {
        if (std::find(skip.begin(), skip.end(), i) != skip.end()) continue;
        std::vector<double> tmp;
        for (std::size_t j = 0; j < size; ++j) {
            if (std::find(skip.begin(), skip.end(), j) != skip.end()) continue;
            tmp.emplace_back(m_totalNPcovarianceCorrelated(i,j));
        }
        cov.emplace_back(tmp);
    }

    TMatrixDSym matrix(cov.size());
    for (std::size_t i = 0; i < cov.size(); ++i) {
       for (std::size_t j = i; j < cov.size(); ++j) {
            matrix(i,j) = cov.at(i).at(j);
            if (i != j) {
                matrix(j,i) = cov.at(i).at(j);
            }
       }
    }

    m_totalNPcovarianceCorrelated.ResizeTo(matrix);
    m_totalNPcovarianceCorrelated = matrix;

    m_npsCorrelated = nps;

    return result;
}
