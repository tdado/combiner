#include "include/Ranking.h"

#include "RooAbsPdf.h"
#include "RooArgList.h"
#include "RooFitResult.h"
#include "RooMinimizer.h"
#include "RooRealVar.h"
#include "RooWorkspace.h"

#include <fstream>
#include <iostream>

Ranking::Ranking() :
    m_debug(false),
    m_fitStrategy(1),
    m_threadN(1)
{}

void Ranking::ReadFileResults(const std::string& path) {
    m_result.clear();
    std::fstream file;
    file.open(path, std::fstream::in);
    if (!file.is_open() || !file.good()) {
        std::cerr << "Ranking::FileResults: ERROR: Cannot open results file at: " << path << "\n";
        std::exit(EXIT_FAILURE);
    }

    std::string name;
    double mean;
    double up;
    double down;
    while (file >> name >> mean >> down >> up) {
        Impact impact;
        impact.nominal = mean;
        impact.up      = up;
        impact.down    = down;
        m_result.insert(std::make_pair(name, impact));
    }

    file.close();
}

void Ranking::RunRanking(RooWorkspace* ws, const RooArgList& globalObservables, const std::vector<std::string>& nps) {

    auto model = ws->pdf("model");
    auto data = ws->data("data");

    for (const auto& ival : m_result) {
        auto it = std::find(m_poiNamesUnique.begin(), m_poiNamesUnique.end(), ival.first);
        if (it == m_poiNamesUnique.end()) {
            m_npNames.emplace_back(ival.first);
        }
    }
    
    auto ReleaseAllParams = [model, data, this]() {
        auto params = model->getParameters(*data);
        for (auto iparam : *params) {
            if (std::find(this->m_npNames.begin(), this->m_npNames.end(), iparam->GetName()) == this->m_npNames.end()) continue;
            static_cast<RooRealVar*>(iparam)->setVal(0.);
            static_cast<RooRealVar*>(iparam)->setConstant(kFALSE);
        }
    };

    auto RunSingleRanking = [model, data, ReleaseAllParams, &globalObservables, this](const std::string& name, const bool isPrefit, const bool isUp) {
        ReleaseAllParams();
        auto params = model->getParameters(*data);
        auto par = params->find(name.c_str());
        if (!par) {
            std::cerr << "Ranking::RunRanking: ERROR: Cannot find parameter: " << name << " in the list\n";
            std::exit(EXIT_FAILURE); 
        }
        auto p = static_cast<RooRealVar*>(par);

        auto it = this->m_result.find(name);
        if (it == this->m_result.end()) {
            std::cerr << "Ranking::RunRanking: ERROR: Cannot find parameter: " << name << " in the list\n";
            std::exit(EXIT_FAILURE); 
        }
        const double up   = isPrefit ? (it->second.nominal + 1.) : (it->second.nominal + it->second.up); 
        const double down = isPrefit ? (it->second.nominal - 1.) : (it->second.nominal + it->second.down); 
        p->setVal(isUp ? up : down);
        p->setConstant(kTRUE);
        std::cout << "\nRanking::RunRanking: Running ranking on: " << name << " setting value to: " << (isUp ? up : down) << "\n";

        std::unique_ptr<RooAbsReal> nll(model->createNLL(*data,
                                                         RooFit::GlobalObservables(globalObservables),
                                                         RooFit::NumCPU(this->m_threadN),
                                                         RooFit::Optimize(kTRUE)
                                                         ));

        ROOT::Math::MinimizerOptions::SetDefaultMinimizer("Minuit2");
        const TString minimType = ::ROOT::Math::MinimizerOptions::DefaultMinimizerType().c_str();
        const TString algorithm = ::ROOT::Math::MinimizerOptions::DefaultMinimizerAlgo().c_str();
        const double tol =        ::ROOT::Math::MinimizerOptions::DefaultTolerance();

        RooMinimizer minim(*nll);
        minim.optimizeConst(2);
        minim.setStrategy(m_fitStrategy);
        minim.setMinimizerType(minimType.Data());
        minim.setEps(tol);
        minim.setPrintLevel(this->m_debug ? 1 : -1);

        minim.minimize(minimType, algorithm);

        std::unique_ptr<RooFitResult> r(minim.save());
        std::vector<double> poi;
        auto parameters = r->floatParsFinal();
        for (const auto& ipar : parameters) {
            const auto i = static_cast<RooRealVar*>(ipar);
            auto itr = std::find(this->m_poiNamesUnique.begin(), this->m_poiNamesUnique.end(), i->GetName());
            if (itr != this->m_poiNamesUnique.end()) {
                poi.emplace_back(i->getVal());
            }
        }

        return poi;
    };

    const std::size_t npSize = m_npNames.size();
    const std::size_t poiSize = m_result.size() - npSize;

    m_ranking.resize(poiSize);
    for (auto& irank : m_ranking) {
        irank.resize(npSize);
    }

    std::size_t iparam(0);
    for (const auto& inp : nps) {
        if (std::find(m_poiNamesUnique.begin(), m_poiNamesUnique.end(), inp) != m_poiNamesUnique.end()) continue;
        const std::vector<double> prefitUp    = RunSingleRanking(inp, true, true);
        const std::vector<double> prefitDown  = RunSingleRanking(inp, true, false);
        const std::vector<double> postfitUp   = RunSingleRanking(inp, false, true);
        const std::vector<double> postfitDown = RunSingleRanking(inp, false, false);

        std::vector<std::tuple<double, double, double, double, std::string> > tmp;
        for (std::size_t ipoi = 0; ipoi < m_poiNamesUnique.size(); ++ipoi) {
            const std::string poi = m_poiNamesUnique.at(ipoi);
            auto it = m_result.find(poi);
            if (it == m_result.end()) {
                std::cerr << "Ranking::RunRanking: ERROR: Cannot find " << poi << " in the results\n";
                std::exit(EXIT_FAILURE);
            }
            const double poiVal = it->second.nominal;
            const double rankingPrefitUp    = prefitUp.at(ipoi)    - poiVal;
            const double rankingPrefitDown  = prefitDown.at(ipoi)  - poiVal;
            const double rankingPostfitUp   = postfitUp.at(ipoi)   - poiVal;
            const double rankingPostfitDown = postfitDown.at(ipoi) - poiVal;
            auto tuple = std::make_tuple(rankingPrefitUp, rankingPrefitDown, rankingPostfitUp, rankingPostfitDown, inp);
            m_ranking.at(ipoi).at(iparam) = std::move(tuple);
        }
        ++iparam;
    }

    auto SortTuple = [](const auto& lhs, const auto& rhs) {
        const double maxLHS = std::max(std::abs(std::get<2>(lhs)), std::abs(std::get<3>(lhs)));
        const double maxRHS = std::max(std::abs(std::get<2>(rhs)), std::abs(std::get<3>(rhs)));
        return maxLHS > maxRHS;
    };

    for (std::size_t ipoi = 0; ipoi < m_poiNamesUnique.size(); ++ipoi) {
        std::sort(m_ranking.at(ipoi).begin(), m_ranking.at(ipoi).end(), SortTuple);
    }

    if (m_debug) {
        // print the ranking to the terminal
        std::cout << "\n";
        for (std::size_t ipoi = 0; ipoi < m_poiNamesUnique.size(); ++ipoi) {
            std::cout << "Ranking::RunRanking: Ranking for POI: " << m_poiNamesUnique.at(ipoi) << "\n";
            for (const auto& inp : m_ranking.at(ipoi)) {
                std::cout << "Ranking::RunRanking: -> NP: " << std::get<4>(inp) << ", prefit up: " << std::get<0>(inp) <<
                ", prefit down: " << std::get<1>(inp) << ", postfit up: " << std::get<2>(inp) <<
                ", postfit down: " << std::get<3>(inp) << "\n";
            }
            std::cout << "\n";
        }
    }
}

void Ranking::WriteRankingToFile(const std::string& folder, const std::string& suffix) const {
    for (std::size_t ipoi = 0; ipoi < m_poiNamesUnique.size(); ++ipoi) {
        std::fstream out;
        out.open(folder+"/Ranking_"+m_poiNamesUnique.at(ipoi) + suffix + ".txt", std::fstream::out);
        if (!out.is_open() || !out.good()) {
            std::cerr << "Ranking::WriteRankingToFile: ERROR Cannot open output file at: " << folder+"/Ranking_"+m_poiNamesUnique.at(ipoi) + suffix + ".txt" << "\n";
            std::exit(EXIT_FAILURE);
        }
        for (const auto& inp : m_ranking.at(ipoi)) {
            out << std::get<4>(inp) << " " << std::get<0>(inp) << " " << std::get<1>(inp) << " " << std::get<2>(inp) << " " << std::get<3>(inp) << "\n";
        }
        out.close();
    }
}
