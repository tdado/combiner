#include "include/Fitter.h"
#include "include/Parser.h"

#include <iostream>
#include <string>

int main(int argc, const char* argv[]) {
  if (argc < 2) {
    std::cerr << "Expecting at least 1 argument, but " << argc -1 << " arguments provided\n";
    std::abort();
  }

  const std::string path(argv[1]);
  std::vector<std::string> cli;
  for (int i = 2; i < argc; ++i) {
    const std::string token = argv[i];
    cli.emplace_back(std::move(token));
  }

  Fitter fitter{};

  Parser parser(&fitter);
  parser.ReadConfigFile(path);
  parser.ReadCommandLine(cli);
  parser.PrintSummary();

  fitter.RunFit();

  // successful run
  return 0;
}
