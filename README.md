# Combiner v0.0.4 [![build status](https://gitlab.cern.ch/tdado/combiner/badges/master/pipeline.svg "build status")](https://gitlab.cern.ch/tdado/combiner/commits/master)

The package provides a framework to combine different measurements that can have statistical overlap and/or different constraints and pulls of the nuisance parameters.
The combination will give results for all the individual parameters in each measurement and the NPs that can be common across different measurements.
The code allows to reparametrise the parameters of interest using any formula readable by `TFormula`, which can be used e.g. for EFT interpretations.

## Table of contents
1. [Basic concept](#basic-concept)
2. [Getting the code](#getting-the-code)
3. [Setup and compilation](#setup)
4. [Running the code](#running-the-code)
5. [Configuration options](#configuration)
6. [Developer Notes](#developer-notes)

## Basic concept
The basic idea of the code is to minimise a $`\chi^2`$ likelihood that contains all the information from the individual measurements.
The likelihood has the following form

```math
\chi^2 = \chi^2_{stat} + \chi^2_{NP},
```

where the individual terms are

```math
\chi^2_{stat} = \vec{D}(\vec{x}, \vec{\theta})C^{-1}\vec{D}^{T}(\vec{x}, \vec{\theta}),
```

where

```math
D_{i} = x_{i} - \bar{x}_{i} + \sum_{p \in NP}(\tau_i\Delta_{i,p}(\theta_p - \bar{\theta}_{p,i}))
```

with $`C`$ being the covariance matrix of the parameters of interest (POI) that are being estimated, $`x_i`$ are the input measured values of POI $`i`$, $`\bar{x}_i`$ are the sought after values of POI $`i`$, $`\Delta_{i,p}`$ is the impact of a NP $`p`$ on bin $`i`$, $`\theta_p`$ is the nuisance parameter term for NP $`p`$, $`\bar{\theta}_{p,i}`$ is the pull of the NP $`p`$ in the measurement that has POI $`i`$ and

```math
\tau_i = \bar{x}_i/x_i,
```

is the Pearson correction.

The second term can be split into two parts
```math
\chi^2_{NP} =  \chi^2_{prior} + \chi^2_{constraint},
```

where

```math
\chi^2_{prior} = \vec{\theta}^T X^{-1}\vec{\theta},
```

where $`X`$ is the correlation matrix of the NPs provided a priori. Usually, this is a unity matrix meaning no additional correlations between NPs, however cross-correlations of the NPs can be provided by hand.

The second part consists of contributions from each measurement that is to be combined
```math
\chi^2_{constraint} = \sum_m \left(\vec{\theta} - [C^{-1}_{m} - I^{-1}]^{-1}C^{-1}_{m}\vec{\bar{\theta_m}}\right)^T \left(C^{-1}_{m} - I^{-1}\right)\left(\vec{\theta} - [C^{-1}_{m} - I^{-1}]^{-1}C^{-1}_{m}\vec{\bar{\theta_m}}\right)
```

where $`C_m`$ are the covariance matrices for measurement $`m`$, $`I`$ is a unity matrix and $`\bar{\theta}_m`$ is the fitted valued of NP $`\theta`$ in measurement $`m`$.

If the number of total NPs (represented by size of $`X`$) and the number of NPs in each measurement (represented by $`C`$) is not equal, a "padding" is added to $`C`$ with 1s in the diagonal and 0s in the off-diagonal elements.

Alternatively, you can tell the code to use a modified likelihood where the statistical correlations between the measurements of the NPs (not the correlations of the NPs) are included in the likelihood. This can be configured by passing the bootstrap replicas for the NPs and setting `useCorrelatedNPs` parameter to `true`. The modified likelihood has the following form

```math
\chi^2_{prior} = \vec{\theta}^T X^{-1}\vec{\theta},
```

```math
\chi^2_{constraint} = \left(\vec{\theta} - [C^{-1} - K^{-1}]^{-1}C^{-1}\vec{\bar{\theta}}\right)^T \left(C^{-1} - K^{-1}\right)\left(\vec{\theta} - [C^{-1} - K^{-1}]^{-1}C^{-1}\vec{\bar{\theta}}\right),
```

where matrix $`C`$ now represents a join covariance matrix for all NPs for all measurements. E.g., if we have $`M`$ measurements, each with $`N`$ nuisance parameters, the $`C`$ matrix will have $`N \times M`$ rows (columns). Similarly, vectors $`\theta`$ and $`\bar{\theta}`$ need to be concatenated to represent joint vectors of NPs (pulls) in each measurement. Matrix $`K`$ is the correlation matrix of the NPs.  Due to a possibly very large matrices, this option is turned off by default. An example configuration file is provided [here](config/bootstrap_correlated.yml).

## Getting the code
To get the code use the following command
```bash
git clone ssh://git@gitlab.cern.ch:7999/tdado/combiner.git
```

or the html version
```bash
git clone https://gitlab.cern.ch/tdado/combiner.git
```

Then initialise the submodules with
```
cd combiner
git submodule update --init
```

## Setup
The code prerequisites are: `cmake` and `ROOT` compliled and linked with `TMinuit2`.

### Lxplus setup
If you work on lxplus or a similar machine with access to cvmfs, all you need to do is

```bash
source /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/user/atlasLocalSetup.sh # usual alias is `setupATLAS`
lsetup "views LCG_102b_ATLAS_2 x86_64-centos7-gcc11-opt" cmake
```

### Compiling the code
To compile the code create a `build` directory in the main repository

```bash
mkdir build
```

And then run the `cmake` and `make` commands in the `build` directory

```bash
cd build
cmake ../
make
```

## Running the code
To run the code, call the executable
```bash
# from the main working directory
./build/bin/runCombiner configFile.yml [<command line options>]
```

Where `configFile.yml` is the path to the config file in the yaml format.
An example config file is provided [here](config/test.yml).
An example config file with bootstraps also for NPs is provided [here](config/bootstraps_correlated.yml).
An example config file showing the reparametrisation is provided [here](config/reparametrised.yml).
The command line options are optional and have the following form `option1=value option2=value`.
The supported command line options are summarised in the table below

| *Option*  | *Description* |
| --------  | --------- |
| createWS | Sets the `createWS` option. In case it is specified in the config file, it is overwritten by the command line option. |
| fixNPsToFitted | Sets the `fixNPsToFitted` option. In case it is specified in the config file, it is overwritten by the command line option. |
| ranking | Sets the `ranking` option. In case it is specified in the config file, it is overwritten by the command line option. |
| rankingNP | Sets the `rankingNP` option. In case it is specified in the config file, it is overwritten by the command line option. |
| skipFit | Sets the `skipFit` option. In case it is specified in the config file, it is overwritten by the command line option. |
| statOnly | Sets the `statOnly` option. In case it is specified in the config file, it is overwritten by the command line option. |

## Configuration options
There are two main blocks that need to be provided in the configuration file: `general` and `measurement` block, one optional block `crossNuisanceParameters` and one block for each `measurement`.
The `general` blocks contains some common settings such as the output path.
The `measurement` block just lists the names of the other blocks that represent the individual measurements.
The optional `crossNuisanceParameters` allows user to specify cross-correlations for NPs that should be partially correlated outside of the individual measurements. More information about these blocks can be found in the table below.

### general block
| *Setting* | *Optional?/Default* | *Description*|
| --------- | ----------- | ------------ |
| compareMeasurements | Yes/uses first two measurements | A sequence with the names of the measurements for be added to the pull plot comparison. |
| correlationMatrixThreshold | Yes/0.1 | Threshold that needs to be passed for a given parameter to be included in the correlation matrix plot. |
| createWS | Yes/true | If set to `true`, the code will create a RooFit workspace and then fit it. If set to False, it will only read the workspace. |
| debug | Yes/false | Print debug info? |
| draw | Yes/true | Plot matrices/pulls/post-fit correlation matrix? |
| fixParams | Yes/No parameters fixed | Fix parameters to some value (remove them from the fit) |
|  - parameter | Yes/No parameters fixed | In the "fixParameter" block - name of the parameter to be fixed |
|  - value | Yes/No parameters fixed | In the "fixParameter" block - value of the parameter to be fixed |
| fitStrategy | Yes/1 | Fit strategy value as defined in Minuit2. Has to be between in <0,2>. |
| fixNPsToFitted | Yes/false | Fix NPs to their fitted values. Requires to have the output Parameters.txt file ready (from the standard fit). Can be used to get stat-only result. Produces output with `_fixedNPs` suffix. Will reuse the WS. |
| fractionToCov | Yes/0 | Automatically add the fraction of the NPs with the smallest impact (based on the largest absolute value) to the covariance matrix. |
| inversionPrecision | Yes/1e-3 | An "epsilon" used in some places to make the matrices invertible. Changing this value can help in case some inversion fails. Must be between 0 and 1. |
| minos | Yes/false | Tells the code to run Minos for error estimate |
| minosParameters | Yes/see description | Only valid if `minos` is set to `true`. List of parameters to run MINOS for, if not provided, MINOS will run for all parameters |
| outputPath | No | The path to the folder where the results will be printed |
| parameterRange | No | Range of the POIs to be used for the workspace and fit |
|  - parameter | No | In the "parameterRange" block - name of the parameter |
|  - min | No | In the "parameterRange" block - minimum allowed range for the POI |
|  - max | No | In the "parameterRange" block - maximum allowed range for the POI |
| pearsonChi2 | Yes/false for all POIs | A sequence with the list of POIs where the Pearson chi^2 will be used |
| printParameters | Yes/all POI names | Yaml sequence, a list of parameters to be used in the printed covariance and correlation matrix in the txt files (stored with "_customParams" suffix). |
| printFormat | Yes/png | A sequence with the list of formats for plots, e.g. [png, pdf] |
| ranking | Yes/false | Tells the code to run NP ranking |
| rankingNP | Yes/empty string | If set, the ranking will run only on this NP and produce a text file ending with the name. If an empty string is passed, all NPs are run |
| removeNonConstrainedCovarianceTerms | Yes/true | If set, the code will remove the elements from the matrices and vectors for NPs that have constraint very close to 1 (controlled by `inversionPrecision`). This stabilises the matrix inversion and the fit in general |
| skipFit | Yes/false | Tells the code to skip the standard fit (useful e.g. for ranking) |
| statOnly | Yes/false | Tells the code to ignore systematics |
| threads | Yes/1 | Number of threads to be used for fitting |
| statCovMatrix | Yes/NA | The statistical covariance matrix (its triangular part) for all POIs. Has to match the size of all POIs in all measurements, including the same POIs. Order follows the order of `poiNames` for each `measurement`. If this is not provided, the per-measurement statistical covariance matrices *AND* bootstrap information has to be provided in each `measurement` block. If the per-measurement covariance matrix is not provided, this is estimated from the bootstrap replicas. |
| useCorrelatedNPs | Yes/false | Tells the code to use the likelihood for the statistical correlations between the measurements of the NPs |

### block for each measurement
| *Setting* | *Optional?/Default* | *Description*|
| --------- | ----------- | ------------ |
| configFile | Yes/No file | Path to a yaml file that contains the config options for the given measurement. Added to make it easier to split the configuration file into multiple files. |
| poiNames | No | A sequence of names of the POIs for this measurement, use the same names between measurements to tell the code these are the same parameters |
| values | No | Values for the POIs as measured by the current measurement, have to match the order of `poiNames` |
| fractionToCovPOI | Yes/"" | The nape of the POI used in the automatic decision to add the NPs to the covariance matrix. If not set, the maximum of all impacts is used. |
| statCovMatrix | Mandatory if `statCovMatrix` is not provided in `general` | The statistical covariance matrix (its triangular part) for all the POIs in the given measurement. |
| bootstrapReplicaFile | Yes/NA | The path to the ROOT file with the bootstrap replicas |
| bootstrapReplicaTree | Yes/NA | TTree name inside the ROOT file |
| bootstrapReplicaVariables | Yes/NA | Defined the block for the branches to be read |
| - parameter | No | Name of the parameter for which the values will be read |
|   branchName | No | Branch name inside the TTree to be read from. Has to be of type double |
| systematics | No | Defines a node describing the systematics |
| - parameter | No | In the `systematics` node. Defines the name of the NP. Use the same `parameter` to tell the code the NPs are the same |
| - impact    | No | In the `systematics` node. The impact of the given systematics on POIs, has to match the order of `poiNames` |
| - pull | Yes/No pulls | In the `systematics` node. The pull of the NP |
| - constraint | Yes/No constraint | In the `systematics` node. Constraint of the NP |
| - category | Yes/"Other" | Used for plotting the pull plot, will split the NPs by this category (note that the category will be taken from the first measurement in case of conflicting categories for a given NP) |
| - addToCovariance | Yes/false | In the `systematics` node. If set to `true` the impact of this NP will be added to the statistical only covariance matrix and the NP from this measurement will not be added to the likelihood. Allows to reduce the number of parameters in the fit |
| nuisanceParameters | Yes/See Description | Defines a node describing NP correlations and pulls. If not set, assumes correlation = unity matrix and no pulls. |
| - names | No | In the `nuisanceParameters` node. List of NPs - needed for the order of the NPs provided in correlations/pulls |
| - correlationMatrix | Yes/NA | In the `nuisanceParameters` node. Correlations of the NPs, needs to match the order of `names`. |

### crossNuisanceParameters block
| *Setting* | *Optional?/Default* | *Description*|
| --------- | ----------- | ------------ |
| configFile | Yes/No file | Path to a yaml file that contains the config options for the given crossNuisanceParameters. Added to make it easier to split the configuration file into multiple files. |
| names | No | Names of the NPs that you want to correlate by hand |
| correlationMatrix | Yes/NA | The correlation matrix for the NPs that you want to correlate by hand. |

### reparametrisation block
| *Setting* | *Optional?/Default* | *Description*|
| --------- | ----------- | ------------ |
| configFile | Yes/No file | Path to a yaml file that contains the config options for the given reparametrisation. Added to make it easier to split the configuration file into multiple files. |
| formulae | No | Block for the parameters to be replaced. |
| - replacementFor | No | In the "formulae" node. POI to be replaced |
| - formula | No | In the "replacementFor" node. Mathematical formula, TFormula style, to be used for the reparametrisation |
| dependency | No | Block for the dependencies of the parameters that are replaced replaced. |
| - parameter | No | In the "dependency" node. Name of the parameter |
| - mean | No | In the "parameter" node. Central value of the parameter |
| - min | No | In the "parameter" node. Lower limit for the parameter |
| - max | No | In the "parameter" node. Upper limit for the parameter |

### statNPCorrelations block
| *Setting* | *Optional?/Default* | *Description*|
| --------- | ----------- | ------------ |
| configFile | Yes/No file | Path to a yaml file that contains the config options for the given statNPCorrelations Added to make it easier to split the configuration file into multiple files. |
| correlationMatrix | Yes/NA | The correlation matrix for the stat correlation of the NPs used when 'useCorrelatedNPs' is used. This allows to add the correlations by hand. Useful for debugging. The roder needs to match the order of the provided NPs! |


## Developer Notes

To create a new tag, use `python`, install `tbump` or `pipx` and run:

```
pipx run tbump v0.1.0
# or
tbump v0.1.0
```
