# Set the minimum CMake version required to build the project.
cmake_minimum_required( VERSION 3.6 )

# Silence some warnings on macOS with new CMake versions.
if( NOT ${CMAKE_VERSION} VERSION_LESS 3.9 )
   cmake_policy( SET CMP0068 NEW )
endif()

# Set the project's name and version.
project( Combiner VERSION 0.0.4 )

# Set up the "C++ version" to use.
set( CMAKE_CXX_STANDARD_REQUIRED 14 CACHE STRING
   "Minimum C++ standard required for the build" )
set( CMAKE_CXX_STANDARD 17 CACHE STRING
   "C++ standard to use for the build" )
set( CMAKE_CXX_EXTENSIONS FALSE CACHE BOOL
   "(Dis)allow using compiler extensions" )

set(CMAKE_EXPORT_COMPILE_COMMANDS ON)

# Specify the install locations for libraries and binaries.
set( CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin )
set( CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib )
set( CMAKE_INSTALL_LIBDIR ${CMAKE_BINARY_DIR}/lib )  # Needed by ROOT_GENERATE_DICTIONARY()

# Set the warning flag(s) to use.
set( CMAKE_CXX_FLAGS "-Wall -Wextra -Wshadow -pedantic -O2 -g -fPIC" )

set(CMAKE_POLICY_DEFAULT_CMP0077 NEW)

# ------------------------------------------------
# Dependencies and sub-libraries
# ------------------------------------------------

# Add ROOT system directory and require ROOT.
find_package( ROOT 6.20.04 REQUIRED COMPONENTS Core MathCore RooFitCore RooStats Minuit2 Graf Hist RIO Tree Gpad )

set( YAML_CPP_BUILD_TESTS OFF )
set( YAML_CPP_BUILD_TOOLS OFF )

# include yaml-cpp library
include_directories( yaml-cpp )
add_subdirectory( yaml-cpp )

#link_libraries(stdc++fs)

# Public header files for the shared/static library.
set( lib_headers
     include/Fitter.h
     include/FitUtils.h
     include/Parser.h
     include/Parametrisation.h
     include/Plotter.h
     include/Ranking.h )

# Source files for the shared/static library.
set( lib_sources
     src/Fitter.cc
     src/FitUtils.cc
     src/Parser.cc
     src/Parametrisation.cc
     src/Plotter.cc
     src/Ranking.cc )

# Build the shared library.
add_library( Combiner SHARED ${lib_headers} ${lib_sources} )
target_include_directories( Combiner
   PUBLIC ${ROOT_INCLUDE_DIRS}
   $<BUILD_INTERFACE:${CMAKE_SOURCE_DIR}> $<INSTALL_INTERFACE:> )
 target_link_libraries( Combiner yaml-cpp ${ROOT_LIBRARIES} )
 set_property( TARGET Combiner
   PROPERTY PUBLIC_HEADER ${lib_headers} )
 set_property( TARGET Combiner
   PROPERTY VERSION ${PROJECT_VERSION} )
 set_property( TARGET Combiner
   PROPERTY SOVERSION ${PROJECT_VERSION_MAJOR} )

 target_include_directories(Combiner PUBLIC ${CMAKE_CURRENT_LIST_DIR} )

# Install the libraries.
install( TARGETS Combiner
  EXPORT CombinerTargets
   ARCHIVE DESTINATION lib
   LIBRARY DESTINATION lib
   PUBLIC_HEADER DESTINATION include/Combiner )


# ------------------------------------------------
# Combiner executables
# ------------------------------------------------

# Helper macro for building the project's executables.
macro( Combiner_add_executable name )
  add_executable( ${name} ${ARGN} )
  target_include_directories( ${name} PUBLIC ${ROOT_INCLUDE_DIRS} )
  target_link_libraries( ${name} Combiner yaml-cpp ${ROOT_LIBRARIES} )
  install( TARGETS ${name}
    EXPORT CombinerTargets
    RUNTIME DESTINATION bin )
endmacro( Combiner_add_executable )

Combiner_add_executable( runCombiner util/runCombiner.cc )
