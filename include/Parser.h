#pragma once

#include "TMatrixDSym.h"

#include <map>
#include <memory>
#include <string>
#include <vector>

/// Forward class declaration
namespace YAML {
  class Node;
}
class Fitter;
class TFile;
class TTree;

class Parser {

public:
  /**
   * @brief Construct a new Parser object
   * 
   * @param fitter Pointer to a fitter object 
   */
  explicit Parser(Fitter* fitter);

  /**
   * @brief Destroy the Parser object
   * Closes the opened ROOT files
   * 
   */
  ~Parser();

  /**
   * @brief Delete move/asignment/copy constructors
   * 
   * @param rhs 
   */
  Parser(const Parser& rhs) = delete;
  Parser& operator=(const Parser& rhs) = delete;
  Parser(Parser&& rhs) = delete;
  Parser& operator=(Parser&& rhs) = delete;

  /**
   * @brief Prints config summary 
   * 
   */
  void PrintSummary() const;
  
  /**
   * @brief Reads the provided yaml file
   * 
   * @param path Path to the yaml file 
   */
  void ReadConfigFile(const std::string& path);

  /**
   * @brief Process the commnd line arguments
   * 
   * @param params 
   */
  void ReadCommandLine(const std::vector<std::string>& params);

  /**
   * @brief convert string to a boolean in a case insensitive way 
   * 
   * @param s 
   * @return true 
   * @return false 
   */
  static bool StringToBoolean(std::string s);

  /**
   * @brief Split string based on a character 
   * 
   * @param s string to split
   * @param c the character to split by
   * @return std::vector<std::string> 
   */
  static std::vector<std::string> Vectorize(const std::string& s, const char c);

private:

  /**
   * @brief Add systematics to the stat covariance 
   * 
   * @param cov 
   */
  void AddSystToStatCov(TMatrixDSym* cov) const;

  /**
   * @brief A helper function to calculate correlations between tho sets of replicas
   * Then, using cov_i,j = sigma_i * corr_i,j * sigma_j the combined covariance matrix is built 
   * 
   * @param vec1 Set of results for replicas for one POI
   * @param vec2 Set of results for replicas for another POI
   * @return double Correlation
   */
  static double CalculateBootsrapCorrelation(const std::vector<double>& vec1, const std::vector<double>& vec2);

  /**
   * @brief Returns RMS from the values 
   * 
   * @param vec 
   * @return double 
   */
  static double CalculateRMS(const std::vector<double>& vec);

  /**
   * @brief Checks if the values in a correlation matrix are between -1 and 1 
   * 
   * @param values Values representing the matrix 
   * @return true if it is okay 
   * @return false if values are outside of <-1,1>
   */
  static bool CheckCorrelationMatrix(const std::vector<std::vector<double> >& values);

  /**
   * @brief Checks if a node containts unsupported parameters 
   * 
   * @param names names of the parameters to check
   * @return true 
   * @return false 
   */
  bool CheckNode(const YAML::Node& node, const std::vector<std::string>& names) const;
  
  /**
   * @brief Check Measurement node for parameters
   * 
   * @param node 
   * @return true 
   * @return false 
   */
  bool CheckMeasurementNode(const YAML::Node& node) const;
  
  /**
   * @brief Check nuisanceParameter node for parameters 
   * 
   * @param node 
   * @return true 
   * @return false 
   */
  bool CheckNuisanceParameterNode(const YAML::Node& node) const;
  
  /**
   * @brief Check systematics node for parameters
   * 
   * @param node 
   * @return true 
   * @return false 
   */
  bool CheckSystematicsNode(const YAML::Node& node) const;

  /**
   * @brief Get the covariance from the yaml file. Tried to read the values directly form the file, if it fails, then it reads from the ROOT files
   * 
   * @param node YAML node 
   * @param name for the values directly in the config
   * @param measurement name of the measurement
   * @return std::vector<std::vector<double> > triangular part of the matrix
   */
  std::vector<std::vector<double> > GetCovarianceMatrix(const YAML::Node& node,
                                                        const std::string& name,
                                                        const std::string& measurement);
  
  /**
   * @brief Get the the NPs from the node and sets them to the fitter object
   * 
   * @param node YAML node 
   * @param nSyst number of systematics for a given measurement
   * @param measurement name of the measurement
   */
  TMatrixDSym GetNuisanceParameters(const YAML::Node& node, const std::size_t nSyst, const std::string& measurement);
  
  /**
   * @brief Get set of values from a YAML sequence. First tries to read it from the file, if it fail, then it reads it from a ROOT file
   * 
   * @param node YAML node
   * @param name for the sequence directly in the config
   * @param nodeName name of the node
   * @return std::vector<double> values 
   */
  std::vector<double> GetValues(const YAML::Node& node,
                                const std::string& name,
                                const std::string& nodeName) const;

  /**
   * @brief Turn 2D vector into 1D for a given index
   * 
   * @param vec 
   * @param index 
   * @return std::vector<double> 
   */
  std::vector<double> GetVector(const std::vector<std::vector<double> >& vec, const std::size_t index) const;

  /**
   * @brief Open yaml file and return the pointed to the topmost node 
   * 
   * @param path Path to the file
   * @return YAML::Node 
   */
  static YAML::Node OpenConfigFile(const std::string& path);

  /**
   * @brief Searches a map with the path as the key, if it is there, return the pointer tothe file, if not, open it and add it to the map 
   * 
   * @param path ROOT file path 
   * @return std::shared_ptr<TFile> 
   */
  std::shared_ptr<TFile> OpenRootFile(const std::string& path);

  /**
   * @brief Read the impacts for a given measurements that can be sued for automatic addition to the covariance 
   * 
   * @param node 
   * @return std::vector<std::pair<std::string, std::vector<double > > > impacts per systematics 
   */
  std::vector<std::pair<std::string, std::vector<double > > > PreReadImpacts(const YAML::Node& node) const;

  /**
   * @brief Process the NP stat correlations provided by hand 
   * 
   */
  void ProcessByHandStatNPCorrelations();

  /**
   * @brief A helper function to process bootstrap replicas and turn them into the covariance matrix 
   * 
   */
  void ProcessBootstraps();
  
  /**
   * @brief A helper function to process bootstrap replicas for NPs and turn them into the covariance matrix 
   * 
   */
  void ProcessBootstrapsNPs();

  /**
   * @brief Process replicas into a covariance matrix 
   * 
   */
  void ProcessCovarianceMatrixFromReplicas();

  /**
   * @brief Reads the bootstraps and processes them
   * 
   * @param node YAML node 
   */
  void ReadBootstraps(const YAML::Node& node);

  /**
   * @brief Reads the compare measurements options 
   * 
   * @param node 
   */
  void ReadCompareMeasurements(const YAML::Node& node);

  /**
   * @brief Read the correlation threshold for plotting 
   * 
   * @param node 
   */
  void ReadCorrelationThreshold(const YAML::Node& node);

  /**
   * @brief Reads the create WS option 
   * 
   * @param node 
   */
  void ReadCreateWS(const YAML::Node& node);

  /**
   * @brief Read the cross nuisance parameter settings 
   * 
   * @param node 
   */
  void ReadCrossNPs(const YAML::Node& node);

  /**
   * @brief Read debug info 
   * 
   * @param node 
   */
  void ReadDebug(const YAML::Node& node);

  /**
   * @brief Read draw option
   * 
   * @param node 
   */
  void ReadDraw(const YAML::Node& node);

  /**
   * @brief Read fit strategy
   * 
   * @param node 
   */
  void ReadFitStrategy(const YAML::Node& node);

  /**
   * @brief Read the parameters to be fixed 
   * 
   * @param node 
   */
  void ReadFixParams(const YAML::Node& node);

  /**
   * @brief For stat-only fit where all NPs are fixed to the fitted values
   * 
   * @param node 
   */
  void ReadFixNPsToFitted(const YAML::Node& node);

  /**
   * @brief Read the fraction of NPs to be added to the covariance matrix 
   * 
   * @param node 
   */
  void ReadFractionToCov(const YAML::Node& node);

  /**
   * @brief Read the POI used in the automatic fractionToCov decision 
   * 
   * @param node 
   */
  void ReadFractionToCovPOI(const YAML::Node& node); 

  /**
   * @brief Read inversionPrecision setting 
   * 
   * @param node 
   */
  void ReadInversionPrecision(const YAML::Node& node);

  /**
   * @brief Read minos options 
   * 
   * @param node YAML node 
   * @return std::pair<bool, std::vector<std::string> > first value is a flag if minos is used, second is the list of parameters to run minos on
   */
  std::pair<bool, std::vector<std::string> > ReadMinos(const YAML::Node& node);

  /**
   * @brief Read nuisanceParameters and passes the info to the Fitter object 
   * 
   * @param node YAML node 
   * @param nSyst number of systematics for a given measurement 
   * @param measurement name of the measurement
   * @return returns true of the block exists in the config 
   */
  bool ReadNuisanceParameters(const YAML::Node& node, const std::size_t nSyst, const std::string& measurement);
  
  /**
   * @brief Read the outputPath config 
   * 
   * @param node YAML node 
   * @return std::string 
   */
  std::string ReadOutputPath(const YAML::Node& node) const;

  /**
   * @brief Read a flag to set Pearson chi^2 for given POIs 
   * 
   * @param node 
   * @return the list of POIs where the Pearson chi^2 is used
   */
  std::vector<std::string> ReadPearsonChi2(const YAML::Node& node) const;

  /**
   * @brief Read the printParameters object 
   * 
   * @param node 
   */
  void ReadPrintParameters(const YAML::Node& node) const;

  /**
   * @brief Read print format 
   * 
   * @param node 
   */
  void ReadPrintFormat(const YAML::Node& node);
  
  /**
   * @brief Read the paremeter ranges from the general block 
   * 
   * @param node 
   */
  void ReadRanges(const YAML::Node& node);

  /**
   * @brief Read a flag to tell the code to run ranking 
   * 
   * @param node 
   */
  void ReadRanking(const YAML::Node& node) const;

  /**
   * @brief 
   * 
   * @param node 
   */
  void ReadRankingNP(const YAML::Node& node) const;

  /**
   * @brief Read removeCovarianceTerms
   * 
   * @param node 
   */
  void ReadRemoveCovarianceTerms(const YAML::Node& node);

  /**
   * @brief Read reparametrisation 
   * 
   * @param node 
   */
  void ReadReparametrisation(const YAML::Node& node);

  /**
   * @brief Read and set one measurement block 
   * 
   * @param node YAML node 
   * @param name name of the measurement 
   */
  void ReadSingleMeasurement(const YAML::Node& node, const std::string& name);
  
  /**
   * @brief Read a flag if the fit should be skipped (e.g. for ranking only) 
   * 
   * @param node 
   */
  void ReadSkipFit(const YAML::Node& node) const;

  /**
   * @brief Read stat covariance matrix either from config 
   * 
   * @param node YAML node 
   * @param name name of the node 
   * @return returns true if the setting is valid
   */
  bool ReadStatCovarianceMatrix(const YAML::Node& node, const std::string& name);

  /**
   * @brief Read stat NP correlations provided in the config 
   * 
   * @param node 
   */
  void ReadStatNPCorrelations(const YAML::Node& node);

  /**
   * @brief Read the statOnly flag 
   * 
   * @param node 
   * @return true 
   * @return false 
   */
  bool ReadStatOnly(const YAML::Node& node) const;

  /**
   * @brief Read the number of threads used 
   * 
   * @param node 
   */
  void ReadThreads(const YAML::Node& node) const;

  /**
   * @brief Reads TTree branches based on the ordered list of the branches.
   * All branches are assumed to me double 
   * 
   * @param tree The tree to be read 
   * @param branches Ordered list of the branches
   * @return std::vector<std::vector<double> > 
   */
  std::vector<std::vector<double> > ReadTree(TTree* tree, const std::vector<std::string>& branches);

  /**
   * @brief Read the useCorrelatedNPs flag 
   * 
   * @param node 
   * @return true 
   * @return false 
   */
  bool ReadUseCorrelatedNPs(const YAML::Node& node);

  /**
   * @brief A helper function to turn the triagonal values in vectors into TMatrixDSym 
   * 
   * @param values 
   * @return TMatrixDSym 
   */
  static TMatrixDSym VectorToMatrix(const std::vector<std::vector<double> >& values);

  /**
   * @brief number of bins for each measurement 
   * 
   */
  std::vector<std::size_t> m_bins;

  /**
   * @brief Total number of bins at a given time 
   * 
   */
  std::size_t m_binSizes;

  /**
   * @brief Matrix of correlations of NPs provided in the config (not from bootstraps) 
   * 
   */
  TMatrixDSym m_byHandNPcorrelations;

  /**
   * @brief do we create the WS? 
   * 
   */
  bool m_createWS;

  /**
   * @brief Print debug info? 
   * 
   */
  bool m_debug;

  /**
   * @brief map with the TFile pointers 
   * 
   */
  std::map<std::string, std::shared_ptr<TFile> > m_fileMap;

  /**
   * @brief The main Fitter object 
   * 
   */
  Fitter* m_fitter;

  /**
   * @brief the fraction of NPs to be added to the cov matrix 
   * 
   */
  double m_fractionToCov;

  /**
   * @brief 
   * 
   */
  std::string m_fractionToCovPOI;

  /**
   * @brief Flag to tell if the nuisanceParameters were provided in the config for a given measurement
   * 
   */
  std::vector<bool> m_hasNuisanceParameters;

  /**
   * @brief Flag to tell if the statCovMatrix was provided in the general block were provided in the config
   * 
   */
  bool m_hasStatInGeneral;

  /**
   * @brief Precision used for some amtrices to make them invertible 
   * 
   */
  double m_inversionPrecision;

  /**
   * @brief Names of the measurements
   * 
   */
  std::vector<std::string> m_measurements;

  /**
   * @brief NP constraint per measurement 
   * 
   */
  std::vector<std::vector<double> > m_npConstraint;

  /**
   * @brief 
   * 
   */
  std::vector<TMatrixDSym> m_npCorrelations;

  /**
   * @brief NP per measurement 
   * 
   */
  std::vector<std::string> m_npPerMeas;

  /**
   * @brief names of the NPs per measurement 
   * 
   */
  std::vector<std::vector<std::string> > m_npNames;

  /**
   * @brief Order of the NPs for the correlation matrix per measurement 
   * 
   */
  std::vector<std::vector<std::string> > m_npNamesOrder;

  /**
   * @brief number of NPs per measurement 
   * 
   */
  std::vector<std::size_t> m_nps;

  /**
   * @brief list of the POIs for a single measurement 
   * 
   */
  std::vector<std::string> m_pois;

  /**
   * @brief Bootstrap replica results, measurement|replica|bin
   * 
   */
  std::vector<std::vector<std::vector<double> > > m_replicas;
  
  /**
   * @brief Bootstrap replicas results for the NPs, measurement|replica|bin
   * 
   */
  std::vector<std::vector<std::vector<double> > > m_replicasNPs;

  /**
   * @brief List of skipped NPs that are added to the covariance matrix measurement|parameters 
   * 
   */
  std::vector<std::vector<std::string> > m_skippedNPs;

  /**
   * @brief Stat covariance matrices for each measurement
   * 
   */
  std::vector<TMatrixDSym> m_statCovMatrices;

  /**
   * @0brief Is stat only fit? 
   * 
   */
  bool m_statOnly;

  /**
   * @brief values to be added to the covariance matrix measurement|POI 
   * 
   */
  std::vector<std::vector<double> > m_toCovariance;

  /**
   * @brief use correlated NPs? 
   * 
   */
  bool m_useCorrelatedNPs;

  /**
   * @brief categories of NPs for plotting 
   * 
   */
  std::vector<std::string> m_npUniqueCategories;
};
