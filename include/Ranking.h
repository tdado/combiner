#pragma once

#include <string>
#include <tuple>
#include <unordered_map>
#include <vector>

class RooWorkspace;
class RooFitResult;
class RooArgList;

class Ranking {

public:

  /**
   * @brief Construct a new Ranking object
   * 
   */
  explicit Ranking();

  /**
   * @brief Destroy the Ranking object
   * 
   */
  ~Ranking() = default;

  /**
   * @brief Read the results of the standard fit
   * 
   * @param path path to the output fit file
   */
  void ReadFileResults(const std::string& path);

  /**
   * @brief Run the actual ranking
   * 
   * @param ws workspace
   * @param globObservables global observalbes 
   * @param nps nuisance parameters
   */
  void RunRanking(RooWorkspace* ws, const RooArgList& globObservables, const std::vector<std::string>& nps);

  /**
   * @brief Set the Debug value
   * 
   * @param debug 
   */
  inline void SetDebug(const bool debug) {m_debug = debug;}

  /**
   * @brief Set the Fit Strategy 
   * 
   * @param strategy 
   */
  inline void SetFitStrategy(const int strategy) {m_fitStrategy = strategy;}

  /**
   * @brief Set the names of the POIs
   * 
   * @param names 
   */
  inline void SetPoiNamesUnique(const std::vector<std::string>& names) {m_poiNamesUnique = names;}

  /**
   * @brief Set the number of threads
   * 
   * @param threads 
   */
  inline void SetThreadN(const bool threads) {m_threadN = threads;}
  
  /**
   * @brief Write the ranking result to a file
   * 
   * @param folder folder where the new file will appear
   * @param suffix suffic for the files
   */
  void WriteRankingToFile(const std::string& folder, const std::string& suffix) const;

private:

  /**
   * @brief class to store the fit results
   * 
   */
  struct Impact {
    /**
     * @brief central value
     * 
     */
    double nominal;

    /**
     * @brief plus uncertainty
     * 
     */
    double up;

    /**
     * @brief mnus uncertainty
     * 
     */
    double down;
  };

  /**
   * @brief debug? 
   * 
   */
  bool m_debug;

  /**
   * @brief fit strategy
   * 
   */
  int m_fitStrategy;

  /**
   * @brief NP names
   * 
   */
  std::vector<std::string> m_npNames;

  /**
   * @brief Results of the standard fit
   * 
   */
  std::unordered_map<std::string, Impact> m_result;

  /**
   * @brief Names of the POIs
   * 
   */
  std::vector<std::string> m_poiNamesUnique;

  /**
   * @brief ranking result
   * 
   */
  std::vector<std::vector<std::tuple<double, double, double, double, std::string> > > m_ranking;

  /**
   * @brief number of threads
   * 
   */
  int m_threadN;
};
