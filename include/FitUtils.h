#pragma once

#include <string>
#include <vector>

class RooAbsData;
class RooAbsPdf;

namespace FitUtils {

    /**
     * @brief Fixes the parameters to provided values
     * 
     * @param model pdf
     * @param data data
     * @param params parameters to be fixed
     */
    void FixParameters(RooAbsPdf* model,
                       RooAbsData* data,
                       const std::vector<std::pair<std::string, double> >& params);
}