#pragma once

#include "include/Parametrisation.h"

#include "TMatrixDSym.h"

#include "RooMultiVarGaussian.h"

#include <map>
#include <memory>
#include <string>
#include <vector>

class RooAbsReal;
class RooArgList;
class RooWorkspace;

class Fitter {

public:

  /**
   * @brief Construct a new Fitter object
   * 
   */
  explicit Fitter();

  /**
   * @brief Destroy the Fitter object
   * 
   */
  ~Fitter() = default;

  /**
   * @brief Add a parameter to be fixed 
   * 
   * @param name 
   * @param value 
   */
  void AddFixParam(const std::string& name, const double value);

  /**
   * @brief Add a systematic to the list. If it exists, check if some padding 0 need to be added, then add the values.
   * If it does not exist, add 0s if needed and then add the values
   * 
   * @param name Name of the NP
   * @param impact values for the impact
   * @param totalSize the total number of bins after the current ones are added
   */
  void AddSystematic(const std::string& name,
                     const std::vector<double>& impact,
                     const std::size_t totalSize);

  /**
   * @brief Add the NuisanceParameter correlations object
   * 
   * @param cor the correlations for the NPs for each measurement
   */
  void AddNPCorrelation(const TMatrixDSym& cor);

  /**
   * @brief Add the ordered list of the NPs that are used in the correlation per measurement
   * 
   * @param names the ordered list of NP names 
   */
  inline void AddNPCorrelationNames(const std::vector<std::string>& names) {m_npCorrelationNames.emplace_back(names);}

  /**
   * @brief Add NP constraints for one measurement 
   * 
   * @param vec 
   */
  inline void AddNPConstraints(const std::vector<double>& vec) {m_npInputConstraints.emplace_back(vec);}

  /**
   * @brief Add NP names per measurement 
   * 
   * @param names 
   */
  inline void AddNPNames(const std::vector<std::string>& names) {m_npNames.emplace_back(names);}

  /**
   * @brief Add a parameter with its range to the map 
   * 
   * @param name name of the POI
   * @param min minimum
   * @param max maximum
   */
  void AddParameterRange(const std::string& name, const double min, const double max);

  /**
   * @brief Set the Pulls object
   * 
   * @param pulls pulls for each NPs
   */
  inline void AddPulls(const std::vector<double>& pulls) {m_pulls.emplace_back(pulls);}

  /**
   * @brief Add a single reparametrisation rule 
   * 
   * @param para 
   */
  inline void SetReparametrisation(const Parametrisation& para) {m_reparametrisation = para;}

  /**
   * @brief Add skipped NPs for a measurement 
   * 
   * @param nps 
   */
  inline void AddSkippedNPs(const std::vector<std::string>& nps) {m_skippedNPs.emplace_back(nps);}

  /**
   * @brief Add POI value and its name to the list of POIs and values. Also identify unique POIs and values - needed for correlating POIs
   * 
   * @param value 
   * @param pois 
   */
  void AddValuePOI(const double value, const std::string& poi);
  
  /**
   * @brief Turn Correlations and sigmas into a covariance matrix
   * 
   * @param cor correlation matrix 
   * @param sigmas uncertainties
   * @return TMatrixDSym 
   */
  static TMatrixDSym CorrelationToCovariance(const TMatrixDSym& cor, const std::vector<double>& sigmas);
  
  /**
   * @brief Turn covaraince into correlation 
   * 
   * @param cov covariance matrix 
   * @return TMatrixDSym 
   */
  static TMatrixDSym CovarianceToCorrelation(const TMatrixDSym& cov);

  /**
   * @brief Get the draw option
   * 
   * @return true 
   * @return false 
   */
  inline bool GetDraw() const {return m_draw;}

  /**
   * @brief Get the NP input constraints
   * 
   * @return const std::vector<std::vector<double> >& 
   */
  inline const std::vector<std::vector<double> >& GetInputConstraints() const {return m_npInputConstraints;}

  /**
   * @brief Return the POI names 
   * 
   * @return const std::vector<std::string>& 
   */
  inline const std::vector<std::string>& GetPOINames() const {return m_poiNamesUnique;}

  /**
   * @brief Get the print format settings
   * 
   * @return const std::vector<std::string>& 
   */
  inline const std::vector<std::string>& GetPrintFormat() const {return m_printFormat;}

  /**
   * @brief Get the Reparametrised object
   * 
   * @return const std::vector<Parametrisation>& 
   */
  inline const Parametrisation& GetReparametrised() const {return m_reparametrisation;}

  /**
   * @brief Get the Stat Covariance Matrix
   * 
   * @return const TMatrixDSym& 
   */
  inline const TMatrixDSym& GetStatCovarianceMatrix() const {return m_statCovariance;}

  /**
   * @brief Get the Systematic Map object
   * 
   * @return const std::map<std::string, std::vector<double> >& 
   */
  inline const std::map<std::string, std::vector<double> >& GetSystematicMap() const {return m_systematicMap;}

  /**
   * @brief Get the Use Correlated NPs
   * 
   * @return true 
   * @return false 
   */
  inline bool GetUseCorrelatedNPs() const {return m_useCorrelatedNPs;}

  /**
   * @brief Get the use minos
   * 
   */
  inline const std::pair<bool, std::vector<std::string> >& GetUseMinos() const { return m_useMinos;}

  /**
   * @brief Get the Use Pearson Chi 2 object
   * 
   * @return the list of POIs where Pearson chi^2 is used 
   */
  inline std::vector<std::string> GetUsePearsonChi2() const { return m_usePearsonChi2;}

  /**
   * @brief Multiply two TMAtridxDSyms
   * 
   * @param a 
   * @param b 
   */
  static TMatrixDSym MultiplyMatrices(const TMatrixDSym& a, const TMatrixDSym& b);

  /**
   * @brief Print debug info 
   * 
   */
  void PrintDebugInfo() const;

  /**
   * @brief Read the fitted values of the NPs to fix them if needed 
   * 
   */
  void ReadFittedNPs();

  /**
   * @brief Runs the fit
   * 
   */
  void RunFit();

  /**
   * @brief Set the names of the measurements to compare
   * 
   * @param measurements 
   */
  inline void SetCompareMeasurements(const std::vector<std::string>& measurements) {m_compareMeasurements = measurements;}

  /**
   * @brief Set the Correlation Threshold value for the plotting
   * 
   * @param threshold 
   */
  inline void SetCorrelationThreshold(const double threshold) {m_correlationThreshold = threshold;}

  /**
   * @brief Flag to recreate the WS
   * 
   * @param flag 
   */
  inline void SetCreateWS(const bool flag){m_createWS = flag;}

  /**
   * @brief Set the Cross NP Names object
   * 
   * @param names 
   */
  inline void SetCrossNPNames(const std::vector<std::string>& names) {m_crossNPNames = names;}

  /**
   * @brief Set the Cross NP correlations
   *  
   * @param cor 
   */
  inline void SetCrossNPCorrelations(const TMatrixDSym& cor) {m_crossNPCorrelation.ResizeTo(cor); m_crossNPCorrelation = cor;}

  /**
   * @brief Set the Debug object
   * 
   * @param debug 
   */
  inline void SetDebug(const bool debug) {m_debug = debug;}

  /**
   * @brief Set the draw flag
   * 
   * @param flag 
   */
  inline void SetDraw(const bool flag) {m_draw = flag;}

  /**
   * @brief Set the Fit Strategy
   * 
   * @param strategy 
   */
  inline void SetFitStrategy(const int strategy) {m_fitStrategy = strategy;}

  /**
   * @brief Set the Fix NPs To Fitted flag
   * 
   * @param flag 
   */
  inline void SetFixNPsToFitted(const bool flag) {m_fixNPsToFitted = flag;}

  /**
   * @brief Set the Inversion Precision object
   * 
   * @param val 
   */
  inline void SetInversionPrecision(const double val) {m_inversionPrecision = val;}

  /**
   * @brief Set the names of the measurements
   * 
   * @param measurements 
   */
  inline void SetMeasurements(const std::vector<std::string>& measurements) {m_measurements = measurements;}

  /**
   * @brief Set the number of threads to be used
   * 
   * @param n 
   */
  inline void SetNthreads(const std::size_t n) {m_threadN = n;}

  /**
   * @brief Set number of POIs per measurement 
   * 
   * @param pois 
   */
  inline void SetNPOIs(const std::vector<std::size_t>& pois) {m_nPOIs = pois;}

  /**
   * @brief Set the Plot Parameters object
   * 
   * @param params 
   */
  inline void SetPrintParameters(const std::vector<std::string>& params) {m_printParameters = params;}

  /**
   * @brief Set the print format
   * 
   * @param formats 
   */
  inline void SetPrintFormat(const std::vector<std::string>& formats) {m_printFormat = formats;}

  /**
   * @brief Set the Stat Covariance object
   * 
   * @param cov the total (combined for all measurements) stat covariance
   */
  inline void SetStatCovariance(const TMatrixDSym& cov) {m_statCovariance.ResizeTo(cov); m_statCovariance = cov;}

  /**
   * @brief Set the Output Path object
   * 
   * @param path path to the output
   */
  inline void SetOutputPath(const std::string& path) {m_outPath = path;}

  /**
   * @brief Set the run ranking flag
   * 
   * @param flag 
   */
  inline void SetRanking(const bool flag) {m_runRanking = flag;}

  /**
   * @brief Set the Ranking N Ps object
   * 
   * @param nps 
   */
  inline void SetRankingNP(const std::string& np) {m_rankingNP = np;}

  /**
   * @brief Set the removeCovTerms flag
   * 
   * @param flag 
   */
  inline void SetRemoveCovTerms(const bool flag) {m_removeCovarianceTerms = flag;}

  /**
   * @brief Set the the skipFit
   * 
   * @param flag 
   */
  inline void SetSkipFit(const bool flag) {m_skipFit = flag;}

  /**
   * @brief Set the Stat Only object
   * 
   * @param isStatOnly 
   */
  inline void SetStatOnly(const bool isStatOnly) {m_statOnly = isStatOnly;}

  /**
   * @brief Set the Total NP Covariance in case of the correlated NPs
   * 
   */
  inline void SetTotalNPCovarianceCorrelated(const TMatrixDSym& cov) {m_totalNPcovarianceCorrelated.ResizeTo(cov); m_totalNPcovarianceCorrelated = cov;}

  /**
   * @brief Set the Use Correlated NPs
   * 
   * @param use 
   */
  inline void SetUseCorrelatedNPs(const bool use) {m_useCorrelatedNPs = use;}

  /**
   * @brief Set the use of minos
   * 
   * @param use 
   */
  inline void SetUseMinos(const std::pair<bool, std::vector<std::string> > use) {m_useMinos = use;}
  
  /**
   * @brief Set the Use Pearson Chi 2 object
   * 
   * @param use list of the POIs where the Pearson chi^2 is used 
   */
  inline void SetUsePearsonChi2(const std::vector<std::string>& use) {m_usePearsonChi2 = use;}

  /**
   * @brief Set per NP categories
   * 
   * @param categories 
   */
  inline void AddNPCategories(const std::vector<std::string>& categories) {m_npCategories.emplace_back(categories);}

  /**
   * @brief Set Unique NP categories 
   * 
   * @param categories 
   */
  inline void SetNPUniqueCategories(const std::vector<std::string>& categories) {m_npUniqueCategories = categories;}

private:

  /**
   * @brief Rescale the stat covariance matrix to get determinant closer to 1
   * 
   */
  void AdjustStatCovarianceMatrix();

  /**
   * @brief Add 0s to the impact if a NP is not present in the last measurement 
   * 
   */
  void AdjustImpactSizes();

  /**
   * @brief Get the Global Observables from a model
   * 
   * @param model 
   * @param data 
   * @return RooArgList 
   */
  RooArgList GetGlobalObservables(const RooAbsPdf* model, const RooAbsData* data) const;

  /**
   * @brief Get the list of the POIs 
   * 
   * @param model 
   * @param data 
   * @return RooArgList 
   */
  RooArgList GetPOIs(const RooAbsPdf* model, const RooAbsData* data);
  
  /**
   * @brief Get the list of the NPs 
   * 
   * @param model 
   * @param data 
   * @return RooArgList 
   */
  RooArgList GetNPs(const RooAbsPdf* model, const RooAbsData* data);

  /**
   * @brief The main method that constructs the chi^2 in RooFit
   * 
   */
  void PrepareChi2();

  /**
   * @brief Prepare the chi^2 term with the stat covariance 
   * 
   */
  void PrepareStatGaussian(RooArgList* obs, RooWorkspace* ws) const;

  /**
   * @brief Prepare the chi^2 term with the np correlations 
   * 
   */
  void PrepareNpCorrelationGaussian(RooWorkspace* ws) const;

  /**
   * @brief Prepare the chi^2 term with the np pulls and constraints 
   * 
   */
  void PrepareNpConstraintGaussian(RooWorkspace* ws) const;

  /**
   * @brief Print fitted parameters
   * 
   * @param fr 
   */
  void PrintParameters(const RooFitResult* fr) const;

  /**
   * @brief Print correlation and covariance matrices 
   * 
   * @param fr 
   */
  void PrintCorrelationCovariance(const RooFitResult* fr, const bool customParams) const;

  /**
   * @brief Prepare the C^-1 and I^-1 terms and the pulls 
   * 
   */
  void ProcessCovarianceTerms();
  
  /**
   * @brief Prepare the C^-1 and I^-1 terms and the pulls for the corelated NPs
   * 
   */
  void ProcessCovarianceTermsCorrelated();

  /**
   * @brief Additional processing of inputs before passing them to the fit
   * This includes finding the indices of the POIs - needed for same POI fits
   * as well as finding the indices for the NPs for the cross-correlalation term.
   * Finally, it also calculates the correction factors k. 
   * 
   */
  void ProcessInputs();

  /**
   * @brief 
   * 
   */
  void ProcessPOIPulls();

  /**
   * @brief 
   * 
   * @param pulls Remove the elements from the matrices and vectors for NPs where the constrain is exactly 1
   * also midified the list of the NPs for the given measurement
   * @param cov Original cov matrix for a given measurement
   * @return std::pair<std::vector<double>, TMatrixDSym> updated pulls and the covariance matrix
   */
  std::pair<std::vector<double>, TMatrixDSym> RemoveElements(const std::vector<double>& pulls, const TMatrixDSym& cov);

  /**
   * @brief Remove the elements from the matrices and vectors for NPs where the constraint is exactly 1 
   * @param pulls the concatenated pulls of the NPs
   * 
   * @return std::vector<double> reduced set of pulls
   * 
   */
  std::vector<double> RemoveElementsCorrelated(const std::vector<double>& pulls);

  /**
   * @brief Ordered list of all unique NPs; 
   * 
   */
  std::vector<std::string> m_allNPs;
  
  /**
   * @brief Ordered list of all unique NPs after removing the ones with constraint = 1 
   * 
   */
  std::vector<std::vector<std::string> > m_allNPsReduced;

  /**
   * @brief Names of the measurements for plotting
   * 
   */
  std::vector<std::string> m_compareMeasurements;

  /**
   * @brief Threshold for the correlation matrix plotting 
   * 
   */
  double m_correlationThreshold;

  /**
   * @brief Cross NP correlations
   * 
   */
  TMatrixDSym m_crossNPCorrelation;

  /**
   * @brief Names of the cross NPs 
   * 
   */
  std::vector<std::string> m_crossNPNames;

  /**
   * @brief C^-1 - I^-1 term 
   * 
   */
  std::vector<TMatrixDSym> m_cx;
  
  /**
   * @brief (C^-1 - I^-1)^-1 term
   * 
   */
  std::vector<TMatrixDSym> m_cxInv;

  /**
   * @brief  C^-1 - I^-1 term for correlated NPs
   * 
   */
  TMatrixDSym m_cx_correlated;

  /**
   * @brief (C^-1 - I^-1) ^ -1 term for the correlated NPs
   * 
   */
  TMatrixDSym m_cxInv_correlated;

  /**
   * @brief (C^-1 - I^-1)^-1 * C^-1 term 
   * 
   */
  std::vector<TMatrixDSym> m_cxInvC;
  
  /**
   * @brief (C^-1 - I^-1)^-1 * C^-1 term for correlated NPs
   * 
   */
  TMatrixDSym m_cxInvC_correlated;

  /**
   * @brief create the WS or not 
   * 
   */
  bool m_createWS;

  /**
   * @brief Print debug info 
   * 
   */
  bool m_debug;

  /**
   * @brief Flag to draw plots 
   * 
   */
  bool m_draw;

  /**
   * @brief Fit strategy 
   * 
   */
  int m_fitStrategy;

  /**
   * @brief Flag to control fixing NPs to the fitted values 
   * 
   */
  bool m_fixNPsToFitted;
  
  /**
   * @brief List of parameters to be fixed in the fit (name, value)
   * 
   */
  std::vector<std::pair<std::string, double> > m_fixParams;

  /**
   * @brief Precision used for some matrices that cannot be inverted 
   * 
   */
  double m_inversionPrecision;

  /**
   * @brief Names of the measurements 
   * 
   */
  std::vector<std::string> m_measurements;

  /**
   * @brief Ordered names of the NPs that go into the correlation matrix per measurement 
   * 
   */
  std::vector<std::vector<std::string> > m_npCorrelationNames;

  /**
   * @brief NP correlations matrix
   * 
   */
  std::vector<TMatrixDSym> m_npCorrelations;

  /**
   * @brief Input NP constraints as read from the config 
   * 
   */
  std::vector<std::vector<double> > m_npInputConstraints;

  /**
   * @brief names of the NPs for each measurements 
   * 
   */
  std::vector<std::vector<std::string> > m_npNames;

  /**
   * @brief concatenated names of the NPs for the correlated NPs likelihood 
   * 
   */
  std::vector<std::string> m_npsCorrelated;

  /**
   * @brief size of the POIs in each measurement 
   * 
   */
  std::vector<std::size_t> m_nPOIs;

  /**
   * @brief Path to the output folder 
   * 
   */
  std::string m_outPath;

  /**
   * @brief Ranges of the POIs for the workspace 
   * 
   */
  std::map<std::string, std::pair<double,double> > m_parameterRanges; 

  /**
   * @brief flag for each POI if it uses the pearson chi^2 
   * 
   */
  std::vector<bool> m_pearsonIndices;

  /**
   * @brief Parameters to be printed in the correlation and covariance matrices
   * 
   */
  std::vector<std::string> m_printParameters;

  /**
   * @brief names of the POIs 
   * 
   */
  std::vector<std::string> m_poiNames;

  /**
   * @brief names of the reparametrised POIs
   * 
   */
  std::vector<std::string> m_poiNamesReparametrised;

  /**
   * @brief 
   * 
   */
  std::vector<std::string> m_poiNamesUnique;

  /**
   * @brief pulls for NPs for a given POI 
   * 
   */
  std::vector<std::vector<double> > m_poiPulls;

  /**
   * @brief Format of the plots, e.g. "png"
   * 
   * @return std::vector<std::string> 
   */
  std::vector<std::string> m_printFormat;

  /**
   * @brief Input pulls of the NPs
   * 
   */
  std::vector<std::vector<double> > m_pulls;

  /**
   * @brief list of NPs to run over 
   * 
   */
  std::string m_rankingNP;

  /**
   * @brief flag to remove elements in the covariance with constraint = 1 
   * 
   */
  bool m_removeCovarianceTerms;

  /**
   * @brief Reparametrisation information 
   * 
   */
  Parametrisation m_reparametrisation;

  /**
   * @brief Run ranking fits? 
   * 
   */
  bool m_runRanking;

  /**
   * @brief stores the scale applied to get covariance matrix determinant closer to 1 
   * 
   */
  double m_scaleCovMatrix;

  /**
   * @brief ru nthe usual fit? 
   * 
   */
  bool m_skipFit;

  /**
   * @brief list of skipped NPs (added to covariance) for each measurement 
   * 
   */
  std::vector<std::vector<std::string> > m_skippedNPs;

  /**
   * @brief Total stat covariance matrix
   * 
   */
  TMatrixDSym m_statCovariance;

  /**
   * @brief Is that only fit? 
   * 
   */
  bool m_statOnly;

  /**
   * @brief Maps of the impacts for each systematic
   * 
   */
  std::map<std::string, std::vector<double> > m_systematicMap;

  /**
   * @brief Number of threads to be used 
   * 
   */
  std::size_t m_threadN;

  /**
   * @brief X parameter 
   * 
   */
  TMatrixDSym m_totalNPcorrelation;

  /**
   * @brief Covariance matrix in the case of the correlated NPs 
   * 
   */
  TMatrixDSym m_totalNPcovarianceCorrelated;

  /**
   * @brief (C^-1 - I^-1)^-1 * C^-1 * tau term
   * 
   */
  std::vector<std::vector<double> > m_totalPull;

  /**
   * @brief (C^-1 - I^-1)^-1 * C^-1 * tau term for the correlated NPs
   * 
   */
  std::vector<double> m_totalPullCorrelated;

  /**
   * @brief tells the code to use the likelihood with correlated NPs 
   * 
   */
  bool m_useCorrelatedNPs;
  
  /**
   * @brief Flag to tell if to use Minos and on which parameters
   * 
   */
  std::pair<bool, std::vector<std::string> > m_useMinos;

  /**
   * @brief Flag to tell if to use the Pearson chi^2 correction 
   * 
   */
  std::vector<std::string> m_usePearsonChi2;

  /**
   * @brief Input values for the POIs 
   * 
   */
  std::vector<double> m_values;

  /**
   * @brief Input values for the POIs - unique 
   * 
   */
  std::vector<double> m_valuesUnique;

  /**
   * @brief NP categories used for plotting 
   * 
   */
  std::vector<std::string> m_npUniqueCategories;

  /**
   * @brief Per NP category
   * 
   */
  std::vector<std::vector<std::string> > m_npCategories;
};
