#pragma once

#include "RooArgList.h"

#include <string>
#include <vector>

/**
 * @brief Class containing the parameters used by roofit 
 * 
 */
class Parameter {

public:

  /**
   * @brief Construct a new Parameter object
   * 
   * @param s name
   * @param min minimum of the parameter
   * @param max maximum of the parameter
   */
  Parameter(const std::string& s, const double mean, const double min, const double max) :
  m_mean(mean),
  m_min(min),
  m_max(max),
  m_param(s) {}

  /**
   * @brief Destroy the Parameter object
   * 
   */
  ~Parameter() = default;

  /**
   * @brief Mean value
   * 
   * @return double 
   */
  inline double Mean() const {return m_mean;}

  /**
   * @brief Minimum of the parameter 
   * 
   * @return double 
   */
  inline double Min() const {return m_min;}

  /**
   * @brief Maximum of the parameter
   * 
   * @return double 
   */
  inline double Max() const {return m_max;}

  /**
   * @brief Name of the perameter 
   * 
   * @return const std::string& 
   */
  inline const std::string& Name() const {return m_param;}

private:

  /**
   * @brief mean value
   * 
   */
  double m_mean;

  /**
   * @brief minimum of the parameter
   * 
   */
  double m_min;

  /**
   * @brief maximum of the parameter
   * 
   */
  double m_max;

  /**
   * @brief name of the parameter
   * 
   */
  std::string m_param;

};

/**
 * @brief Class containing information needed for reparametrisation
 * 
 */
class Parametrisation {

public:

  /**
   * @brief Construct a new Parametrisation object
   */
  explicit Parametrisation();

  /**
   * @brief Destroy the Parametrisation object
   * 
   */
  ~Parametrisation() = default;

  /**
   * @brief Add formula 
   * 
   * @param replacement Replacement for
   * @param formula The formula
   */
  inline void AddFormula(const std::string& replacement, const std::string& formula) {m_formulae.emplace_back(std::make_pair(replacement, formula));}

  /**
   * @brief Add a new parameter
   * 
   * @param par Parameter
   */
  inline void AddParameter(const Parameter& par) {m_parameters.emplace_back(par);}

  /**
   * @brief Get the Dependencies list
   * 
   * @param poi The POI
   * 
   * @return RooArgList 
   */
  RooArgList GetDependencies(const std::string& poi) const;

  /**
   * @brief Get the names of the parameters used for the dependencies
   * 
   * @return std::vector<std::string> 
   */
  std::vector<std::string> GetDependencyNames() const;

  /**
   * @brief Get the Formula object
   * 
   * @param poi 
   * @return std::string 
   */
  std::string GetFormula(const std::string& poi) const;

  /**
   * @brief Check if the POI is reparametrised
   * 
   * @param poi 
   * @return true 
   * @return false 
   */
  bool IsReparametrised(const std::string& poi) const;

  /**
   * @brief 
   * 
   */
  void Print() const;

private:

  /**
   * @brief Formula used in the replacement 
   * 
   */
  std::vector<std::pair<std::string, std::string> > m_formulae;

  /**
   * @brief List of the parameters that the replacement depends on
   * 
   */
  std::vector<Parameter> m_parameters;

};
