#pragma once

#include <memory>
#include <string>
#include <vector>

#include "TMatrixDSym.h"

class TH2;
class TH2D;

/**
 * @brief Class used for plotting of various things 
 * 
 */
class Plotter {

public:

  /**
   * @brief Construct a new Plotter object
   * 
   * @param outputPath path to the output folder
   */
  explicit Plotter(const std::string& outputPath, const std::vector<std::string>& format);

  /**
   * @brief Destroy the Plotter object
   * 
   */
  ~Plotter() = default;

  /**
   * @brief 
   * 
   * @param mat Matrix to be plotted 
   * @param poiNames Names of the POIs - needed for the bin labels
   * @param isCovariance Correlation or covariance
   */
  void PlotCorrelationCovariance(const TMatrixDSym& mat, const std::vector<std::string>& poiNames, const bool isCovariance) const;

  /**
   * @brief Plot the correlation matrix from the fit
   * 
   * @param cor Raw matrix
   * @param params Order list of the parameters
   * @param threshold threhsold for plotting
   */
  void PlotParametersCorrelation(const TH2* cor, const std::vector<std::string>& params, const double threshold) const;

  /**
   * @brief Plot the POIs
   * 
   * @param pois fitted POIs, name of the NP | central value | -ve | +ve
   */
  void PlotPOIs(const std::vector<std::tuple<std::string, std::string, double, double, double> >& pois) const;

  /**
   * @brief Plot the NP pulls
   * 
   * @param combinedNPs NPs for the combined fit, name of the NP | category | central value | -ve | +ve
   * @param measurements names of the measurements
   * @param npNames names of the NPs, measurement | np name
   * @param pulls pulls of the NPs, measurement | pull
   * @param constraints constraints of the NPs, measurement | pull
   * @param category NP category
   */
  void PlotPulls(const std::vector<std::tuple<std::string, std::string, double, double, double> >& combinedNPs,
                 const std::vector<std::string>& measurements,
                 const std::vector<std::vector<std::string> >& npNames,
                 const std::vector<std::vector<double> >& pulls,
                 const std::vector<std::vector<double> >& constraints,
                 const std::string& category) const;

  /**
   * @brief 
   * 
   * @param mat Matrix
   * @return std::unique_ptr<TH2> TH2 version of the matrix
   */
  static std::unique_ptr<TH2D> MatrixToTH2(const TMatrixDSym& mat);

private:

  /**
   * @brief Path to the output folder 
   * 
   */
  std::string m_outputPath;

  /**
   * @brief Format of the plot outputs
   * 
   */
  std::vector<std::string> m_printFormat;
};
