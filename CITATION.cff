cff-version: 1.2.0
message: "Please cite the following works when using this software."
type: software
authors:
- family-names: "Dado"
  given-names: "Tomas"
  orcid: "https://orcid.org/0000-0002-7050-2669"
  affiliation: "Institute European Laboratory for Particle Physics, CERN"
title: "Combiner, v0.0.4"
version: 0.0.4
doi: 10.5281/zenodo.6328570
repository-code: "https://gitlab.cern.ch/tdado/combiner/-/tags/v0.0.4"
url: "https://gitlab.cern.ch/tdado/combiner/"
keywords:
  - c++
  - physics
  - combination
  - statistical framework
license: "Apache-2.0"
abstract: |
  The package provides a framework to combine different measurements that can
  have statistical overlap and/or different constraints and pulls of the
  nuisance parameters.  The combination will give results for all the
  individual parameters in each measurement and the NPs that can be common
  across different measurements.  The code allows to reparametrise the
  parameters of interest using any formula readable by `TFormula`, which can be
  used e.g. for EFT interpretations.
